package cs544.game.rooms;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import cs544.game.exception.IllegalBetException;
import cs544.game.exception.OutOfTurnException;
import cs544.game.messages.AbstractMessage;
import cs544.game.messages.AvailableRoomsMessage;
import cs544.game.messages.JoinRoomMessage;
import cs544.game.server.ConnectionThread;
import cs544.game.server.Server;
import cs544.game.util.Constants;

public class LobbyRoom extends AbstractRoom {
    public LobbyRoom(Server server) {
        super(server);
        this.setMaximumConnections(Constants.MAX_LOBBY_CONNECTIONS);
        this.notificationTimer.schedule(new NotificationTask(this), 0,
                Constants.LOBBY_NOTIFICATION_INTERVAL);
    }

	protected void finalize() throws Throwable {
		this.notificationTimer.cancel();
	}

	public short getType() {
	   return Constants.JOIN_ROOM_MESSAGE;	
	}

    public void handleMessage(ConnectionThread connection, AbstractMessage message) throws IOException, IllegalBetException, OutOfTurnException {
        super.handleMessage(connection, message);
        switch (message.getMessageType()) {
        case Constants.JOIN_ROOM_MESSAGE:
            this.handleJoinRoomMessage(connection, (JoinRoomMessage)message);
            break;
        }
    }

    protected void handleJoinRoomMessage(ConnectionThread connection, JoinRoomMessage message) {
        AbstractRoom room;
        try {
            room = this.server.getRooms().get(message.getRoomId());
        } catch (IndexOutOfBoundsException e) {
            /* We only have HoldemRooms now, this would be expanded later. */
            room = new HoldEmRoom(this.server);
            this.server.getRooms().add(room);
        }
        this.leaveRoom(connection);
        try {
            room.joinRoom(connection);
        } catch (AbstractRoom.FullRoomException e) {
        	return;
        }
    }

	private final Timer notificationTimer = new Timer(true);

	private class NotificationTask extends TimerTask {
		public NotificationTask(LobbyRoom room) {
			super();
			this.room = room;
		}

		public void run() {
            AvailableRoomsMessage message = new AvailableRoomsMessage(
                    this.room.server.getRooms());
            this.room.distributeMessage(message);
		}

		private final LobbyRoom room;
	}
}
