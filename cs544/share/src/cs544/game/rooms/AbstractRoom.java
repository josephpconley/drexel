package cs544.game.rooms;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import cs544.game.exception.GameException;
import cs544.game.exception.IllegalBetException;
import cs544.game.exception.OutOfTurnException;
import cs544.game.messages.AbstractMessage;
import cs544.game.messages.ChatMessage;
import cs544.game.messages.ChatResponseMessage;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.server.ConnectionThread;
import cs544.game.server.Server;
import cs544.game.util.Constants;

public abstract class AbstractRoom {
    public static class FullRoomException extends GameException {}

    public static void joinOpenRoom(
            ConnectionThread connection, List<? extends AbstractRoom> rooms)
        throws FullRoomException {
        for (AbstractRoom room : rooms) {
            try {
                room.joinRoom(connection);
                return;
            } catch (FullRoomException e) {
                continue;
            }
        }
        throw new FullRoomException();
    }

    public AbstractRoom(Server server) {
        this.server = server;
    }

    public void close() {
        for (ConnectionThread connection : this.connections) {
            connection.close();
        }
    }

	public abstract short getType();

    public boolean isFull() {
        System.err.format("Current = %s, Max = %s\n", this.getCurrentConnections(), this.getMaximumConnections());
        return this.getCurrentConnections() >= this.getMaximumConnections();
    }

    public int getCurrentConnections() {
        return this.connections.size();
    }

    public int getMaximumConnections() {
        return this.maximumConnections;
    }

    public void setMaximumConnections(int maximumConnections) {
        this.maximumConnections = maximumConnections;
    }

    public void joinRoom(ConnectionThread connection) throws FullRoomException {
    	this.lock.lock();
        try {
            if (this.isFull()) {
                throw new FullRoomException();
            }
            this.connections.add(connection);
            connection.setRoom(this);
        } finally {
            this.lock.unlock();
        }
        this.sendWelcome(connection);
    }

    public void leaveRoom(ConnectionThread connection) {
        this.lock.lock();
        try {
            this.connections.remove(connection);
        } finally {
            this.lock.unlock();
        }
    }
    
    public void handleMessage(ConnectionThread connection, AbstractMessage message) throws IOException, IllegalBetException, OutOfTurnException {
    	switch (message.getMessageType()) {
        case Constants.CHAT_MESSAGE:
            this.handleChatMessage(connection, (ChatMessage)message);
            break;
    	case Constants.HOLDEM_GAME_MESSAGE:
    		this.handleGameMessage(connection, (HoldemGameMessage)message);
    		break;
    	}
    }

    protected void handleChatMessage(ConnectionThread connection, ChatMessage message) {
        String username = this.server.getUserName(connection);
        ChatResponseMessage response = new ChatResponseMessage(
                username,
                message.getMessage());
        System.err.format("Distributing message from '%s': %s\n", username,
                message.getMessage());
        this.distributeMessage(response, connection);
    }
    
    public void handleGameMessage(ConnectionThread connection, HoldemGameMessage message)
    			throws IllegalBetException, OutOfTurnException, IOException{};

    protected void sendWelcome(ConnectionThread connection) {
        ChatResponseMessage message = new ChatResponseMessage(
                "SERVER", String.format("Welcome to room '%s'", this));
        try {
            message.send(connection.getOutput());
        } catch (IOException e) {
            System.err.format("Could not send message to %s\n", connection);
        }
        
        message = new ChatResponseMessage(
                "SERVER", String.format("User '%s' has joined the room.",
                    this.server.getUserName(connection)));
        this.distributeMessage(message, connection);
    }

    public void distributeMessage(AbstractMessage message) {
        this.distributeMessage(message, null);
    }

    public void distributeMessage(AbstractMessage message, ConnectionThread connection) {
        for (ConnectionThread other : this.connections) {
            if (connection != null && connection == other) {
                continue;
            }
            try {
                message.send(other.getOutput());
            } catch (IOException e) {
                System.err.format("Could not send message to '%s' at %s\n",
                        this.server.getUserName(other),
                        other);
            }
        }
    }

    protected final Server server;
    /**
     * Need this lock to deal with race conditions in joining/leaving rooms.
     */
    private final Lock lock = new ReentrantLock();
    private int maximumConnections;
    protected final Set<ConnectionThread> connections = Collections.synchronizedSet(
            new HashSet<ConnectionThread>());
}
