package cs544.game.rooms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cs544.game.exception.IllegalBetException;
import cs544.game.exception.OutOfTurnException;
import cs544.game.holdem.Card;
import cs544.game.holdem.Deck;
import cs544.game.holdem.Hand;
import cs544.game.holdem.Player;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.messages.holdem.CardUpdateMessage;
import cs544.game.messages.holdem.GameResponseMessage;
import cs544.game.messages.holdem.GameResultMessage;
import cs544.game.messages.holdem.StateUpdateMessage;
import cs544.game.server.ConnectionThread;
import cs544.game.server.Server;
import cs544.game.util.Constants;

public class HoldEmRoom extends AbstractRoom {
    
	/**
	 * Creates room in given server based on max connections
	 */
    public HoldEmRoom(Server server) {
        super(server);
        this.setMaximumConnections(Constants.MAX_HOLDEM_CONNECTIONS);
        community = new LinkedList<Card>();
    }

	public short getType() {
		return Constants.HOLDEM_GAME_MESSAGE;	
	}
    
    public void joinRoom(ConnectionThread connection) throws FullRoomException {
    	super.joinRoom(connection);
    	if (this.getCurrentConnections() >= 2)
    	{ 
    		try {
				startGame();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
    	}
    }
	
	/**
	 * Starts game if adequate numbers of players are present
	 */
    public void startGame() throws IOException{
    	System.out.println("Started Game");
    	Iterator<ConnectionThread> iterator = this.connections.iterator();
    	
    	ConnectionThread connection; 
    	while (iterator.hasNext()){
    		connection = iterator.next();
    		players.add(new Player(connection, server.getUserName(connection), true));
    	}
    	
    	if(this.players.size() > 1){
    		dealerButton = -1;
    		dealHands();
    	}
    }
    
	/**
	 * Deals two cards to every player in the room, starts inital betting round
	 */
    public void dealHands() throws IOException{
    	System.out.println("Started Dealing Hands");
    	deck = new Deck();
    	deck.shuffle();
    	holdemState = HoldemState.PREFLOP;
    	for(int i=0;i<players.size();i++){
    		players.get(i).setInHand(true);
    		players.get(i).setGoodToGo(false);
    	}
    	
    	//Pass the dealer button to start the hand
    	dealerButton = (dealerButton + 1) % players.size();
    	if(dealerButton == players.size()){
    		dealerButton = 0;
    	}
    	
    	for(Player player : players){
    		if(player.isInHand()){
    			Hand h = new Hand();
	    		h.addCard(deck.deal());
	    		h.addCard(deck.deal());
	
	    		CardUpdateMessage c1 = new CardUpdateMessage(0,h.getCards().getFirst());
	    		c1.send(player.getThread().getOutput());
	        	System.out.println("Sent out: " + c1 + "Card: " + c1.getCard() );
	        	
	    		CardUpdateMessage c2 = new CardUpdateMessage(1,h.getCards().getLast());
	    		c2.send(player.getThread().getOutput());
	        	System.out.println("Sent out: " + c2 + "Card: " + c2.getCard() );
	
	    		player.setHand(h);
	    		player.setInHand(true);
    		}
    	}
    	
    	//Auto-blinds for now
    	small = (dealerButton + 1) % players.size();
    	players.get(small).setCurrentBet(Constants.SMALL_BLIND);
		pot += players.get(small).getCurrentBet();
		StateUpdateMessage s1 = new StateUpdateMessage(players.get(small).getUsername(),
															Constants.BID_UPDATE, players.get(small).getCurrentBet());
		s1.sendAll(players);
		
		big  = (dealerButton + 2) % players.size();
		players.get(big).setCurrentBet(Constants.BIG_BLIND);
		currentBet = players.get(big).getCurrentBet(); 
		pot += currentBet;
		StateUpdateMessage s2 = new StateUpdateMessage(players.get(big).getUsername(),
														Constants.BID_UPDATE, players.get(big).getCurrentBet());
		s2.sendAll(players);
		
		actionRequired = (dealerButton + 3) % players.size();
		Player nextToAct = players.get(actionRequired);
		StateUpdateMessage s3 = new StateUpdateMessage(nextToAct.getUsername(), 
																Constants.YOUR_TURN, (short)0);
		s3.send(nextToAct.getThread().getOutput());
    }
    
	/**
	 * Deals cards of index m to index n
	 */
    public void dealCards(int m, int n) throws IOException{
    	for(int i=m;i<n;i++){
    		System.out.println(i);
    		Card c = deck.deal();
    		community.add(c);
    		CardUpdateMessage u = new CardUpdateMessage(i,c);
    		u.sendAll(players);
    	}

    	for(int i=0;i<players.size();i++){
    		players.get(i).setCurrentBet((short)0);
   			players.get(i).setGoodToGo(false);
    	}
    	
    	//No one has yet to raise
    	currentBet = 0;
    	notifyNextPlayer(small);
    }
    
	/**
	 * Determines winner using util classes, sends results to players
	 */
    public void showdown() throws IOException{
    	ArrayList<Player> winners = new ArrayList<Player>();
    	double max = 0;
    	for(int i=0;i<players.size();i++){
    		if(players.get(i).isInHand()){
    			double handValue = players.get(i).getHand().bestHoldemHand(this.community).pokerRank();
    			if(handValue > max){
    				winners.removeAll(winners);
    				winners.add(players.get(i));
    				max = handValue;
    			}else if(handValue == max){
    				winners.add(players.get(i));
    			}
    			System.out.println(players.get(i).getHand().bestHoldemHand(community).toString());
    		}
    	}
    	
    	for(Player p : winners){
    		System.out.println("winner " + p.getHand().bestHoldemHand(community).toString());
    		GameResultMessage gr = new GameResultMessage(Constants.WON,pot,
										p.getHand().bestHoldemHand(community).toString());
    		gr.send(p.getThread().getOutput());
    	}
    }
    
	/**
	 * Talks to the next player to act, determines if everyone is done betting and/or the hand is over
	 */
    public void notifyNextPlayer(int next) throws IOException{
    	Player nextToAct = players.get(next);
    	System.out.println(nextToAct.getUsername() + " " + nextToAct.isGoodToGo());
    	boolean handOver = handOver();
    	
    	if(handOver){
			for(Player p : players){
				if(p.isInHand()){
					GameResultMessage gr = new GameResultMessage(Constants.WON,pot,
												p.getHand().bestHoldemHand(community).toString());
					gr.send(p.getThread().getOutput());
				}else{
					GameResultMessage gr = new GameResultMessage(Constants.LOST,(short)0,
												p.getHand().bestHoldemHand(community).toString());
					gr.send(p.getThread().getOutput());
				}
			}
			
			dealHands();
		}else{
	    	if(nextToAct.isGoodToGo()){
				switch(holdemState.ordinal()){
				case 0:
					holdemState = HoldemState.FLOP;
					dealCards(2,5);
					break;
				case 1:
					holdemState = HoldemState.TURN;
					dealCards(5,6);
					break;
				case 2:
					holdemState = HoldemState.RIVER;
					dealCards(6,7);
					break;
				case 3:
					holdemState = HoldemState.SHOWDOWN;
					showdown();
					handOver = true;
					break;
				}
			}
	    	if(handOver){
	    		dealHands();
	    	}else{
	    		StateUpdateMessage s3 = new StateUpdateMessage(nextToAct.getUsername(), 
																	Constants.YOUR_TURN, (short)0);
	    		s3.send(nextToAct.getThread().getOutput());
	    	}
		}
    }
    
	/**
	 * Handles any response message received from clients
	 */
    public void handleGameMessage(ConnectionThread connection, HoldemGameMessage message) 
    										throws OutOfTurnException, IllegalBetException, IOException{
    	switch (message.getGameMessageType()){
    	case Constants.GAME_RESPONSE_MESSAGE:
    		this.handleResponseMessage(connection, (GameResponseMessage)message);
    		break;
    	}
    }
    	
    public void handleResponseMessage(ConnectionThread connection, GameResponseMessage message) 
    											throws OutOfTurnException, IllegalBetException, IOException{
    	//Verify user
    	if(connection.getOutput() != players.get(actionRequired).getThread().getOutput()){
			throw new OutOfTurnException();
		}else{
			Player p = players.get(actionRequired);
			short bet = (short) (message.getResponseValue() + p.getCurrentBet());
	    	switch(message.getResponse()){
	    	case Constants.BET:
	    		System.out.println("Got bet from " + p.getUsername() + " of " + bet);
	    		if(bet < currentBet){
	    			System.out.println("illegal bet");
	    			throw new IllegalBetException();
	    		}else{
	    			if(bet > currentBet){
	    				currentBet = bet;
	    				pot += currentBet;
	    			}

	    			players.get(actionRequired).setCurrentBet(bet);
	    			players.get(actionRequired).setGoodToGo(true);
	    			StateUpdateMessage s = new StateUpdateMessage(p.getUsername(),Constants.BID_UPDATE, bet);
	    			s.sendAll(players);
	    			actionRequired = (actionRequired + 1) % players.size();
	    			notifyNextPlayer(actionRequired);
	    		}
	    		break;
	    	case Constants.CHECK:
	    		if(players.get(actionRequired).getCurrentBet() != currentBet){
	    			throw new IllegalBetException();
	    		}else{
	    			players.get(actionRequired).setGoodToGo(true);
	    			StateUpdateMessage s = new StateUpdateMessage(p.getUsername(),Constants.BID_UPDATE, (short)0);
	    			s.sendAll(players);
	    			actionRequired = (actionRequired + 1) % players.size();
	    			notifyNextPlayer(actionRequired);
	    		}
	    		break;
	    	case Constants.FOLD:
	    		System.out.println("fold!");
	    		StateUpdateMessage s = new StateUpdateMessage(p.getUsername(),Constants.BID_UPDATE,(short)-1);
	    		s.sendAll(players);
	    		players.get(actionRequired).setInHand(false);
	    		actionRequired = (actionRequired + 1) % players.size();
    			notifyNextPlayer(actionRequired);
	    		break;
	    	}
    	}
    }
    
	/**
	 * Checks to see if the hand is over
	 */
    private boolean handOver(){
    	int i = 0;
    	for(Player p : players){
    		if(p.isInHand()){
    			i++;
    		}
    	}
    	return (i < 2);
    }

    private HoldemState holdemState;
    private Deck deck;
    private LinkedList<Card> community;
    private short pot;
    private short currentBet;
    private int dealerButton;
    private int actionRequired;
    private int small;
    private int big;
    private final List<Player> players = Collections.synchronizedList(
            new ArrayList<Player>());
	public enum HoldemState{
		PREFLOP,FLOP,TURN,RIVER,SHOWDOWN;
	}
}
