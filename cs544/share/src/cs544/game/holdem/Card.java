package cs544.game.holdem;

public class Card implements Comparable<Card> {
	
    public enum Rank{
    	Two("2"), Three("3"), Four("4"), Five("5"), Six("6"), Seven("7"), 
    	Eight("8"), Nine("9"), Ten("10"), Jack("J"), Queen("Q"), King("K"), Ace("A");
    	
    	private final String rank;
    	Rank(String rank){
    		this.rank = rank;
    	}
    	public String rank(){
    		return rank;
    	}
    }

    public enum Suit{ 
    	Spades("S"), Hearts("H"), Diamonds("D"), Clubs("C"); 
    	
    	private final String suit;
    	Suit(String suit){
    		this.suit = suit;
    	}
    	public String suit(){
    		return suit;
    	}
    }	

    private final Rank rank;
    private final Suit suit;
    
	
	public Card(Rank rank, Suit suit){
		this.rank = rank;
		this.suit = suit;
	}
	
	public Rank getRank() {
		return rank;
	}
	public Suit getSuit() {
		return suit;
	}
	public String toString(){
		return this.getRank().rank() + this.getSuit().suit();
	}

	public int compareTo(Card card){
		return this.rank.compareTo(card.getRank());
	}
}
