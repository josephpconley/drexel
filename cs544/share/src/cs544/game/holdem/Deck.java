package cs544.game.holdem;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.lang.StringBuilder;

import cs544.game.holdem.Card.Rank;
import cs544.game.holdem.Card.Suit;

public class Deck {
	
	private LinkedList<Card> cards;
	
	public Deck(){
		this.cards = new LinkedList<Card>();
		for(Suit suit : Suit.values()){
			for (Rank rank : Rank.values()){
				cards.add(new Card(rank, suit));
			}
		}
		shuffle();
	}
	
	public Card deal(){
		return cards.remove();
	}
	
	public void shuffle(){
		Collections.shuffle(cards);
	}
	
	public String toString(){
		StringBuilder deckStringBuilder = new StringBuilder();
		/*
		deckStringBuilder.append("Deck Size: " +
				deck.size());
		/*IF we don't want the whole deck to print out
		 * then we can just print out the size.
		 */
		Card card;
		Iterator<Card> iterator = cards.iterator();
		while (iterator.hasNext())
		{
			card = iterator.next();
			deckStringBuilder.append(card.toString());
			deckStringBuilder.append(",");
		}
		return deckStringBuilder.toString();
	}
}
