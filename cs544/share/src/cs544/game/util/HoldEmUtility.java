package cs544.game.util;

import java.util.ArrayList;

public class HoldEmUtility {

	public static String[] suits = {"s","h","d","c"};
	public static String[] ranks = {"J","Q","K","A"};
	
	public static String cardToStr(int rank, int suit){
		
		StringBuffer str = new StringBuffer();
		if(rank > 10){
			str.append(ranks[rank - 11]);
		}else{
			str.append(Integer.toString(rank));
		}
		
		str.append(suits[suit]);
		return str.toString();	
	}
	
	public static int strToRank(String card){
		int n = 0;
		ArrayList<String> rank = new ArrayList<String>();
		for(int i=0;i<ranks.length;i++){
			rank.add(ranks[i]);
		}
		
		if(card.length() == 3){
			n = 10;
		}else{
			String r = card.substring(0,1);
			if(r.compareTo(":") > 0){
				n = rank.indexOf(r) + 11; 
			}else{
				n = Integer.valueOf(r);
			}
		}
		return n;
	}
	
	public static int strToSuit(String card){
		ArrayList<String> suit = new ArrayList<String>();
		for(int i=0;i<suits.length;i++){
			suit.add(suits[i]);
		}
		
		return suit.indexOf(card.substring(card.length()-1));
	}
}
