package cs544.game.util;

public interface Constants {
	public static final byte PROTOCOL_VERSION = 1;

	public static final String DEFAULT_ADDR = "127.0.0.1";
	public static final int DEFAULT_PORT = 8123;
	public static final int DEFAULT_BUFFERSIZE = 1500;
    public static final int MAX_USERNAME_LENGTH = 12;
    public static final int MAX_LOBBY_CONNECTIONS = 30;
    public static final int MAX_HOLDEM_CONNECTIONS = 10;
    /* In milliseconds */
    public static final int LOBBY_NOTIFICATION_INTERVAL = 5000;
	
	//Message types
	public static final short ERROR_MESSAGE = 0;
	public static final short CHAT_MESSAGE = 1;
	public static final short CHAT_RESPONSE_MESSAGE = 2;
	public static final short INITIALIZATION_MESSAGE = 3;
	public static final short AVAILABLE_ROOMS_MESSAGE = 4;
	public static final short JOIN_ROOM_MESSAGE = 5;
	public static final short HOLDEM_GAME_MESSAGE = 6;

	public static enum RoomType{
		HOLDEM_ROOM("Hold'em Room");
		
		private final String desc;
    	RoomType(String desc){
    		this.desc = desc;
    	}
    	public String desc(){
    		return desc;
    	}
	}
	
	//Game message types
	public static final int STATE_UPDATE_MESSAGE = 0;
	public static final int CARD_UPDATE_MESSAGE = 1;
	public static final int GAME_RESPONSE_MESSAGE = 2;
	public static final int GAME_RESULT_MESSAGE = 3;
	public static final int GAME_ENDED_MESSAGE = 4;
	public static final int LEAVE_GAME_MESSAGE = 5;
	
	//State updates
	public static final short YOUR_TURN = 0;
	public static final short BID_UPDATE = 1;
	public static final short PLAYER_JOINS = 2;
	public static final short PLAYER_LEAVES = 3;
	
	//Game betting responses
	public static final short BET = 0;
	public static final short RAISE = 1;
	public static final short CHECK = 2;
	public static final short FOLD = 3;
	
	public static final short SMALL_BLIND = 1;
	public static final short BIG_BLIND = 2;
	
	public static final short WON = 0;
	public static final short LOST = 1;
	public static final short TIE = 2;
}
