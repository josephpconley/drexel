package cs544.game.util;

import java.util.ArrayList;
import java.util.Random;

public class MathUtility {
	private static Random generator = new Random();
	
	//Performs factorial (n!)	
	public static int fact(int n){
		int fact = 1;

		if(n > 1){
			for(int i=1;i<n+1;i++){
				fact = fact * i;
			}
		}
		return fact;
	}
	
	public static int nCr(int n, int r){
		int C = 0;
		C = ( fact(n) / (fact(n-r) * fact(r)) );	
		return C;
	}

	//Produce next combination as an int[]
	public static int[] nextCombo(int n, int r,int[] a){
		int[] b = new int[r];
	
		for(int x=0;x<r;x++){
			b[x] = a[x];	//can't do straight equality of arrays, gets weird
		}
	
		int i = r - 1;
		while (b[i] == n - r + i) {
			i--;
		}
		b[i]++;
	
		for(int j=i+1;j<r;j++){
			b[j] = b[i]+j-i;
		}
		return b;
	}	
	
//Enumerates all combinations of nCr
	public static int[][] combos(int n, int r){
		int nCr = nCr(n,r);
		int[] a = new int[r];
		int[][] combos = new int[nCr][];
		
		for(int i=0;i<r;i++){
			a[i]=i;
		}
		combos[0]=a;
	
		for(int j=1;j<nCr;j++){
			a = nextCombo(n,r,a);
			combos[j] = a;
		}
	
		return combos;
	}
	
	//Return the min of an array of doubles
	public static double min (double[] index1)	{
		int count = 0;
		double min = 0;
		int len = index1.length;
			
		for(int i=0;i<len;i++){
			count = 0;
			for(int j=0;j<len;j++){
				if(index1[i] > index1[j])
				count++;
			}
			if(count == 0){
				min = index1[i];
			}
		}
		return min;
	}

//Return the min of an array of integers	
	public static int min (int[] index1){
		int count = 0;
		int min = 0;
		int len = index1.length;
			
		for(int i=0;i<len;i++){
			count = 0;
			for(int j=0;j<len;j++){
				if(index1[i] > index1[j]){
					count++;
				}
			}
	
			if(count == 0){
				min=index1[i];
			}
		}
		return min;
	}
	
//Return the max of an array of doubles
	public static double max (double[] index1){
		int count = 0;
		double max = 0;
		int len = index1.length;
			
		for(int i=0;i<len;i++){
			count = 0;
			for(int j=0;j<len;j++){
				if(index1[i] < index1[j]){
					count++;
				}
			}
	
			if(count == 0){
				max=index1[i];
			}
		}
		return max;
	}

//Return the max of an array of integers	
	public static int max (int[] index1){
		int count = 0;
		int max = 0;
		int len = index1.length;
			
		for(int i=0;i<len;i++){
			count = 0;
			for(int j=0;j<len;j++){
				if(index1[i] < index1[j]){
					count++;
				}
			}
			if(count == 0){
				max=index1[i];
			}
		}
		return max;
	}
	
//Produces an array of random integers from 0 to n-1//
	public static ArrayList<Integer> randIntList(int n)	{
		ArrayList<Integer> nums = new ArrayList<Integer>();
		nums.add(generator.nextInt(n));
		
		for(int i=1;i<n;i++){
			int num = -1;
			while(nums.contains((num = generator.nextInt(n)))){
			}
			
			nums.add(num);
		}
		
		return nums; 	
	}
}
