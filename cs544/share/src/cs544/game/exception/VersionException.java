package cs544.game.exception;

/**
 * Thrown upon reception of message with wrong version
 */
public class VersionException extends GameException {}
