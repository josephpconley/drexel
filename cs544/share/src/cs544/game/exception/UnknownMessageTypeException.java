package cs544.game.exception;

/**
 * Thrown upon reception of message with unknown type
 */
public class UnknownMessageTypeException extends GameException {}
