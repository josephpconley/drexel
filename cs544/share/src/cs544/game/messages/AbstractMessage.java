package cs544.game.messages;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cs544.game.util.Constants;
import cs544.game.exception.UnknownMessageTypeException;
import cs544.game.exception.VersionException;
import cs544.game.rooms.AbstractRoom;

public abstract class AbstractMessage {
    abstract public short getMessageType();

    public final void send(DataOutputStream stream) throws IOException {
    	stream.writeByte(Constants.PROTOCOL_VERSION);
    	stream.writeShort(this.getMessageType());
        this.sendImpl(stream);
        stream.flush();
    }

    public abstract void sendImpl(DataOutputStream stream) throws IOException;

    public static AbstractMessage receive(DataInputStream stream) throws IOException, VersionException, UnknownMessageTypeException {
        /* First byte is the version */
        final byte version = stream.readByte();
        if (version != Constants.PROTOCOL_VERSION) {
            System.out.format("%s != %s\n", version, Constants.PROTOCOL_VERSION);
            throw new VersionException();
        }
        /* Next short is the version */
        short messageType = stream.readShort();
        System.out.println("Received Message: " +  messageType );
        switch (messageType) {
        case Constants.INITIALIZATION_MESSAGE:
            return InitializationMessage.receive(stream);
        case Constants.CHAT_MESSAGE:
            return ChatMessage.receive(stream);
        case Constants.CHAT_RESPONSE_MESSAGE:
            return ChatResponseMessage.receive(stream);
        case Constants.AVAILABLE_ROOMS_MESSAGE:
            return AvailableRoomsMessage.receive(stream);
        case Constants.JOIN_ROOM_MESSAGE:
            return JoinRoomMessage.receive(stream);            
        case Constants.HOLDEM_GAME_MESSAGE:
        	return HoldemGameMessage.receive(stream);
        default:
            throw new UnknownMessageTypeException();
        }
    }
}
