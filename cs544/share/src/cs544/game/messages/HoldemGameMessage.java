package cs544.game.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import cs544.game.exception.UnknownMessageTypeException;
import cs544.game.exception.VersionException;
import cs544.game.holdem.Player;
import cs544.game.messages.holdem.CardUpdateMessage;
import cs544.game.messages.holdem.GameEndedMessage;
import cs544.game.messages.holdem.GameResponseMessage;
import cs544.game.messages.holdem.GameResultMessage;
import cs544.game.messages.holdem.LeaveGameMessage;
import cs544.game.messages.holdem.StateUpdateMessage;
import cs544.game.util.Constants;

public abstract class HoldemGameMessage extends AbstractMessage {
	
	public short getMessageType() {
		return Constants.HOLDEM_GAME_MESSAGE;
	}
	
	abstract public int getGameMessageType();
	
	public void sendImpl(DataOutputStream stream) throws IOException {
		stream.writeInt(getGameMessageType());
		sendGameImpl(stream);
	}
	
	public void sendAll(List<Player> players) throws IOException{
		for(Player p : players){
			this.send(p.getThread().getOutput());
		}
	}
	
	public abstract void sendGameImpl(DataOutputStream stream) throws IOException;
	
	public static HoldemGameMessage receive(DataInputStream stream) throws IOException, UnknownMessageTypeException{
        /* Next int is the Game MT */
		int gameMessageType = stream.readInt();
		System.out.println("Received game message type: " + gameMessageType);
        switch (gameMessageType) {
        case Constants.STATE_UPDATE_MESSAGE:
            return StateUpdateMessage.receive(stream);
        case Constants.CARD_UPDATE_MESSAGE:
        	return CardUpdateMessage.receive(stream);
        case Constants.GAME_RESPONSE_MESSAGE:
        	return GameResponseMessage.receive(stream);
        case Constants.GAME_RESULT_MESSAGE:
        	return GameResultMessage.receive(stream);
        case Constants.GAME_ENDED_MESSAGE:
        	return GameEndedMessage.receive(stream);
        case Constants.LEAVE_GAME_MESSAGE:
        	return LeaveGameMessage.receive(stream);        	
        default:
            throw new UnknownMessageTypeException();
        }
	}
}
