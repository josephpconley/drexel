package cs544.game.messages;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cs544.game.util.Constants;

public class ChatMessage extends AbstractMessage {
    public ChatMessage() {
        this("");
    }

	public ChatMessage(String message) {
		this.message = message;
	}
	
	public short getMessageType() {
		return this.type;
	}

    public void sendImpl(DataOutputStream stream) throws IOException {
        /* Write the size of the chat data */
        stream.writeInt(this.message.length());
        /* Write the actual message.  We do it this way because we don't want
         * to write the last newline byte in the string. */
        stream.write(this.message.getBytes(), 0, this.message.length());
    }

    public static ChatMessage receive(DataInputStream stream) throws IOException {
        /* Header information has already been read off by Message.receive. */
        /* size of the following text */
        final int size = stream.readInt();
        byte[] data = new byte[size];
        /* Read the full text of the mesage */
        stream.readFully(data);
        return new ChatMessage(new String(data));
    }
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
    private static final short type = Constants.CHAT_MESSAGE;
	private String message;
}
