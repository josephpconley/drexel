package cs544.game.messages;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cs544.game.util.Constants;

public class JoinRoomMessage extends AbstractMessage {

	public JoinRoomMessage(short roomType, short roomId) {
		this.roomType = roomType;
		this.roomId = roomId;
	}
	
	public short getMessageType() {
		return this.type;
	}

    public short getRoomType() {
        return this.roomType;
    }

    public short getRoomId() {
        return this.roomId;
    }

    public void sendImpl(DataOutputStream stream) throws IOException {
    	stream.writeShort(this.roomType);
        stream.writeShort(this.roomId);
    }

    public static JoinRoomMessage receive(DataInputStream stream) throws IOException {
        /* Header information has already been read off by Message.receive. */
    	return new JoinRoomMessage(stream.readShort(), stream.readShort());
    }
	
    private static final short type = Constants.JOIN_ROOM_MESSAGE;
	private final short roomType;
	private final short roomId;
}
