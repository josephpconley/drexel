package cs544.game.messages;

import java.util.Arrays;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cs544.game.util.Constants;

public class InitializationMessage extends AbstractMessage {
    public InitializationMessage() {
        this("");
    }

    public InitializationMessage(String username) {
        this.username = username;
    }

    public short getMessageType() {
        return this.type;
    }

    public String getUserName() {
        return this.username;
    }

    public void setUserName(String userName) {
        this.username = username;
    }

    public void sendImpl(DataOutputStream stream) throws IOException {
        /* We need the username field to be exactly 12 bytes */
        byte[] bytes = Arrays.copyOf(this.getUserName().getBytes(),
                Constants.MAX_USERNAME_LENGTH);
        stream.write(bytes, 0, bytes.length);
    }

    public static InitializationMessage receive(DataInputStream stream) throws IOException {
        /* Again, our username field is exactly 12 bytes */
        byte[] data = new byte[Constants.MAX_USERNAME_LENGTH];
        stream.readFully(data);
        /* We trim the string of whitespace in case the username is actually shorter
         * than the 12 byte field. */
        return new InitializationMessage(new String(data).trim());
    }

    public static final short type = Constants.INITIALIZATION_MESSAGE;
    private String username;
}
