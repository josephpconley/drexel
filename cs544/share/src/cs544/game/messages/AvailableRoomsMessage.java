package cs544.game.messages;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;

import cs544.game.rooms.AbstractRoom;
import cs544.game.util.Constants;

public class AvailableRoomsMessage extends AbstractMessage {
    public static class AvailableRoom {
        public AvailableRoom(short type, short index,
                short maxPlayers, short currentPlayers) {
            this.type = type;
            this.index = index;
            this.maxPlayers = maxPlayers;
            this.currentPlayers = currentPlayers;
        }

        public final short type;
        public final short index;
        public final short maxPlayers;
        public final short currentPlayers;
    }

	public AvailableRoomsMessage() {
		//this(null);
	}

	public AvailableRoomsMessage(List<AbstractRoom> rooms) {
		this.setRooms(rooms);
	}

	public short getMessageType() {
		return this.type;
	}

    public void setRooms(List<AbstractRoom> rooms) {
        this.rooms.clear();
    	int i = 0;
    	for(AbstractRoom r : rooms){
			short id = (short)i;
            this.rooms.add(new AvailableRoom(r.getType(), id,
                        (short)r.getMaximumConnections(),
                        (short)r.getCurrentConnections()));
            i++;
        }

    }

	public List<AvailableRoom> getRooms() {
		return this.rooms;
	}

	public void sendImpl(DataOutputStream stream) throws IOException {
		/* Write size of list (in items) */
		stream.write(this.rooms.size());
        for (AvailableRoom room : this.rooms) {
			/* Write room type (16 bits) */
			stream.writeShort(room.type);
			/* Room id (index) */
			stream.writeShort(room.index);
			/* Max players */
			stream.writeShort(room.maxPlayers);
			/* Current players */
			stream.writeShort(room.currentPlayers);
		}
	}

	//Joe: this deviates somewhat from our design, should be noted
	public static AvailableRoomsMessage receive(DataInputStream stream) throws IOException {
		int size = stream.readByte();
        AvailableRoomsMessage message = new AvailableRoomsMessage();
		while (size > 0) {
            message.rooms.add(new AvailableRoom(
                /* Room type */
                stream.readShort(),
                /* Room id */
                stream.readShort(),
                /* Max players */
                stream.readShort(),
                /* Current players */
                stream.readShort()));
            
            size--;
		}
        return message;
	}

	public static final short type = Constants.AVAILABLE_ROOMS_MESSAGE;
	public final List<AvailableRoom> rooms = new ArrayList<AvailableRoom>();
}
