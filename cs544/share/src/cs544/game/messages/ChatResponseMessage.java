package cs544.game.messages;

import java.util.Arrays;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cs544.game.util.Constants;

public class ChatResponseMessage extends AbstractMessage {
	public ChatResponseMessage(String userName, String message) {
		this.userName = userName;
		this.message = message;
	}
	
	public short getMessageType() {
		return Constants.CHAT_RESPONSE_MESSAGE;
	}

	public void sendImpl(DataOutputStream stream) throws IOException {
        byte[] bytes = Arrays.copyOf(this.userName.getBytes(),
                Constants.MAX_USERNAME_LENGTH);
        stream.write(bytes, 0, bytes.length);
        stream.writeInt(this.message.length());
        stream.write(this.message.getBytes(), 0, this.message.length());
	}

    public static ChatResponseMessage receive(DataInputStream stream) throws IOException {
        byte[] userName = new byte[Constants.MAX_USERNAME_LENGTH];
        stream.readFully(userName);

        final int size = stream.readInt();
        byte[] message = new byte[size];
        stream.readFully(message);
        return new ChatResponseMessage(new String(userName).trim(), new String(message));
    }
	
	public String getUserName() {
		return userName;
	}

	public String getMessage() {
		return message;
	}
	
	private final String userName;
	private final String message;
}
