/**
 * This Class is depreciated.  The processing of handling each message was
 * divided up into separate message handling methods.
 */

package cs544.game.messages.processing;

import java.util.LinkedHashMap;
import java.util.Map;

import cs544.game.util.Constants;

@Deprecated
public class MessageProcessorManager 
{
//	private static Map<Integer,MessageProcessingIF> manager = null;
	
//	public static Map<Integer,MessageProcessingIF> getInstance()
//	{
//		if (manager == null)
//		{
//			manager = new LinkedHashMap<Integer, MessageProcessingIF>();
//			initialize();
//		}
//		return manager;
//	}
	
	private static Map<Integer,MessageProcessorIF> manager = 
		new LinkedHashMap<Integer, MessageProcessorIF>();
	{
		initialize();
	}
	
	
//	public static Map<Integer,MessageProcessingIF> getInstance()
//	{
//		return manager;
//	}

	private static void initialize()
	{
		manager.put(new Integer(Constants.AVAILABLE_ROOMS_MESSAGE),
				new AvailableRoomsMessageProcessor());
		manager.put(new Integer(Constants.CHAT_MESSAGE),
				new ChatMessageProcessor());
		manager.put(new Integer(Constants.CHAT_RESPONSE_MESSAGE),
				new ChatResponseMessageProcessor());
		manager.put(new Integer(Constants.ERROR_MESSAGE),
				new ErrorMessageProcessor());
		manager.put(new Integer(Constants.HOLDEM_GAME_MESSAGE),
				new HoldEmGameMessageProcessor());
		manager.put(new Integer(Constants.INITIALIZATION_MESSAGE),
				new InitializationMessageProcessor());
		manager.put(new Integer(Constants.JOIN_ROOM_MESSAGE),
				new JoinRoomMessageProcessor());
		manager.put(new Integer(Constants.CHAT_RESPONSE_MESSAGE),
				new ChatResponseMessageProcessor()); 
		//continued for all messages
	}

	@Deprecated
	public static MessageProcessorIF getMessageProcessor(short messageType)
	{
		Integer messageTypeInt = new Integer(messageType);
		MessageProcessorIF msgProcessor = manager.get(messageTypeInt);
		return msgProcessor;
	}
}
