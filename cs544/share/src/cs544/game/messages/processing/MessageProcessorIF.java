/**
 * This Class is depreciated.  The processing of handling each message was
 * divided up into separate message handling methods.
 */

package cs544.game.messages.processing;

import cs544.game.messages.AbstractMessage;
import cs544.game.server.ConnectionThread;


@Deprecated
public interface MessageProcessorIF 
{
	@Deprecated
	public void process(ConnectionThread connectionThread,
			AbstractMessage msg);

	@Deprecated
	public AbstractMessage build();
}
