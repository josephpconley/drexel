/**
 * This Class is depreciated.  The processing of handling each message was
 * divided up into separate message handling methods.
 */

package cs544.game.messages.processing;

import cs544.game.messages.AbstractMessage;
import cs544.game.server.ConnectionThread;

@Deprecated
public class AvailableRoomsMessageProcessor implements MessageProcessorIF {

	@Deprecated
	@Override
	public AbstractMessage build() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	@Override
	public void process(ConnectionThread connectionThread, AbstractMessage msg) {
		// TODO Auto-generated method stub
		
	}

}
