package cs544.game.messages.holdem;

import java.util.Arrays;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class StateUpdateMessage extends HoldemGameMessage {

	private final String username;
	private final short stateUpdate;
	private final short chipAmount;
	
	public StateUpdateMessage(String username, short stateUpdate, short chipAmount) {
		this.username = username;
		this.stateUpdate = stateUpdate;
		this.chipAmount = chipAmount;
	}
	
	public String getUsername() {
		return username;
	}
	public short getStateUpdate() {
		return stateUpdate;
	}
	public short getChipAmount() {
		return chipAmount;
	}
	public int getGameMessageType() {
		return Constants.STATE_UPDATE_MESSAGE;
	}

	public void sendGameImpl(DataOutputStream stream) throws IOException {
		System.out.println("state " + stateUpdate + " for " + this.username);
		stream.writeShort(stateUpdate);
        stream.writeShort(chipAmount);
        byte[] userName = Arrays.copyOf(this.username.getBytes(),
                Constants.MAX_USERNAME_LENGTH);
        stream.write(userName, 0, userName.length);
	}
	
    public static StateUpdateMessage receive(DataInputStream stream) throws IOException {
        /* Header information has already been read off by Message.receive. */
        short stateUpdate = stream.readShort();
        short chipAmount = stream.readShort(); 
    	
    	byte[] data = new byte[12];
        stream.readFully(data);
        return new StateUpdateMessage(new String(data), stateUpdate, chipAmount);
    }
}
