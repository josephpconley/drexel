package cs544.game.messages.holdem;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cs544.game.holdem.Card;
import cs544.game.holdem.Card.Rank;
import cs544.game.holdem.Card.Suit;
import cs544.game.messages.ChatMessage;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class CardUpdateMessage extends HoldemGameMessage {

	private final long cardIndex;
	private final Card card;
	
	public CardUpdateMessage(long cardIndex, Card card) {
		this.cardIndex = cardIndex;
		this.card = card;
	}
	
	public long getCardIndex() {
		return cardIndex;
	}
	public Card getCard() {
		return card;
	}
	public int getGameMessageType() {
		return Constants.CARD_UPDATE_MESSAGE;
	}
	
	public void sendGameImpl(DataOutputStream stream) throws IOException {
		stream.writeLong(cardIndex);
        stream.writeInt(card.getRank().ordinal());
        stream.writeInt(card.getSuit().ordinal());
	}
	
	public static CardUpdateMessage receive(DataInputStream stream) throws IOException {
        /* Header information has already been read off by Message.receive. */
    	long cardIndex = stream.readLong();
        int rank = stream.readInt();
        int suit = stream.readInt();
        Card card = new Card(Rank.values()[rank],Suit.values()[suit]);
        return new CardUpdateMessage(cardIndex,card);
    }
}
