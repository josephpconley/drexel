package cs544.game.messages.holdem;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cs544.game.holdem.Card;
import cs544.game.holdem.Card.Rank;
import cs544.game.holdem.Card.Suit;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class GameResponseMessage extends HoldemGameMessage {

	private final short response;
	private final short responseValue;
	
	public GameResponseMessage(short response, short responseValue) {
		this.response = response;
		this.responseValue = responseValue;
	}
	
	public short getResponse() {
		return response;
	}
	public short getResponseValue() {
		return responseValue;
	}
	public int getGameMessageType() {
		return Constants.GAME_RESPONSE_MESSAGE;
	}
	
	public void sendGameImpl(DataOutputStream stream) throws IOException {
        stream.writeShort(response);
        stream.writeShort(responseValue);
	}
	
	public static GameResponseMessage receive(DataInputStream stream) throws IOException{
        /* Header information has already been read off by Message.receive. */
		return new GameResponseMessage(stream.readShort(),stream.readShort());
	}
}
