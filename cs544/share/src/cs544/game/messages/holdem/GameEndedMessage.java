package cs544.game.messages.holdem;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class GameEndedMessage extends HoldemGameMessage {

	private final long reason;
	
	public GameEndedMessage(long reason) {
		this.reason = reason;
	}
	
	public long getReason() {
		return reason;
	}
	public int getGameMessageType() {
		return Constants.GAME_ENDED_MESSAGE;
	}
	
	public void sendGameImpl(DataOutputStream stream) throws IOException {
        stream.writeLong(reason);
	}	
	
	public static GameEndedMessage receive(DataInputStream stream) throws IOException{
        /* Header information has already been read off by Message.receive. */
		return new GameEndedMessage(stream.readLong());
	}
}
