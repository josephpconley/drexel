package cs544.game.messages.holdem;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cs544.game.holdem.Card;
import cs544.game.holdem.Card.Rank;
import cs544.game.holdem.Card.Suit;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class LeaveGameMessage extends HoldemGameMessage {

	
	public LeaveGameMessage(){}
	
	public int getGameMessageType() {
		return Constants.LEAVE_GAME_MESSAGE;
	}
	
	public void sendGameImpl(DataOutputStream stream) throws IOException {
	}
	
	public static LeaveGameMessage receive(DataInputStream stream) throws IOException{
        /* Header information has already been read off by Message.receive. */
		return new LeaveGameMessage();
	}
}
