package cs544.game.messages.holdem;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

public class GameResultMessage extends HoldemGameMessage {

	private final short result;
	private final short resultValue;
	private final String winningHand;
	
	//Joe: Changed to add the username and a string printing out that user's hand
	public GameResultMessage(short result, short resultValue, String winningHand) {
		this.result = result;
		this.resultValue = resultValue;
		this.winningHand = winningHand;
	}
	public GameResultMessage(short result, short resultValue) {
		this.result = result;
		this.resultValue = resultValue;
		this.winningHand = "";
	}
	
	public short getResult() {
		return result;
	}
	public short getResultValue() {
		return resultValue;
	}
	public String getWinningHand(){
		return winningHand;
	}
	public int getGameMessageType() {
		return Constants.GAME_RESULT_MESSAGE;
	}
	
	public void sendGameImpl(DataOutputStream stream) throws IOException {
        stream.writeShort(result);
        stream.writeShort(resultValue);
        stream.writeInt(winningHand.length());
        stream.write(winningHand.getBytes(), 0, winningHand.length());
	}
	
	public static GameResultMessage receive(DataInputStream stream) throws IOException{
        /* Header information has already been read off by Message.receive. */
		
		short temp1 = stream.readShort();
		short temp2 = stream.readShort();

		final int size = stream.readInt();
	    byte[] message = new byte[size];
	    stream.readFully(message);
		return new GameResultMessage(temp1,temp2, new String(message));
	}
}
