package cs544.game.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import cs544.game.exception.GameException;
import cs544.game.messages.AbstractMessage;
import cs544.game.rooms.AbstractRoom;

public class ConnectionThread extends Thread {
	public ConnectionThread(Socket socket) throws IOException {
		this.socket = socket;
        this.input = new DataInputStream(this.socket.getInputStream());
        this.output = new DataOutputStream(this.socket.getOutputStream());
	}
	
    public Socket getSocket() {
        return this.socket;
    }

	public void run() {
		AbstractMessage message;
		while (true) {
            try {
                message = this.getMessage();
            } catch (GameException e) {
                this.close();
                return;
            } catch (IOException e) {
                this.close();
                return;
            }
            try {
				this.room.handleMessage(this, message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

    public AbstractMessage getMessage() throws IOException, GameException {
        return AbstractMessage.receive(this.input);
    }

	public void close() {
		try {
			this.socket.close();
		} catch (IOException e) {}
	}

    public void setRoom(AbstractRoom room) {
        this.room = room;
    }

    public AbstractRoom getRoom() {
        return this.room;
    }

    public DataOutputStream getOutput() {
        return this.output;
    }
	
	private final Socket socket;
    private final DataInputStream input;
    private final DataOutputStream output;
    private AbstractRoom room;
}
