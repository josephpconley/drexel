/**
 * This Class is depreciated.  The processing to ensure the messages were 
 * received in the right order was transitioned into the message handlers.
 */

package cs544.game.server;

import java.util.HashMap;

import java.util.Map;

import cs544.game.messages.AbstractMessage;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;

@Deprecated
public class GameState {

	@Deprecated
	public enum State{
		LOBBY,
		HAND_START,
		PLAYING, 
		ACTION_REQUIRED,
		OUT;
	}
	@Deprecated
	private static Map<ConnectionThread, State> connectStateMap = 
		new HashMap<ConnectionThread, State>();
	@Deprecated
	public static void addConnection(ConnectionThread connection)
	{

		connectStateMap.put(connection, State.HAND_START);
	}
	
	
	
	@Deprecated
	public static boolean expectedMessage(ConnectionThread connection, AbstractMessage msg)
	{	
		State state = connectStateMap.get(connection);
		
		boolean expected = false;
		short messageType = msg.getMessageType();
		int gameMessageType;
		if (messageType == Constants.HOLDEM_GAME_MESSAGE)
		{
			gameMessageType = ((HoldemGameMessage) msg).getGameMessageType();
		switch (state)
		{
		case HAND_START:
			switch (gameMessageType)
			{
				case Constants.CARD_UPDATE_MESSAGE:
				expected = true;
				state = State.PLAYING;
				break;
			case Constants.LEAVE_GAME_MESSAGE:
			case Constants.GAME_ENDED_MESSAGE:
				expected = true;
				state = State.LOBBY;
				break;
			case Constants.GAME_RESPONSE_MESSAGE:
			case Constants.GAME_RESULT_MESSAGE:
			case Constants.STATE_UPDATE_MESSAGE:
			default:
				break;	
			}
			break;
		case PLAYING:
			switch (messageType)
			{
			case Constants.CARD_UPDATE_MESSAGE:
				expected = true;
				break;
			case Constants.LEAVE_GAME_MESSAGE:
				expected = true;
				state = State.LOBBY;
				break;				
			case Constants.GAME_RESULT_MESSAGE:
				state = State.HAND_START;
				expected = true;
				break;
			case Constants.STATE_UPDATE_MESSAGE:
				expected = true;
				HoldemGameMessage gameMsg = (HoldemGameMessage) msg;
				if (gameMsg.getGameMessageType() != Constants.YOUR_TURN)
				{
					state = State.PLAYING;
				}
				else
				{
					state = State.ACTION_REQUIRED;
				}
				break;
			case Constants.GAME_ENDED_MESSAGE:
			case Constants.GAME_RESPONSE_MESSAGE:
			default:
				break;	
			}
			break;
		case ACTION_REQUIRED:
			switch (messageType)
			{
			case Constants.LEAVE_GAME_MESSAGE:
				expected = true;
				state = State.LOBBY;
				break;		
			case Constants.GAME_RESPONSE_MESSAGE:
				expected = true;
				HoldemGameMessage gameMsg = (HoldemGameMessage) msg;
				if (gameMsg.getGameMessageType() == Constants.FOLD)
				{
					state = State.OUT;
				}
				else
				{
					state = State.PLAYING;
				}
				expected = true;
				break;
			case Constants.CARD_UPDATE_MESSAGE:
			case Constants.GAME_ENDED_MESSAGE:
			case Constants.STATE_UPDATE_MESSAGE:
			case Constants.GAME_RESULT_MESSAGE:
			default:
				break;	
			}
			break;
		case OUT:
			switch (messageType)
			{			
			case Constants.LEAVE_GAME_MESSAGE:
				expected = true;
				state = State.LOBBY;
				break;		
			case Constants.GAME_RESULT_MESSAGE:
				state = State.HAND_START;
				expected = true;
				break;
			case Constants.STATE_UPDATE_MESSAGE:
				HoldemGameMessage gameMsg = (HoldemGameMessage) msg;
				if (gameMsg.getGameMessageType() != Constants.YOUR_TURN)
				{
					state = State.OUT;
					expected = true;
				}
				break;
			case Constants.CARD_UPDATE_MESSAGE:
			case Constants.GAME_ENDED_MESSAGE:
			case Constants.GAME_RESPONSE_MESSAGE:
			default:
				break;	
			}
			break;
			
		}
		}

		return expected;
	}
	
	@Deprecated
	public static State getState(ConnectionThread connection)
	{
		State state = connectStateMap.get(connection);
		return state;
	}
}
