package cs544.game.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import cs544.game.rooms.AbstractRoom;
import cs544.game.rooms.LobbyRoom;
import cs544.game.util.Constants;
import cs544.game.exception.GameException;
import cs544.game.messages.AbstractMessage;
import cs544.game.messages.InitializationMessage;

public class Server {
	public static final int DEFAULT_BACKLOG = 0;

	/**
	 * Default constructor that starts server on default port.
	 */
	public Server() throws IOException {
		this(Constants.DEFAULT_ADDR, Constants.DEFAULT_PORT, DEFAULT_BACKLOG);
	}

	/**
	 * Constructor that allows you to specify the address or port to bind to.
	 * 
	 * @param addr A string for the hostname or IP address.
	 * @param port The port number to bind to.
	 */
	public Server(String address, int port, int backlog) throws IOException {
		try {
			this.socket = new ServerSocket(port, backlog, InetAddress.getByName(address));
		} catch (IOException e) {
			System.err.format("Could not create socket on %s:%s\n", address, port);
			throw e;
		}
        System.err.format("Server started on %s:%s\n", address, port);
	}

	protected void finalize() throws Throwable {
		this.stop();
		super.finalize();
	}

	public void start() throws IOException {
		while (this.listening) {
			ConnectionThread connection = new ConnectionThread(this.socket.accept());

            System.out.printf("New connection from %s\n", connection.getSocket());
            try {
                this.handleInitializationMessage(connection);
            } catch (Exception e) {
                System.out.println("Connection aborted.");
                e.printStackTrace();
                connection.close();
                continue;
            }

            System.out.printf("New connection username '%s'\n", this.users.get(connection));

            try {
                AbstractRoom.joinOpenRoom(connection, this.lobbies);
            } catch (AbstractRoom.FullRoomException e) {
                System.err.println("No open rooms, making a new one.");
                LobbyRoom room = new LobbyRoom(this);
                try {
                    room.joinRoom(connection);
                } catch (AbstractRoom.FullRoomException f) {
                    /* If this happens, something is really wrong. */
                    System.err.println("Something bad happened.");
                    return;
                }
                this.lobbies.add(room);
            }
			connection.start();
		}
	}

	public void stop() {
		try {
			this.socket.close();
		} catch (IOException e) {}
		for (AbstractRoom room : this.rooms) {
			room.close();
		}
		for (LobbyRoom room : this.lobbies) {
			room.close();
		}
		this.listening = false;
	}

    public void handleInitializationMessage(ConnectionThread connection) throws GameException, IOException {
        AbstractMessage message = connection.getMessage();
        if (message.getMessageType() != Constants.INITIALIZATION_MESSAGE) {
            throw new GameException();
        }
        this.users.put(connection, ((InitializationMessage)message).getUserName());
    }

	public InetAddress getAddress() {
		return this.socket.getInetAddress();
	}

	public int getPort() {
		return this.socket.getLocalPort();
	}

    public String getUserName(ConnectionThread connection) {
        return this.users.get(connection);
    }

	public List<AbstractRoom> getRooms() {
		return this.rooms;
	}

	public List<LobbyRoom> getLobbies() {
		return this.lobbies;
	}

	public static void main(String[] args) {
		Server server = null;
		try {
			switch (args.length) {
			case 2:
				server = new Server(args[0], Integer.valueOf(args[1]), DEFAULT_BACKLOG);
				break;
			case 1:
				server = new Server(args[0], Constants.DEFAULT_PORT, DEFAULT_BACKLOG);
				break;
			default:
				server = new Server();
				break;
			}
			server.start();
		} catch (IOException e) {
			System.err.println("Error!");
            e.printStackTrace();
		} finally {
			if (server != null) {
				server.stop();
			}
		}
	}

	private boolean listening = true;
	private ServerSocket socket;
    private final List<AbstractRoom> rooms = Collections.synchronizedList(
            new ArrayList<AbstractRoom>());
    private final List<LobbyRoom> lobbies = Collections.synchronizedList(
            new ArrayList<LobbyRoom>());
    /* Map of connections to user names */
    private final Map<ConnectionThread,String> users = Collections.synchronizedMap(
            new HashMap<ConnectionThread,String>());
}
