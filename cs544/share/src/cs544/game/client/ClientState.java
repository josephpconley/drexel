/**
 * This Class is depreciated.  The processing to ensure the messages were 
 * received in the right order was transitioned into the message handlers.
 */

package cs544.game.client;

import cs544.game.messages.AbstractMessage;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.util.Constants;
@Deprecated
public class ClientState {
	@Deprecated
	public enum State{
		LOBBY,
		HAND_START,
		PLAYING, 
		ACTION_REQUIRED,
		OUT;
	}
	@Deprecated
	public boolean expectedMessage(AbstractMessage msg)
	{	
		boolean expected = false;
		short messageType = msg.getMessageType();
		switch (state)
		{
		case HAND_START:
			switch (messageType)
			{
			case Constants.AVAILABLE_ROOMS_MESSAGE:
			case Constants.CHAT_MESSAGE:
			case Constants.CHAT_RESPONSE_MESSAGE:
			case Constants.ERROR_MESSAGE:
			case Constants.HOLDEM_GAME_MESSAGE:
			case Constants.INITIALIZATION_MESSAGE:
			default:
				break;	
			}
			break;
		case PLAYING:
			switch (messageType)
			{
			case Constants.CHAT_MESSAGE:
			case Constants.CHAT_RESPONSE_MESSAGE:
			case Constants.ERROR_MESSAGE:
			case Constants.HOLDEM_GAME_MESSAGE:
			case Constants.INITIALIZATION_MESSAGE:
			case Constants.JOIN_ROOM_MESSAGE:
			default:
				break;	
			}
			break;
		case ACTION_REQUIRED:
			switch (messageType)
			{
			case Constants.AVAILABLE_ROOMS_MESSAGE:
			case Constants.CHAT_MESSAGE:
			case Constants.CHAT_RESPONSE_MESSAGE:
			case Constants.ERROR_MESSAGE:
			case Constants.HOLDEM_GAME_MESSAGE:
			case Constants.INITIALIZATION_MESSAGE:
			case Constants.JOIN_ROOM_MESSAGE:
			default:
				break;	
			}
			break;
		case OUT:
			switch (messageType)
			{			
			case Constants.AVAILABLE_ROOMS_MESSAGE:
			case Constants.CHAT_MESSAGE:
			case Constants.CHAT_RESPONSE_MESSAGE:
			case Constants.ERROR_MESSAGE:
			case Constants.HOLDEM_GAME_MESSAGE:
			case Constants.INITIALIZATION_MESSAGE:
			case Constants.JOIN_ROOM_MESSAGE:
			default:
				break;	
			}
			break;
			
		}

		return expected;
	}
	@Deprecated
	public State getState()
	{
		return state;
	}
	State state;
}
