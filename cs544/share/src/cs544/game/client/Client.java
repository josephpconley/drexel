package cs544.game.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cs544.game.holdem.Hand;
import cs544.game.messages.AbstractMessage;
import cs544.game.messages.AvailableRoomsMessage;
import cs544.game.messages.ChatMessage;
import cs544.game.messages.ChatResponseMessage;
import cs544.game.messages.HoldemGameMessage;
import cs544.game.messages.InitializationMessage;
import cs544.game.messages.JoinRoomMessage;
import cs544.game.messages.AvailableRoomsMessage.AvailableRoom;
import cs544.game.messages.holdem.CardUpdateMessage;
import cs544.game.messages.holdem.GameEndedMessage;
import cs544.game.messages.holdem.GameResponseMessage;
import cs544.game.messages.holdem.GameResultMessage;
import cs544.game.messages.holdem.LeaveGameMessage;
import cs544.game.messages.holdem.StateUpdateMessage;
import cs544.game.util.Constants;
/*
 * This file represents a Client in a Texas HoldEm Game.
 */
public class Client implements ActionListener{
	
	JFrame frame;
	JPanel chatPanel;
	JPanel gamePanel;
	JPanel outPanel;
	JPanel roomPanel;
	JPanel buttonPanel;
	JTextField chatInput;
	JTextField gameInput;
	JTextArea outputText;
	JTextArea roomText;
	JButton chatButton;
	JButton betButton;
	JButton checkButton;
	JButton foldButton;
	JButton quitButton;
	JButton createButton;
	JButton joinButton;
	ArrayList<JRadioButton> rooms;
	ButtonGroup group;
	Hand hand;
	
	public Client() throws IOException {
		this(Constants.DEFAULT_ADDR, Constants.DEFAULT_PORT);
	}

	/**
	 * Constructs a client based on the given address, port
	 */
	public Client(String address, int port) throws IOException {
		try {
			this.socket = new Socket(address, port);
            this.output = new DataOutputStream(this.socket.getOutputStream());
		} catch (IOException e) {
			System.err.format("Cannot connect to %s:%s", address, port);
			throw e;
		}

		frame = new JFrame();
		frame.setSize(800,500);
		frame.setTitle("Texas Hold'em");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		chatPanel = new JPanel();
		chatInput = new JTextField(40);
		chatInput.requestFocus();
		chatButton = new JButton("Send");
		chatButton.addActionListener(this);
		chatPanel.add(new JLabel("Chat:"));
		chatPanel.add(chatInput);
		chatPanel.add(chatButton);
		
		gamePanel = new JPanel();
		gamePanel.setPreferredSize(new Dimension(175,0));
		gameInput = new JTextField(10);

		checkButton = new JButton("Check");		checkButton.addActionListener(this);
		betButton = new JButton("Bet");				betButton.addActionListener(this);
		foldButton = new JButton("Fold");		foldButton.addActionListener(this);
		quitButton = new JButton("Quit");		quitButton.addActionListener(this);
		joinButton = new JButton("Join");		joinButton.addActionListener(this);
		createButton = new JButton("Create");		createButton.addActionListener(this);
		
		roomPanel = new JPanel();
		roomPanel.setLayout(new BoxLayout(roomPanel,BoxLayout.PAGE_AXIS));
		rooms = new ArrayList<JRadioButton>();
		group = new ButtonGroup();
		buttonPanel = new JPanel();
		roomPanel.add(new JLabel("Available Hold 'em rooms:"));
		roomPanel.add(buttonPanel);
		roomPanel.add(createButton);
		roomPanel.add(joinButton);
		
		gamePanel.add(new JLabel("Bet:"));
		gamePanel.add(gameInput);
		gamePanel.add(checkButton);
		gamePanel.add(betButton);
		gamePanel.add(foldButton);
		
		outPanel = new JPanel();
		outPanel.setBackground(Color.green);
		outputText = new JTextArea(25,50);
		outputText.setEditable(false);
		outPanel.add(outputText);
		
		frame.getContentPane().add(BorderLayout.CENTER,new JScrollPane(outPanel));
		frame.getContentPane().add(BorderLayout.SOUTH,chatPanel);
		frame.getContentPane().add(BorderLayout.EAST,roomPanel);
		frame.setVisible(true);

        this.initUserName();
        hand = new Hand();
	}

	/**
	 * Initializes the Username by interacting with the GUI
	 */
    private void initUserName() {
        this.userName = null;
        while (this.userName == null) {
            this.userName = JOptionPane.showInputDialog(frame, "Choose a user name");
        }
    }

    /**
     * Initializes the Game
     */
    private void sendInitializationMessage() {
        InitializationMessage message = new InitializationMessage(userName);
        try {
            message.send(this.output);
        } catch (IOException e) {
            this.stop();
        }
    }

	protected void finalize() throws Throwable {
		this.stop();
		super.finalize();
	}

	public void start() throws IOException {
        this.sendInitializationMessage();
        Thread recvThread = new Thread(new ReceiveThread(this.socket,this));
        recvThread.setDaemon(true);
        recvThread.start();
	}

	public void stop() {
		try {
			this.socket.close();
		} catch (IOException e) {}
	}

	public static void main(String[] args){
		try {
			switch (args.length) {
			case 2:
				new Client(args[0], Integer.valueOf(args[1])).start();
				break;
			case 1:
				new Client(args[0], Constants.DEFAULT_PORT).start();
				break;
			default:
				new Client().start();
				break;
			}
		} catch (IOException e) {
            e.printStackTrace();
			System.err.println("Error!");
		}
	}

	/**
	 * Handles any button-clicks by user, sends appropriate message
	 */
	public void actionPerformed(ActionEvent evt){
		if(evt.getSource() == chatButton){
	        try {
		        ChatMessage message = new ChatMessage();
				
				if(this.running){
		            String line = chatInput.getText();
		            if (line == null) {
		                return;
		            }
		            message.setMessage(line);
		            message.send(this.output);
                    this.outputText.append(String.format("[%s]: %s\n",
                                this.userName, line));
				}
				chatInput.setText(null);
	        } catch(Exception e) {
	        	e.printStackTrace();
	        }
		}else if(evt.getSource() == betButton){
			try{
				GameResponseMessage message = new GameResponseMessage(Constants.BET,
																Short.valueOf(gameInput.getText()));
				message.send(this.output);
				gameInput.setText(null);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(evt.getSource() == checkButton){
			GameResponseMessage message = new GameResponseMessage(Constants.CHECK,(short)0);
			try {
				message.send(this.output);
			} catch (IOException e) {
				e.printStackTrace();
			}
			gameInput.setText(null);
		}else if(evt.getSource() == foldButton){
			GameResponseMessage message = new GameResponseMessage(Constants.FOLD,(short)-1);
			try {
				message.send(this.output);
			} catch (IOException e) {
				e.printStackTrace();
			}
			gameInput.setText(null);
		}else if(evt.getSource() == quitButton){
			LeaveGameMessage message = new LeaveGameMessage();
			try {
				message.send(this.output);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(evt.getSource() == createButton){
			JoinRoomMessage message = new JoinRoomMessage(Constants.HOLDEM_GAME_MESSAGE,(short)0);
			try {
				message.send(this.output);
				
				//If no error thrown, we're connected
				showGamePanel();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(evt.getSource() == joinButton){
			short index = Short.parseShort(group.getSelection().getActionCommand());
			JoinRoomMessage message = new JoinRoomMessage(Constants.HOLDEM_GAME_MESSAGE,index);
			try {
				message.send(this.output);
				
				//If no error thrown, we're connected
				showGamePanel();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void showGamePanel(){
		frame.getContentPane().remove(roomPanel);
		frame.getContentPane().add(BorderLayout.EAST,gamePanel);
		frame.getContentPane().validate();
	}
	
	private boolean running = true;
	private final Socket socket;
    private final DataOutputStream output;
    private String userName;
}

/**
 * Handles the receiving of messages from the server, prints appropriate output to main window
 */
class ReceiveThread extends Thread {
	public ReceiveThread(Socket socket, Client client) {
		super("ReceiveThread");
		this.socket = socket;
		this.client = client;
	}

	public void run() {
		DataInputStream input;
		try {
			input = new DataInputStream(this.socket.getInputStream());
		} catch (IOException e) {
			return;
		}
		AbstractMessage message;
		while (true) {
			try {
				message = AbstractMessage.receive(input);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}

            switch (message.getMessageType()) {
            case Constants.AVAILABLE_ROOMS_MESSAGE:
            	this.handleAvailableRoomsMessage((AvailableRoomsMessage)message);
            	break;
            case Constants.CHAT_RESPONSE_MESSAGE:
                this.handleChatResponseMessage((ChatResponseMessage)message);
                break;
            case Constants.HOLDEM_GAME_MESSAGE:
            	this.handleHoldemGameMessage((HoldemGameMessage)message);
            	break;
            default:
                System.err.format("Unknown message type: %s\n", message.getMessageType());
            }
		}
	}

    private void handleAvailableRoomsMessage(AvailableRoomsMessage message) {
		this.client.buttonPanel.removeAll();
		for(JRadioButton b : this.client.rooms){
			this.client.group.remove(b);
		}
		this.client.rooms.clear();
		
		int i=0;
    	for(AvailableRoom r : message.getRooms()){
    		this.client.rooms.add(new JRadioButton("Holdem Room " + r.index));
    		this.client.rooms.get(i).setActionCommand(Short.toString(r.index));
    		this.client.group.add(this.client.rooms.get(i));
    		this.client.buttonPanel.add(this.client.rooms.get(i));
		}
    	this.client.buttonPanel.validate();
	}

	private void handleChatResponseMessage(ChatResponseMessage message) {
        this.client.outputText.append(String.format("[%s]: %s\n",
                    message.getUserName(), message.getMessage()));
    }

    private void handleHoldemGameMessage(HoldemGameMessage message){
    	int messageType = message.getGameMessageType();
    	System.out.println("Received HoldEmGameMessage: " + messageType);
    	switch(messageType){
    	case Constants.STATE_UPDATE_MESSAGE:
    		this.handleStateUpdateMessage((StateUpdateMessage) message);
    		break;
    	case Constants.CARD_UPDATE_MESSAGE:
    		this.handleCardUpdateMessage((CardUpdateMessage)message);
    		break;
    	case Constants.GAME_RESULT_MESSAGE:
    		this.handleGameResultMessage((GameResultMessage)message);
    		break;
    	case Constants.GAME_ENDED_MESSAGE:
    		this.handleGameEndedMessage((GameEndedMessage)message);
    		break;     		
    	default:
    		System.err.format("Unknown game message type: %s\n", message.getGameMessageType());
    	}
    }
    
	private void handleStateUpdateMessage(StateUpdateMessage message) {
		short messageType = message.getStateUpdate();
		System.out.println("Received State Update Message: " + messageType);
		
        switch(messageType){
        case Constants.YOUR_TURN:
        	this.client.outputText.append(String.format("[%s]: %s\n","SERVER", 
        										message.getUsername() + ", it is your turn."));
        	break;
        case Constants.BID_UPDATE:
        	short bet = message.getChipAmount();
        	if(bet < 0){
        		this.client.outputText.append(String.format("[%s] %s\n",message.getUsername(), 
						"folds."));
        	}else if(bet > 0){
        		this.client.outputText.append(String.format("[%s] %s\n",message.getUsername(), 
						"bets " + message.getChipAmount()));
        	}else{
        		this.client.outputText.append(String.format("[%s] %s\n",message.getUsername(), 
												"checks "));
        	}
        	break;
        case Constants.PLAYER_JOINS:
        	this.client.outputText.append(String.format("[%s] %s\n",message.getUsername(), 
												"has joined the game"));
        	break;
        case Constants.PLAYER_LEAVES:
        	this.client.outputText.append(String.format("[%s] %s\n",message.getUsername(), 
												"has left the game"));
        	break;        	
        default:
        	System.err.format("Unknown state update type: %s\n", message.getStateUpdate());
        }
    	
    }
    
    private void handleCardUpdateMessage(CardUpdateMessage message){
    	System.out.println("Received Card: "+ message.getCard());
    	this.client.hand.addCard(message.getCard());
    	//Below is not the best solution...
        ChatResponseMessage chatMessage = new ChatResponseMessage(
                "HOLDEM", String.format("Received Card: '%s'", message.getCard()));
        this.handleChatResponseMessage(chatMessage);
    }
    
    private void handleGameResultMessage(GameResultMessage message) {
    	short pot = message.getResultValue();
    	switch(message.getResult()){
    	case Constants.WON:
    		this.client.outputText.append(String.format("%s\n", "You won " + pot + " with " + message.getWinningHand()));
    	case Constants.LOST:
    		this.client.outputText.append(String.format("%s\n", "You lost with " + message.getWinningHand()));    		
    	}
    }
    
    private void handleGameEndedMessage(GameEndedMessage message) {
	}
    
    private final Socket socket;
	private final Client client;
}
