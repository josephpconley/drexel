package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import banking.Card;
import banking.Message;
import banking.Money;

public class MessageTest {

	private static final int VALID_CARD = 1;
	private static final Card validCard = new Card(VALID_CARD);
	private static final int VALID_PIN = 42;	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(100);
		
		Message msg = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		assertTrue(msg.getMessageCode() == msgType);
		assertTrue(msg.getCard().getNumber() == VALID_CARD);
		assertTrue(msg.getPIN() == VALID_PIN);
		assertTrue(msg.getSerialNumber() == serialNumber);
		assertTrue(msg.getFromAccount() == fromAcct);
		assertTrue(msg.getToAccount() == toAcct);
		assertTrue(msg.getAmount().getCents() == m.getCents());
	}
	
	@Test
	public void testWithdrawalToString(){
		//withdrawal
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(100);
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		
		StringBuilder sb = new StringBuilder();
		sb.append("WITHDRAW");
        sb.append(" CARD# ");
        sb.append(Integer.toString(validCard.getNumber()));
        sb.append(" TRANS# ");
        sb.append(Integer.toString(serialNumber));
        sb.append(" FROM  ");
        sb.append(Integer.toString(fromAcct));
        sb.append(" NO TO");
        sb.append(" " + m);
		
        //System.out.println(msg1.toString());
        //System.out.println(sb.toString());
        assertTrue(msg1.toString().equals(sb.toString()));		
	}
	
	@Test
	public void testInitDepositToString(){
        //init deposit
		int msgType = Message.INITIATE_DEPOSIT;
		int serialNumber = 0;
		int fromAcct = -1;
		int toAcct = 1;
		Money m = new Money(5000);
		Message msg2 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		
		StringBuilder sb = new StringBuilder();
		sb.append("INIT_DEP");
        sb.append(" CARD# ");
        sb.append(Integer.toString(validCard.getNumber()));
        sb.append(" TRANS# ");
        sb.append(Integer.toString(serialNumber));
        sb.append(" NO FROM");
        sb.append(" TO  ");
        sb.append(Integer.toString(toAcct));
        sb.append(" " + m);
		
        //System.out.println(msg2.toString());
        //System.out.println(sb.toString());
        assertTrue(msg2.toString().equals(sb.toString())); 		
	}
	
	@Test
	public void testCompleteDepositToString(){
        //complete deposit
		int msgType = Message.COMPLETE_DEPOSIT;
		int serialNumber = 0;
		int fromAcct = -1;
		int toAcct = 1;
		Money m = new Money(20);
		Message msg3 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		
		StringBuilder sb = new StringBuilder();
		sb.append("COMP_DEP");
        sb.append(" CARD# ");
        sb.append(Integer.toString(validCard.getNumber()));
        sb.append(" TRANS# ");
        sb.append(Integer.toString(serialNumber));
        sb.append(" NO FROM");
        sb.append(" TO  ");
        sb.append(Integer.toString(toAcct));
        sb.append(" " + m);
		
        //System.out.println(msg3.toString());
        //System.out.println(sb.toString());
        assertTrue(msg3.toString().equals(sb.toString()));		
	}

	@Test
	public void testTransferToString(){
        //transfer
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int fromAcct = 1;
		int toAcct = 2;
		Money m = new Money(1000);
		Message msg4 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		
		StringBuilder sb = new StringBuilder();
		sb.append("TRANSFER");
        sb.append(" CARD# ");
        sb.append(Integer.toString(validCard.getNumber()));
        sb.append(" TRANS# ");
        sb.append(Integer.toString(serialNumber));
        sb.append(" FROM  ");
        sb.append(Integer.toString(fromAcct));
        sb.append(" TO  ");
        sb.append(Integer.toString(toAcct));
        sb.append(" " + m);
		
        //System.out.println(msg4.toString());
        //System.out.println(sb.toString());
        assertTrue(msg4.toString().equals(sb.toString()));		
	}
	
	@Test
	public void testInquiryToString(){

		int msgType = Message.INQUIRY;
		int serialNumber = 0;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(0);
		Message msg5 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		
		StringBuilder sb = new StringBuilder();
		sb.append("INQUIRY ");
        sb.append(" CARD# ");
        sb.append(Integer.toString(validCard.getNumber()));
        sb.append(" TRANS# ");
        sb.append(Integer.toString(serialNumber));
        sb.append(" FROM  ");
        sb.append(Integer.toString(fromAcct));
        sb.append(" NO TO");
        sb.append(" NO AMOUNT");
		
        //System.out.println(msg5.toString());
        //System.out.println(sb.toString());
        assertTrue(msg5.toString().equals(sb.toString()));        
	}
	
	@Test 
	public void testSetPIN(){
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(100);
		
		Message msg = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		assertTrue(msg.getPIN() == 42);

		int newPIN = 999;
		msg.setPIN(newPIN);
		assertTrue(msg.getPIN() == newPIN);
	}
}
