package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simulation.Simulation;
import atm.ATM;
import atm.physical.CashDispenser;
import atm.physical.Log;
import banking.Money;

public class CashDispenserTest{

	@Before
	public void setUp() throws Exception {
		ATM atm = new ATM(1, "Conley College", "Bank of Joe", null /* We're not really talking to a bank! */);
		Simulation sim = new Simulation(atm);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		Log log = new Log();
		CashDispenser cd = new CashDispenser(log);
		
		assertTrue(cd.checkCashOnHand(new Money(0)));
		assertNotNull(cd.getLog());
	}
	
	@Test
	public void testCash(){
		CashDispenser cd = new CashDispenser(new Log());
		
		Money m = new Money(5000);
		cd.setInitialCash(m);
		
		assertTrue(cd.checkCashOnHand(new Money(5000)));
		assertFalse(cd.checkCashOnHand(new Money(5001)));
	}
	
	@Test
	public void testSufficientDispenseCash(){
		Log log = new Log();
		CashDispenser cd = new CashDispenser(log);
		cd.setInitialCash(new Money(5000));
		
		//testing the gui piece
		Money m = new Money(1000);
		try{
			cd.dispenseCash(m);	
		}catch(Exception e){
			e.printStackTrace();
			fail("Cash dispensing failed");
		}
		
		assertTrue(cd.checkCashOnHand(new Money(4000)));
		assertFalse(cd.checkCashOnHand(new Money(4001)));
		
		//test log
		String logMsg = "Dispensed: " + m.toString() + "\n";
		String guiMsg = Simulation.getInstance().getLogPanelContents();
		
//		System.out.println(logMsg);
//		System.out.println(guiMsg);
		assertTrue(logMsg.equals(guiMsg));
	}
	
	@Test
	public void testInsufficientDispenseCash(){
		Log log = new Log();
		CashDispenser cd = new CashDispenser(log);
		
		//testing the gui piece
		Money m = new Money(1000);
		try{
			cd.dispenseCash(m);
			fail("Cash dispensing should have failed");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
