package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Label;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simulation.Simulation;
import atm.ATM;
import atm.physical.CustomerConsole.Cancelled;
import banking.Money;

public class CustomerConsoleTest {

	private static ATM atm =  null;
	private static boolean passed;
	private static String[] menu = new String[]{"one", "two", "three"};
	private static final int GOOD_CHOICE = 1;
	
	@Before
	public void setUp() throws Exception {
		atm = new ATM(1, "Conley College", "Bank of Joe", null /* We're not really talking to a bank! */);
		Simulation sim = new Simulation(atm);		
	}

	@After
	public void tearDown() throws Exception {
		atm = null;
	}

	@Test
	public void testDisplay() {
		String msg = "Test display";
		atm.getCustomerConsole().display(msg);
		
		Label[] display = Simulation.getInstance().getDisplay();
		assertTrue(display[0].getText().equals(msg));
	}

	@Test
	public void testNullPIN(){
		Thread helper = new Thread(new ReadPINHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		//can't enter a null input as thread won't wake up, this is only other workaround to test throwing of Cancelled exception
		Simulation.getInstance().pressCancelKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);
	}		
	
	@Test
	public void testBadPIN(){
		Thread helper = new Thread(new ReadPINHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		Simulation.getInstance().pressDigitKey(9);
		Simulation.getInstance().pressDigitKey(9);
		Simulation.getInstance().pressDigitKey(9);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);
	}	
	
	@Test
	public void testGoodPIN(){
		Thread helper = new Thread(new ReadPINHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}
		
		Simulation.getInstance().pressDigitKey(4);
		Simulation.getInstance().pressDigitKey(2);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertTrue(passed);
	}
	
	@Test
	public void testCancelMenuChoice(){
		Thread helper = new Thread(new ReadMenuHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		//can't enter a null input as thread won't wake up, this is only other workaround to test throwing of Cancelled exception
		Simulation.getInstance().pressCancelKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);		
	}		
	
	@Test
	public void testBadReadMenuChoice(){
		Thread helper = new Thread(new ReadMenuHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		int badChoice = 2;
		Simulation.getInstance().pressDigitKey(badChoice);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);		
	}	
	
	@Test
	public void testGoodReadMenuChoice(){
		Thread helper = new Thread(new ReadMenuHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		Simulation.getInstance().pressDigitKey(GOOD_CHOICE);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertTrue(passed);		
	}

	@Test
	public void testNullReadAmount(){
		Thread helper = new Thread(new ReadAmountHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		//can't enter a null input as thread won't wake up, this is only other workaround to test throwing of Cancelled exception
		Simulation.getInstance().pressCancelKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);		
	}	
	
	@Test
	public void testBadReadAmount(){
		Thread helper = new Thread(new ReadAmountHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		Simulation.getInstance().pressDigitKey(5);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertFalse(passed);		
	}	
	
	@Test
	public void testGoodReadAmount(){
		Thread helper = new Thread(new ReadAmountHelper());
		helper.start();
		
		int i = 1000000000;
		while(i>0){
			i--;
		}

		//inserting $50 in cents, the amount expected by the helper thread
		Simulation.getInstance().pressDigitKey(5);
		Simulation.getInstance().pressDigitKey(0);
		Simulation.getInstance().pressDigitKey(0);
		Simulation.getInstance().pressDigitKey(0);
		Simulation.getInstance().pressEnterKey();
		
		try{
			helper.join();
		}catch(InterruptedException e){
			System.out.println("InterruptedException");
		}
		
		assertTrue(passed);		
	}	
	
	private class ReadAmountHelper implements Runnable{

		@Override
		public void run() {
			Money m;
			try{
				m = atm.getCustomerConsole().readAmount("Get amount");
				passed = (m.getCents() == 5000);
			}catch(Cancelled e){
				passed = false;
			}
		}
	}		
	
	private class ReadMenuHelper implements Runnable{

		@Override
		public void run() {
			int choice =  0;
			try{
				choice = atm.getCustomerConsole().readMenuChoice("Need Menu Choice", menu);
				passed = (choice == GOOD_CHOICE - 1);
			}catch(Cancelled e){
				passed = false;
			}
		}
	}	
	
	private class ReadPINHelper implements Runnable{

		@Override
		public void run() {
			int pin =  0;
			try{
				pin = atm.getCustomerConsole().readPIN("Need PIN");
				passed = (pin == 42);
			}catch(Cancelled e){
				passed = false;
			}
		}
	}
}


