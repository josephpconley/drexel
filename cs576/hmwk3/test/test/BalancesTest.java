package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import banking.Balances;
import banking.Money;

public class BalancesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetBalances() {
		Money total = new Money(2500);
		Money avail = new Money(1500);

		Balances b = new Balances();
		b.setBalances(total, avail);
		
		assertTrue(b.getTotal().getCents() == total.getCents());
		assertTrue(b.getAvailable().getCents() == avail.getCents());
	}

}
