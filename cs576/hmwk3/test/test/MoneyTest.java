package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import banking.Money;

public class MoneyTest  {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDollars() {
		int dollars = 10;
		
		Money m = new Money(dollars);
		Long cents = new Long(m.getCents());
		assertTrue(cents.intValue() == (100L * dollars));
	}
	
	@Test
	public void testDollarsAndCents(){
		int dollars = 20;
		int cents = 50;
		
		Money m = new Money(dollars, cents);
		assertTrue(m.getCents() == (100L * dollars + cents));
	}
	
	@Test
	public void testNegativeMoney(){
		int dollars = -5;
		try{
			Money m = new Money(dollars);
			fail("Negative money should not be allowed!");
		}catch(Exception e){
		}
	}
	
	@Test
	public void testCopy(){
		Money copy = new Money(-5);
		Money m = new Money(copy);
		assertTrue(m.getCents() == copy.getCents());
	}
	
	@Test
	public void testToString(){
		int dollars = 55;
		long cents = 100L * dollars;
		String str = "$" + cents/100 + (cents %100 >= 10  ? "." + cents % 100 : ".0" + cents % 100);
		
		Money m = new Money(dollars);
		assertTrue(str.equals(m.toString()));
	}
	
	@Test
	public void testAdd(){
		int dollars = 26;
		int cents = 88;
		Money m = new Money(dollars, cents);
		long origCents = m.getCents();
		
		int dol = 12;
		int cent = 12;
		Money toAdd = new Money(dol, cent);
		long addCents = toAdd.getCents();
		
		m.add(toAdd);
		assertTrue((origCents + addCents) == m.getCents());
	}
	
	@Test
	public void testSubtract(){
		int dollars = 1234;
		int cents = 19;
		Money m = new Money(dollars, cents);
		long origCents = m.getCents();
		
		int dol = 8970;
		int cent = 47;
		Money toSub = new Money(dol, cent);
		long subCents = toSub.getCents();
		
		m.subtract(toSub);
		assertTrue((origCents - subCents) == m.getCents());		
	}
	
	@Test
	public void testLessEqual(){
		int dollars = 7091;
		int cents = 89;
		Money m = new Money(dollars, cents);
		
		int dol = 710;
		int cent = 18;
		Money m2 = new Money(dol, cent);

		assertTrue(m2.lessEqual(m));
	}	
}
