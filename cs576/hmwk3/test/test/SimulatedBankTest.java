package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simulation.SimulatedBank;
import simulation.Simulation;
import atm.ATM;
import banking.Balances;
import banking.Card;
import banking.Message;
import banking.Money;
import banking.Status;

public class SimulatedBankTest  {

	private static final int VALID_CARD = 1;
	private static final Card validCard = new Card(VALID_CARD);
	private static final int VALID_PIN = 42;
	
	@Before
	public void setUp() throws Exception {
		ATM atm = new ATM(1, "Conley College", "Bank of Joe", null /* We're not really talking to a bank! */);
		Simulation sim = new Simulation(atm);
		//Deposit d = new Deposit(atm, new Session(atm), validCard, VALID_PIN);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHandleMessageInvalidCard() {
		//withdrawal
		int msgType = Message.WITHDRAWAL;
		int invalidCard = 999;
		Card c = new Card(invalidCard);
		int serialNumber = 1;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(100);
		
		Message msg1 = new Message(msgType, c, VALID_PIN, serialNumber, fromAcct, toAcct, m);	
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg1, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Invalid card"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());
	}
	
	@Test
	public void testHandleMessageInvalidPIN() {
		//withdrawal
		int msgType = Message.WITHDRAWAL;
		int invalidPin = 999;
		int serialNumber = 1;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(100);
		
		Message msg1 = new Message(msgType, validCard, invalidPin, serialNumber, fromAcct, toAcct, m);	
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg1, new Balances());
		
		assertTrue(s.toString().equals("INVALID PIN"));
		assertFalse(s.isSuccess());
		assertTrue(s.isInvalidPIN());
	}
	
	@Test
	public void testWithdrawalInvalidAcct(){
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int invalidAcct = 2;
		int toAcct = -1;
		Money m = new Money(100);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, invalidAcct, toAcct, m);
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg1, new Balances());

		assertTrue(s.toString().equals("FAILURE Invalid account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());
	}
	
	@Test
	public void testWithdrawalDailyLimitExceeded(){
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int validAcct = 1;
		int toAcct = -1;
		Money tooMuchMoney = new Money(500);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, validAcct, toAcct, tooMuchMoney);
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg1, new Balances());

		assertTrue(s.toString().equals("FAILURE Daily withdrawal limit exceeded"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());			
	}

	@Test
	public void testWithdrawalInsufficientBalance(){
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int checkingAcct = 0;
		int toAcct = -1;
		Money tooMuchMoney = new Money(200);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, checkingAcct, toAcct, tooMuchMoney);
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg1, new Balances());

		assertTrue(s.toString().equals("FAILURE Insufficient available balance"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());		
	}
	
	@Test
	public void testGoodWithdrawal(){
		int msgType = Message.WITHDRAWAL;
		int serialNumber = 1;
		int savingsAcct = 1;
		int toAcct = -1;
		Money m = new Money(200);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, savingsAcct, toAcct, m);
		Balances b = new Balances();
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, b);

		assertTrue(s.toString().equals("SUCCESS"));
		assertTrue(s.isSuccess());
		assertFalse(s.isInvalidPIN());	
		
		//verify transaction worked as intended
		Money savings = new Money(1000);
		savings.subtract(m);
		
		assertTrue(sb.getWithdrawalsToday()[VALID_CARD].getCents() == m.getCents());
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][savingsAcct]].getCents() == savings.getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][savingsAcct]].getCents() == savings.getCents());
		
		//test Balances object
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][savingsAcct]].getCents() == b.getTotal().getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][savingsAcct]].getCents() == b.getAvailable().getCents());
	}
	
	@Test
	public void testInitiateDepositInvalidAcct(){
		int msgType = Message.INITIATE_DEPOSIT;
		int serialNumber = 0;
		int fromAcct = -1;
		int invalidAcct = 2;
		Money m = new Money(5000);

		Message msg2 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, invalidAcct, m);
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg2, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Invalid account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());		
	}
	
	@Test
	public void testGoodInitiateDeposit(){
		int msgType = Message.INITIATE_DEPOSIT;
		int serialNumber = 0;
		int fromAcct = -1;
		int toAcct = 1;
		Money m = new Money(5000);

		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		Balances b = new Balances();
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, b);		

		assertTrue(s.toString().equals("SUCCESS"));
		assertTrue(s.isSuccess());
		assertFalse(s.isInvalidPIN());			
	}
	
	@Test
	public void testCompleteDepositInvalidAcct(){
		int msgType = Message.COMPLETE_DEPOSIT;
		int serialNumber = 0;
		int fromAcct = -1;
		int invalidAcct = 2;
		Money m = new Money(5000);

		Message msg2 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, invalidAcct, m);
		Status s = Simulation.getInstance().getSimulatedBank().handleMessage(msg2, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Invalid account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());			
	}
	
	@Test
	public void testGoodCompleteDeposit(){
		int msgType = Message.COMPLETE_DEPOSIT;
		int serialNumber = 1;
		int fromAcct = -1;
		int checkingAcct = 0;
		Money m = new Money(900);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, checkingAcct, m);
		Balances b = new Balances();
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, b);

		assertTrue(s.toString().equals("SUCCESS"));
		assertTrue(s.isSuccess());
		assertFalse(s.isInvalidPIN());	
		
		//verify transaction worked as intended
		Money checking = new Money(100);
		checking.add(m);
		
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][checkingAcct]].getCents() == checking.getCents());
		//assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[validCard][checkingAcct]].getCents() == checking.getCents());
		
		//test Balances object
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][checkingAcct]].getCents() == b.getTotal().getCents());
		//assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[validCard][checkingAcct]].getCents() == b.getAvailable().getCents());		
	}
	
	@Test
	public void testTransferInvalidFrom(){
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int invalidAcct = 2;
		int toAcct = 1;
		Money m = new Money(1000);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, invalidAcct, toAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Invalid from account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());
	}
	
	@Test
	public void testTransferInvalidTo(){
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int invalidAcct = 2;
		int fromAcct = 1;
		Money m = new Money(1000);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, invalidAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Invalid to account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());
	}
	
	@Test
	public void testTransferSameAcct(){
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int acct = 1;
		Money m = new Money(1000);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, acct, acct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, new Balances());
		
		assertTrue(s.toString().equals("FAILURE Can't transfer money from\nan account to itself"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());		
	}
	
	@Test
	public void testTransferInsufficientBalance(){
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int toAcct = 0;
		int fromAcct = 1;
		Money m = new Money(100000);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Status s = sb.handleMessage(msg1, new Balances());		
		
		assertTrue(s.toString().equals("FAILURE Insufficient available balance"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());			
	}
	
	@Test
	public void testGoodTransfer(){
		int msgType = Message.TRANSFER;
		int serialNumber = 0;
		int toAcct = 0;
		int fromAcct = 1;
		Money m = new Money(200);
		
		Message msg1 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Balances b = new Balances();
		Status s = sb.handleMessage(msg1, b);	
		
		assertTrue(s.toString().equals("SUCCESS"));
		assertTrue(s.isSuccess());
		assertFalse(s.isInvalidPIN());
		
		//verify transaction worked as intended
		Money checking = new Money(100);
		Money savings = new Money(1000);
		checking.add(m);
		savings.subtract(m);
		
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][toAcct]].getCents() == checking.getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][toAcct]].getCents() == checking.getCents());
		
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == savings.getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == savings.getCents());
		
		//test Balances object
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][toAcct]].getCents() == b.getTotal().getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][toAcct]].getCents() == b.getAvailable().getCents());		
	}
	
	@Test
	public void testInquiryInvalidAcct(){
		int msgType = Message.INQUIRY;
		int serialNumber = 0;
		int fromAcct = 2;
		int toAcct = -1;
		Money m = new Money(0);
		
		Message msg5 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Balances b = new Balances();
		Status s = sb.handleMessage(msg5, b);
		
		assertTrue(s.toString().equals("FAILURE Invalid account type"));
		assertFalse(s.isSuccess());
		assertFalse(s.isInvalidPIN());			
	}
	
	@Test
	public void testGoodInquiry(){
		int msgType = Message.INQUIRY;
		int serialNumber = 0;
		int fromAcct = 1;
		int toAcct = -1;
		Money m = new Money(0);
		
		Message msg5 = new Message(msgType, validCard, VALID_PIN, serialNumber, fromAcct, toAcct, m);
		SimulatedBank sb = Simulation.getInstance().getSimulatedBank();
		Balances b = new Balances();
		Status s = sb.handleMessage(msg5, b);
		
		assertTrue(s.toString().equals("SUCCESS"));
		assertTrue(s.isSuccess());
		assertFalse(s.isInvalidPIN());
		
		Money savings = new Money(1000);
		
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == savings.getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == savings.getCents());
		
		assertTrue(sb.getBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == b.getTotal().getCents());
		assertTrue(sb.getAvailableBalance()[sb.getAccountNumber()[VALID_CARD][fromAcct]].getCents() == b.getAvailable().getCents());
	}
}
