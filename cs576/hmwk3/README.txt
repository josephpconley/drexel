Assignment 3 - White-box Testing

NOTES

Using source code from Assignment 1
Added junit-4.9 jar to project.  Saw some bugs in using both "extends TestCase" and annotations, so switched to annotation-only mode.

STRUCTURE

The base source code from assignment 1 is located in the base directory in the src folder.
The junit code is located in the base directory in the test folder, all underneath a test package.  
The junit test results are generated in the junit directory.
The html coverage report is located in the base directory in the coverage folder.


INSTRUCTIONS

1. To run the tests without instrumentation, using ant, type
ant test
2. To run the tests with instrumentation, type
ant emmatest
3. To run the simulator without instrumentation, type
ant run
4. To cleanup, type
ant clean
5. To create the ATM jar, type
ant jar

CHANGES TO CODEBASE

For the most part, I added getters to facilitate testing.  For the SimKeyboard testing, I did change the visibility of the keyPress methods.  However, as a rule, I tried first to add getters rather than change the visibility of methods/objects.

PROTOCOLS

None of the six classes tested have any protocols.  There is no state or progression that need be followed to use these classes.  All of their methods are self-contained, and do not rely on any other methods outside of its constructor method.

TEST SUITE OUTCOMES/RESULTS

The summary of the test suits and its results and commentary can be found in White-Box Test Suite.doc