Assignment 2

The directory structure was not altered much for this project.  There is an additional src/test package which contains all of the JUnit test code.

I've used Eclipse to build and test.  As I have a newer version of Eclipse I've used the inherent JUnit 4 functionality to create test configurations to test all code in src/test.

To compile with Emma, I've used the Eclipse plugin EmmaEcl.  This plugin is very easy to use, simply install and run Coverage As > (JUnit 4 configuration) to create a report.  From there it remains to open the EmmaEcl console window, right-click on the desired session, and do Export Report.

The html coverage report is under /dist/coverage.  The PDF documentation is at /dist/A2.pdf.