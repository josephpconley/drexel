package test;

import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import atm.ATM;

public class SystemShutdownTest {

	private static ATM atm;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		atm = null;
	}

	@Test
	public void testSwitchOff() {
		atm.performShutdown();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Not currently available"));
	}
}
