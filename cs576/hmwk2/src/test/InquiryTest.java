package test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import atm.ATM;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Transaction.CardRetained;
import atm.transaction.Inquiry;
import banking.Money;

public class InquiryTest {

	private static ATM atm;
	
	//valid accts
	private final int CHECKING_ACCT = 0;
	private final int SAVINGS_ACCT = 1;
	private final static Money initialCash = new Money(200);
	
	@Before
	public void setUp() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
		atm.performStartup(initialCash);
		
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);		
	}

	@After
	public void tearDown() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testStartTransaction(){
		//Inquiry
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.cancelTransaction();
	}
	
	//Note: cannot black-box test choice of accounts as that is implicit in transaction creation
	
	@Test
	public void testInquiry() throws CardRetained, Cancelled{
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
	}
	
	@Test
	public void testCancellation() throws CardRetained, Cancelled{
		Inquiry w = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.cancelTransaction();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Last transaction was cancelled\nWould you like to do another transaction?"));
	}	
}
