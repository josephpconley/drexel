package test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import atm.ATM;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Transaction.CardRetained;
import atm.transaction.Transfer;
import banking.Money;

public class TransferTest {

	private static ATM atm;
	
	//valid accts
	private final int CHECKING_ACCT = 0;
	private final int SAVINGS_ACCT = 1;
	private final static Money initialCash = new Money(200);
	
	@Before
	public void setUp() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
		atm.performStartup(initialCash);
		
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);		
	}

	@After
	public void tearDown() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testStartTransaction(){
		//Transfer
		Transfer d = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(d);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.cancelTransaction();
	}
	
	//Note: cannot black-box test choice of accounts as that is implicit in transaction creation
	
	@Test
	public void testValidAmounts() throws CardRetained, Cancelled{
		Transfer t = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(t);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(SAVINGS_ACCT, CHECKING_ACCT, new Money(10));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$10"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("TRANSFER FROM: SVGS TO: CHKG"));
		
		Transfer t2 = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(t2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(CHECKING_ACCT, SAVINGS_ACCT, new Money(5));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$5"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("TRANSFER FROM: CHKG TO: SVGS"));
		
		Transfer t3 = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(t3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(SAVINGS_ACCT, CHECKING_ACCT, new Money(15));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));		
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$15"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("TRANSFER FROM: SVGS TO: CHKG"));
	}
	
	@Test
	public void testCancellation() throws CardRetained, Cancelled{

		Transfer w = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.cancelTransaction();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Last transaction was cancelled\nWould you like to do another transaction?"));
	}	
}
