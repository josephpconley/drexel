package test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import atm.ATM;
import atm.ATM.InvalidPIN;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Deposit;
import atm.transaction.Inquiry;
import atm.transaction.Transaction.CardRetained;
import atm.transaction.Transfer;
import atm.transaction.Withdrawal;
import banking.Money;

public class SessionTest {

	private static ATM atm;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
		Money m = new Money(12345);
		atm.performStartup(m);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testReadableCard(){
		String VALID_CARD_1 = "1";
		String VALID_CARD_2 = "999";
		String VALID_CARD_3 = "1234";
		assertNull(atm.startSession(VALID_CARD_1));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		assertNull(atm.startSession(VALID_CARD_2));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		assertNull(atm.startSession(VALID_CARD_3));
		atm.performShutdown();
	}
	
	@Test
	public void testUnreadabeCard(){
		String INVALID_CARD_1 = "abc";
		String INVALID_CARD_2 = "-5";
		String INVALID_CARD_3 = "0.2";
		String INVALID_CARD_4 = "?;&";
		
		atm.performStartup(new Money(12345));
		assertTrue(atm.startSession(INVALID_CARD_1).equals("Unable to read card"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		assertTrue(atm.startSession(INVALID_CARD_2).equals("Unable to read card"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		assertTrue(atm.startSession(INVALID_CARD_3).equals("Unable to read card"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		assertTrue(atm.startSession(INVALID_CARD_4).equals("Unable to read card"));
		atm.performShutdown();
	}

	@Test
	public void testPIN() throws InvalidPIN{
		String VALID_CARD_1 = "1";
		int VALID_PIN_1 = 42;
		int VALID_PIN_2 = 5;
		int VALID_PIN_3 = 1234;
		
		atm.startSession(VALID_CARD_1);
		atm.enterPIN(VALID_PIN_1);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		atm.performShutdown();
		
		
		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD_1);
		atm.enterPIN(VALID_PIN_2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD_1);
		atm.enterPIN(VALID_PIN_3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		atm.performShutdown();
	}
	
	@Test
	public void testTransaction() throws CardRetained, Cancelled, InvalidPIN{
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		
		//valid accts
		int CHECKING_ACCT = 0;
		int SAVINGS_ACCT = 1;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);

		//Withdrawal
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(1));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		
		//Deposit
		Deposit d = new Deposit(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(d);
		atm.performTransaction(-1, SAVINGS_ACCT, new Money(150));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		
		//Transfer
		Transfer t = new Transfer(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(t);
		atm.performTransaction(SAVINGS_ACCT, CHECKING_ACCT, new Money(100));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		
		//Balance Inquiry
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		
	}
	
	//Cannot perform "Answer yes" test case as the menu selection functionality is implicit in using children of the Transaction object
	
	@Test
	public void testNoMoreTransactions() throws CardRetained, Cancelled, InvalidPIN{
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		
		//valid accts
		int CHECKING_ACCT = 0;
		
		atm.startSession(VALID_CARD);
		atm.enterPIN(VALID_PIN);
		
		//Withdrawal
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(100));
		
		atm.endSession();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please insert your card"));
	}
}
