package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import atm.ATM;
import atm.ATM.InvalidPIN;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Inquiry;
import atm.transaction.Transaction.CardRetained;
import banking.Money;

public class InvalidPINTest {

	private static ATM atm;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testInvalidPIN() throws CardRetained, Cancelled, InvalidPIN{
		String VALID_CARD = "1";
		int INVALID_PIN_1 = 999;
		int INVALID_PIN_2 = -1;
		int INVALID_PIN_3 = 777;
		int CHECKING_ACCT = 0;

		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD);
		atm.enterPIN(INVALID_PIN_1);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Invalid PIN\nWould you like to do another transaction?"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD);
		try{
			atm.enterPIN(INVALID_PIN_2);
			fail("This method should have thrown an InvalidPIN exception");
		}catch(InvalidPIN e){}
		
		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD);
		atm.enterPIN(INVALID_PIN_3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		
		Inquiry i3 = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i3);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Invalid PIN\nWould you like to do another transaction?"));
		atm.performShutdown();		
	}
	
	@Test
	public void testValidPIN() throws CardRetained, Cancelled, InvalidPIN{
		String VALID_CARD_1 = "1";
		String VALID_CARD_2 = "2";
		int VALID_PIN_1 = 42;
		int VALID_PIN_2 = 1234;
		int CHECKING_ACCT = 0;

		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD_1);
		atm.enterPIN(VALID_PIN_1);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		atm.performShutdown();
		
		atm.performStartup(new Money(12345));
		atm.startSession(VALID_CARD_2);
		atm.enterPIN(VALID_PIN_2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please choose transaction type"));
		
		Inquiry i2 = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i2);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		atm.performShutdown();
	}	
}
