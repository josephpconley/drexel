package test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import atm.ATM;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Transaction.CardRetained;
import atm.transaction.Withdrawal;
import banking.Money;

public class WithdrawalTest {

	private static ATM atm;
	
	//valid accts
	private final int CHECKING_ACCT = 0;
	private final int SAVINGS_ACCT = 1;
	private final static Money initialCash = new Money(200);
	
	@Before
	public void setUp() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
		atm.performStartup(initialCash);
		
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);		
	}

	@After
	public void tearDown() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testStartTransaction(){
		//Withdrawal
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.cancelTransaction();
	}
	
	//Note: cannot test choice of accounts as that is implicit in transaction creation
	
	@Test
	public void testValidAmounts() throws CardRetained, Cancelled{
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(SAVINGS_ACCT, -1, new Money(10));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$10"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("WITHDRAWAL"));
		
		Withdrawal w2 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(CHECKING_ACCT, -1, new Money(5));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$5"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("WITHDRAWAL"));
		
		Withdrawal w3 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		atm.performTransaction(SAVINGS_ACCT, -1, new Money(15));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));		
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("$15"));
		assertTrue(atm.getReceiptPrinter().printReceipt().contains("WITHDRAWAL"));
	}
	
	@Test
	public void testAmountsGreaterThanCashOnHand() throws CardRetained, Cancelled{
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		try{
			atm.performTransaction(SAVINGS_ACCT, -1, new Money(250));
		}catch(Cancelled c){
			assertTrue(c.getMessage().equals("Insufficient cash available\n"));
		}
		
		Withdrawal w2 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		try{
			atm.performTransaction(CHECKING_ACCT, -1, new Money(201));
		}catch(Cancelled c){
			assertTrue(c.getMessage().equals("Insufficient cash available\n"));
		}
		
		Withdrawal w3 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));		

		try{
			atm.performTransaction(CHECKING_ACCT, -1, new Money(265));
		}catch(Cancelled c){
			assertTrue(c.getMessage().equals("Insufficient cash available\n"));
		}
	}
	
	//Can only test checking as savings as an amount which is larger than daily limit
	@Test
	public void testAmountGreaterThanBalance() throws CardRetained{
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		try{
			atm.performTransaction(CHECKING_ACCT, -1, new Money(110));
		}catch(Cancelled c){
			System.out.println(c.getMessage());
			assertTrue(c.getMessage().equals("Insufficient available balance"));
		}
		
		Withdrawal w2 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w2);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));
		
		try{
			atm.performTransaction(CHECKING_ACCT, -1, new Money(120));
		}catch(Cancelled c){
			assertTrue(c.getMessage().equals("Insufficient available balance"));
		}
		
		Withdrawal w3 = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w3);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Account to withdraw from"));		

		try{
			atm.performTransaction(CHECKING_ACCT, -1, new Money(130));
		}catch(Cancelled c){
			assertTrue(c.getMessage().equals("Insufficient available balance"));
		}		
	}
	
	//@Test
	public void testTransaction() throws CardRetained, Cancelled{
		
		//Withdrawal
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.performTransaction(SAVINGS_ACCT, -1, new Money(10));
		assertTrue(atm.getCustomerConsole().getMessage().equals("Would you like to do another transaction?"));
	}
	
	@Test
	public void testCancellation() throws CardRetained, Cancelled{

		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.cancelTransaction();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Last transaction was cancelled\nWould you like to do another transaction?"));
	}	
}
