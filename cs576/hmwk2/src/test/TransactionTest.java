package test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import atm.ATM;
import atm.ATM.InvalidPIN;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Deposit;
import atm.transaction.Inquiry;
import atm.transaction.Transaction.CardRetained;
import atm.transaction.Transfer;
import atm.transaction.Withdrawal;
import banking.Money;

public class TransactionTest {

	private static ATM atm;

	private final String VALID_CARD = "1";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
		Money m = new Money(12345);
		atm.performStartup(m);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testCancellation() throws CardRetained, Cancelled, InvalidPIN{
		int VALID_PIN = 42;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);
		
		//Withdrawal
		Withdrawal w = new Withdrawal(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(w);
		atm.cancelTransaction();
		assertTrue(atm.getCustomerConsole().getMessage().equals("Last transaction was cancelled\nWould you like to do another transaction?"));
	}
	
	@Test
	public void testInvalidPIN() throws CardRetained, Cancelled, InvalidPIN{
		int INVALID_PIN = 999;
		int CHECKING_ACCT = 0;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(INVALID_PIN);
		
		//Balance Inquiry
		Inquiry i = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(i);
		atm.performTransaction(CHECKING_ACCT, -1, new Money(0));
		System.out.println(atm.getCustomerConsole().getMessage());
		assertTrue(atm.getCustomerConsole().getMessage().equals("Invalid PIN\nWould you like to do another transaction?"));		
	}
}
