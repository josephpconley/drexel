package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import atm.ATM;
import atm.ATM.InvalidPIN;
import atm.physical.CustomerConsole.Cancelled;
import atm.transaction.Inquiry;
import atm.transaction.Transaction.CardRetained;
import banking.Money;

public class SystemStartupTest {

	private static ATM atm;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		atm = new ATM(999, "Drexel University", "First National Bank of Joe C", null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		atm.performShutdown();
		atm = null;
	}

	@Test
	public void testStartup() {
		Money m = new Money(12345);
		atm.performStartup(m);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please insert your card"));
	}
	
	@Test
	public void performLegitimateTransaction() throws CardRetained, Cancelled, InvalidPIN{
		String VALID_CARD = "1";
		int VALID_PIN = 42;
		int VALID_ACCT = 0;
		
		atm.startSession(VALID_CARD);
		assertTrue(atm.getCustomerConsole().getMessage().equals("Please enter your PIN\n" + "Then press ENTER"));
		atm.enterPIN(VALID_PIN);
		
		Inquiry tx = new Inquiry(atm, atm.getCurrentSession(), atm.getCurrentSession().getCard(), atm.getCurrentSession().getPIN());
		atm.startTransaction(tx);
		atm.performTransaction(VALID_ACCT, -1, new Money(0));
		assertNotNull(atm.getReceiptPrinter().printReceipt());
	}

}
