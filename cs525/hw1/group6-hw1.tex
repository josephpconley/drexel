\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{verbatim}
\usepackage{latexsym}
\usepackage{cancel}
\usepackage{wrapfig}
\oddsidemargin 0.15in
\textwidth 6.25in
\topmargin-0.5in
\textheight 9.0in
\headsep 0.3in

\author{Joe Conley, Mike DeLaurentis}
\begin{document}

\title{CS 525 Spring 2010 Homework 1}

\pagestyle{fancy}

\rhead{\footnotesize \parbox{20cm}{Mike DeLaurentis, CS 522 Winter 2010,
Homework 4}}

\maketitle

\section*{1.31}

Assume a language A is regular.  Then there exists a finite automaton
$M = (Q, \Sigma, \delta, q_0, F)$ which recognizes A.  Since M
recognizes A, then for all strings $w \in A$, there exists a sequence
$r_0, r_1, ..., r_n$ in Q such that:
\begin{eqnarray*}
& & r_0 = q_0 \\
& & \delta(r_i, w_i + 1) = r_{i+1}\ \text{for}\ i = 0, ..., n - 1,\
 \text{and} \\
& & r_n \in F
\end{eqnarray*}

Define the reverse of a string $w$ to be $w^R = w_n...w_2w_1$.  Then
$A^R = \{w^R | w \in A\}$.  Define a finite automaton $M' = (Q,
\Sigma, \delta', q_n, q_0)$.  From the definition of $M$, we can
assert that for all strings $w \in A^R$, there exists a sequence
$r_n, r_{n-1}, ..., r_0$ in Q such that:
\begin{eqnarray*}
& & r_n = q_n\\
& & \delta'(r_i, w_i) = r_{i-1},\ \text{for}\ i = 1, ..., n\ \text{and}\\
& & r_0 \in F
\end{eqnarray*}

\section*{1.33}
\newcommand{\column}[2]{\begin{bmatrix} #1 \\ #2 \end{bmatrix}}

We can show that $C$ is regular by describing an NFA that recognizes
it.

Assume we have a string w of length n of the form
$$w = \column{t_n}{b_n}... \column{t_2}{b_2} \column{t_1}{b_1}$$ Let $t$
be the binary number represented by $t_n...t_2t_1$ and $b$ be the
one represented by $b_n...b_2b_1$.  Note that $b = 3t$ is the same as
$b = 2t + t$, and note that $2t$ in binary representation is simply
$t$ with all of its bits shifted left one position.  So in order to
decide an input string $w$, we need to determine if $0t_n...t_2t_1 +
t_n...t_2t_10 = b_n...b_2b_1$. In order to model the verification of
this product in an NFA, we need to keep track of whether we are
carrying a 1 from the last addition operation and whether the previous
$t_i$ character was a 0 or 1.  Call this quantity the residual count,
which can have a value of 0, 1, or 2.

Assume the result claimed in problem 1.31: if $C^R$ is regular then $C$
is regular.  This allows us to process the characters of $w$ in
reverse, which makes the modeling of addition easier.  Define an NFA $M = (\{q_{00}, q_{01}, q_{10}\}, \Sigma_2, \delta,
q_{00}, \{q_{00}\})$, where

\begin{itemize}

\item $q_{00}$ represents a residual count of 0: $t_{i-1}$ was 0 and there
is no 1 to carry.  $q_{01}$ means a residual count of 1: either
$t_{i-1}$ was 1 or we have a 1 to carry.  $q_{10}$ is a residual count
of 2: $t_{i-1}$ was 1 and we have a 1 to carry.

\item $\delta$ is described as
\begin{tabular}{l|llll}
& $\column{0}{0}$ & $\column{0}{1}$ & $\column{1}{0}$ &
$\column{1}{1}$ \\\hline
$q_{00}$ & $\{q_{00}\}$& $\{\}$& $\{\}$ & $\{q_{01}\}$\\
$q_{01}$ & $\{\}$ & $\{q_{00}\}$ & $\{q_{10}\}$ & $\{\}$ \\
$q_{10}$ & $\{q_{01}\}$ & $\{\}$ & $\{\}$ & $\{q_{10}\}$\\
\end{tabular}
\end{itemize}

See Figure-\ref{p133}.  

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{hw1-33.pdf}
\caption{NFA for 1.33.}
\label{p133}
\end{center}
\end{figure}

\section*{1.36}

Let $B_n = \{a^k | k = mn\ \text{for}\ m \in \mathbb{Z}^+\}$.  We will
show via induction that $\forall n \geq 1$, $B_n$ is regular.

\paragraph{Basis} Let $n = 1$.  Then $B_1 = \{a^k | k = m\ \text{for}\
m \in \mathbb{Z}^+\}$.  Figure-\ref{hw1-36} shows a state diagram for
an NFA that recognizes $B_1$.
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{hw1-36.pdf}
\caption{NFA recognizing $B_1$}
\label{hw1-36}
\end{center}
\end{figure}
\paragraph{Induction}
Assume $B_n$ is regular.  $B_{n+1}$ is defined as

\begin{eqnarray*}
  B_{n+1} & = & \{a^k | k = m(n + 1)\ \text{for}\ m \in \mathbb{Z}^+\}\\
             & = & \{a^k | k = mn + m\ \text{for}\ m \in \mathbb{Z}^+\}
\end{eqnarray*}
So all $w \in B_{n+1}$ can be written as $w = \text{{\tt a}}^{mn+m} = \text{{\tt
  a}}^{mn}\text{{\tt a}}^m$, which means we can rewrite $B_{n+1}$ as
$B_{n+1} = \{xy | x \in B\ \text{and}\ y \in B_n\} = BB_n$.  Since the
class of regular languages is closed under concatenation, $B_{n+1}$
must be a regular language as well.

\section*{1.38}

Assume $N = (Q, \Sigma, \delta, q_0, F)$ is an all-NFA recognizing
some regular language A.  We can construct an equivalant DFA $M = (Q',
\Sigma, \delta', q_0', F')$ using a procedure similar to the one
described in theorem 1.39 of the text.

Let

$$E(R) = \{q | q\ \text{can be reached from R by traveling along 0 or more}
\epsilon\ \text{arrows.}\}$$

\begin{enumerate}
  \item $Q' = P(Q)$ (all possible subsets of Q)
  \item $\delta'(R, a) = \{q \in Q | q \in E(\delta(r, a))\ \text{for
      some}\ r \in R\}$
  \item $q_0' = E(\{q_0\})$
  \item $F' = \{R \in Q' | r \in F\ \text{for every}\ r \in R\}$
\end{enumerate}

So a state $R$ is an accept state of $M$ iff all of its sub-states are
accept states of $N$.

\section*{1.40}

Let $M = (Q, \Sigma, \delta, q_0, F)$ be an NFA recognizing a regular
language A.  We need to construct an NFA $M' = (Q', \Sigma, \delta',
q_0', F')$ such that its accept states are accept states in $M$
from which no other accept state is reachable.

\begin{itemize}
  \item $Q' = Q$
  \item $\delta'(q, a) = \delta(q, a)$
  \item $q_0' = q_0$
  \item $F' = \{q \in F | \delta(q, a) = \emptyset\}$
\end{itemize}

Suppose a state $r$ has a path to an accept state $f \in F$.  Then any
string where $r$ is the terminal state would be a proper prefix of a
string that would be accepted by $M$.  So $F'$ is simply all states in
$F$ that do not have paths to other accept states.

\section*{1.42}

Let $M_A = (Q_A, \Sigma, \delta_A, q_{0_A},  F_A)$ be a DFA that
recognizes $A$, and  $M_B = (Q_B, \Sigma, \delta_B, q_{0_B},  F_B)$ be
a DFA that recognizes $B$.  We can construct an NFA $M = (Q, \Sigma,
\delta, q_0, F_A)$ that recognizes the shuffle of $A$ and $B$ as follows:

\begin{enumerate}
  \item $Q = Q_A \times Q_B$
  \item 
    $\delta((r_A, r_B), c) = \{(\delta_A(r_A, c), r_B), (r_A,
    \delta_B(r_B, c))\}$
 \item $q_0 = (q_{0_A}, q_{0_B})$
  \item $F = \{(q_A, q_B) | q_A \in F_A\ \text{and}\ q_B \in F_B\}$
\end{enumerate}

Each state $q \in Q$ is a tuple of a state $q_A \in Q_A$ and $q_B \in
Q_B$.  The result of the transition function $\delta$ is a set
consisting of the state if we interpret the next symbol c to be a
symbol from A, and the state if we consider it to be a symbol from B.
Each time we read a symbol, we may need to transition through
$M_A$ or $M_B$.  So each transition through $M$ nondeterministically
tries both $M_A$ and $M_B$.  At any given time, each ``thread'' of
execution represents a possible path through $M$.  If at the end, we
are left at an accept state for A and
 B, then the entire string
is accepted as a shuffle of A and B.

\section*{1.46}

\subsection*{(a)}

Assume that $A = \{0^n1^m0^n | m,n \geq 0\}$ is a regular language.
Let p be the pumping length of A.  Let s be the string $0^p10^p$.  We
now try to write s in the form $s = xyz$ by the rules of the pumping
lemma:

\begin{itemize}
\item $y$ can't be all 0s as there must be an equal number of 0s on
  the left and right sides of the string.
\item y can't be 00*1* or 1*00* as this would cause 0 and 1 to
  alternate more than 0*1*0*.
\item y can't be all 1s as no matter how many 1s are chosen, we can
  always choose a value of n such that $|xy| > p$.
\end{itemize}
Since s cannot be written in the form $s = xyz$ conforming to the
pumping lemma, A is not a regular language.

\subsection*{(d)}

Assume that $A = \{wtw | w,t \in \{0,1\}^+\}$ is a regular language.
Let p be the pumping length of A.  Let s be the string $0^p1^p0^p$.
We now try to write s in the form $s=xyz$ by the rules of the pumping
lemma:

\begin{itemize}
  \item y can't be all 0s as there must be an equal number of 0s on
    the left and right of the string.
  \item y can't be $0^+1^+$ or $1^+0^+$ as this would cause 0 and 1
    to alternate more than the pattern of this language allows.
  \item y can't be all 1s as x must be at least one 0 and as such
    $|xy| = |01^p| = p + 1 > p$.
  \end{itemize}
  
  Since s cannot be written in the form $s = xyz$ conforming to the
pumping lemma, A is not a regular language.

\section*{1.55}

\subsection*{(e)}

$(${\tt 01}$)^*$.  Not counting $\epsilon$ (which is of length 0 which
is not possible for a pumping length), the minumum length of any
string in this language is 2, which is the minimal pumping length.
We can specify $x = \epsilon, y =
(${\tt 01}$)^*, z = \epsilon$.

\subsection*{(g)}

{\tt 1}$^*${\tt 01}$^*${\tt 01}$^*$.  The smallest string in this
language is {\tt 00} of length 2, but this cannot be the pumping
length since we can't add any more {\tt 0}'s into the string.  So the minimum pumping length
is 3, with $x =\ ${\tt 0}$, y =\ ${\tt 1}$^*, z =\ ${\tt 0}.

\subsection*{(i)}

{\tt 1011}.  Since there is only one string in the language, the
minimal pumping length is $p = 5$.  The string cannot be pumped at
all, so we must choose a value for $p$ which is greater than the
length of the string, so that no strings of length at least $p$ exist
in the language.  The smallest such value for $p$ is 5.

\section*{1.60}

We can construct an NFA $M = (Q, \Sigma, \delta, q_0, F)$ that recognizes
$C_k$ as follows:
\begin{enumerate}
  \item $Q = \{q_1, q_2, ..., q_k, q_{k+1}\}$
  \item $\Sigma = \{${\tt a}, {\tt b}$\}$
  \item $$\delta(q_i, s) = \left\{ 
      \begin{array}{ll}
        \{q_1, q_2\} & i = 1, s = \text{{\tt a}}\\
        \{q_1\} & i = 1, s = \text{{\tt b}}\\
        \{q_{i+1}\} & 2 \leq i \leq k\\
        \emptyset & \text{otherwise}
      \end{array}
    \right .$$
  \item $q_0 = q_1$
  \item $F = \{q_{k+1}\}$
\end{enumerate}

See Figure-\ref{p160} for a state diagram illustrating the construction of $M$.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{hw1-60.pdf}
\caption{NFA for 1.60.}
\label{p160}
\end{center}
\end{figure}

\section*{1.61}

Intuitively, a DFA that recognizes $C_{k-1}$ must record the path 
consisting of the last $k$ symbols read in.  Since there are two 
symbols we need to differentiate, {\tt a} and {\tt b}, there are a 
total of $2^k$ strings of length $k$ consisting of those two symbols. 
If a DFA needs to remember a string of length $k$ out of $2^k$ 
possible strings, it needs $2^k$ states to do so. We can also prove by induction.

\paragraph{Basis}  Show that a DFA that recognizes $C_1$ needs at
least $2^1 = 2$ states.  Let $M = (Q, \Sigma, \delta, q_0, F)$ be a
DFA that recognizes $C_1$: the language consisting of all strings
where the last symbol is {\tt a}.  We clearly need two states: one
where the curent symbol is {\tt a} and one where the current symbol is
{\tt b}.  If we only had one state, we could only either accept or
reject all strings.  Figure-\ref{p161-basis} shows a state diagram for the DFA that
recognizes $C_1$:

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{hw1-61-basis.pdf}
\caption{DFA recognizing $C_1$.}
\label{p161-basis}
\end{center}
\end{figure}

\paragraph{Inductive step} Assume that any DFA recognizing $C_{k-1}$
needs at least $2^{k-1}$ states, and show that a DFA recognizing $C_k$
needs at least $2^k$ states.  Let $M_{k-1}$ be a DFA recognizing
$C_{k-1}$.  In order to recognize $C_{k}$, $M_{k}$ must remember at
any given time the last $k$ symbols read in.  When we increase $k$ by
1, we need to double the number of states from $M_{k-1}$.  We need one
copy for when the last symbol is {\tt a} and one for when it is {\tt
  b}.  So we need $2 \times 2^{k-1} = 2^k$ states.

\end{document}
