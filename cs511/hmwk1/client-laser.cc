#include <stdio.h>
#include <libplayerc++/playerc++.h>

/*
*	client-laser.cc
*
*	@author - Joe Conley
*
*	Used to complete BOTH the maze-ir.cfg and the maze-laser.cfg mazes
*
	While this code works as is, the biggest issue I've found thus far is modifying the speed slightly can cause it to fail.  The handling of making left turns needs to be a bit cleaner for this program to be more robust.
*/

const double SPEED = 1.1;
const double WALL_DIST = 0.5;
const double TURN_DIST = 0.15;
double getChange(double,double,double,double);

int main(int argc, char *argv[])
{
	/* need to do this line in c++ only */
	using namespace PlayerCc;

	printf("Getting robot \n");
	PlayerClient robot("localhost");

	//ORDER IS IMPORTANT HERE!!!!!
	//When reversing the order of these two, I get a segfault error.  I'm new to C++ so any insight would be welcome
	LaserProxy lp(&robot,0);
	Position2dProxy pp(&robot,0);

	//position, turn variables
	double prevX = 99;
	double prevY = 99;
	int prevYaw = 0;		//initially not rotating
	double turnDistance = 0;	//if this exceeds the size of the robot, time to turn left

	int turnRight = 0;		//initially not turning	
	int turnLeft = 0;		//initially not turning	
	int straight = 0;		//initially straight and not trying to make any left turns

	//loop until we get home
	for(;;)
	{
		robot.Read();
		double left = lp[360];
		double right = lp[0];
		double x = pp.GetXPos();
		double y = pp.GetYPos();
		double yaw = round(rtod(pp.GetYaw()));


		//by default, move forward
		double speed = SPEED;
		double yawSpeed = 0;
			

	std::cout << "Left: " << left << " Yaw: " << yaw << " turnLeft: " << turnLeft << " turnRight: " << turnRight << " x: " << x << " y: " << y << std::endl;

	//must be in a turn or moving

	//finished turning left
		if(turnLeft == 1 && abs(abs(yaw) - prevYaw) == 90){
			std::cout << "Finished turning left: " << std::endl;
			turnLeft = 0;
			straight = 1;
			speed = 5;
		}

	//finished turning right
		else if(turnRight == 1 && abs(abs(yaw) - prevYaw) == 90){
			std::cout << "Finished turning right: " << std::endl;
			turnRight = 0;
			straight = 1;
		}

	//turning right, keep doing until we are finished above
		else if(turnRight == 1 && straight == 0){
			speed = 0;
			yawSpeed = -dtor(30);
		}
		
	//turning left, keep doing until we are finished above
		else if(turnLeft == 1 && straight == 0){
			speed = 0;
			yawSpeed = dtor(30);
		}else{		

			//if we're more than WALL_DIST outside, keep the turn distance
			if(left > WALL_DIST && straight == 1 && TURN_DIST > turnDistance){
				turnDistance = turnDistance + getChange(x,y,prevX,prevY);
				std::cout << "x " << x << " turnDistance " << turnDistance << std::endl;
			}

			//we've waited long enough, gotta turn now
			else if(TURN_DIST < turnDistance){
				turnLeft = 1;
				straight = 0;
				prevYaw = abs(yaw);
				std::cout << "Turning left: " << std::endl;		
				turnDistance = 0;
			}

			//just hit a wall, back up (do this last, want to make sure we're not just spinning our wheels)
			else if(prevX == x && prevY == y){
				speed = -2;
				yawSpeed = 0;
				turnRight = 1;
				straight = 0;
				prevYaw = abs(yaw);
				std::cout << "Turning right: " << std::endl;
			}
		}


		if(ceil(x) > 46 && floor(y) < -45){
			std::cout << "Home! " << std::endl;
			return 0;
		}else{
			pp.SetSpeed(speed,yawSpeed);				
		}

		prevX = x;
		prevY = y;
	}

	return 0;
}

//determines distance we've traversed (handles the quirkiness of doubles in C++)
double getChange(double x, double y, double prevX, double prevY){
	double turnDistance = 0.0;
	
	//must have changed in Y direction
	if(x - prevX == 0){
		if(y > prevY){
			turnDistance = turnDistance + (y - prevY);
		}else{
			turnDistance = turnDistance + (prevY - y);
		}
	}else{
		if(x > prevX){
			turnDistance = turnDistance + (x - prevX);
		}else{
			turnDistance = turnDistance + (prevX - x);
		}
	}

	return turnDistance;
}
