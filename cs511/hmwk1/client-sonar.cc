#include <stdio.h>
#include <libplayerc++/playerc++.h>

/*
*	client-sonar.cc
*
*	@author - Joe Conley
*
*	Used to complete the maze-sonar.cfg maze
*
	This program was more robust in handling left turns, and as a result I've had more success in testing with varying speeds.
*/

const double SPEED = 1;

int main(int argc, char *argv[])
{
	/* need to do this line in c++ only */
	using namespace PlayerCc;

	printf("Getting robot \n");
	PlayerClient robot("localhost");

	SonarProxy sp(&robot,0);
	Position2dProxy pp(&robot,0);


	double prevX = 99;
	double prevY = 99;
	int prevYaw = 0;	//initially not rotating
	double turnDistance = 0;	//if this exceeds the size of the robot, time to turn left

	int turnRight = 0;	//initially not turning	
	int turnLeft = 0;	//initially not turning	
	int straight = 0;	//initially straight and not trying to make any left turns


	for(;;)
	{
		robot.Read();
		double x = pp.GetXPos();
		double y = pp.GetYPos();
		double yaw = round(rtod(pp.GetYaw()));

		double speed = SPEED;		//by default, move forward
		double yawSpeed = 0;		//by default, go straight	
		int numLeft = 0;		//counter of left sensors which don't directly see a left wall

		//get front, back, and left sums
		double left = 0.0;		
		double front = 0.0;
		for(int i=0;i<8;i++){
			front = front + sp[i];

			//check left sensors
			if(i < 4){
				left = left + sp[i];
				
				if(sp[i] > 1){
					numLeft++;
				}
			}
		}

		double back = 0.0;
		for(int i=8;i<16;i++){
			back= back + sp[i];

			if(i > 11){
				left = left + sp[i];
				
				//check the next leftmost sensor only
				if(sp[i] > 1 && i == 15){
					numLeft++;
				}
			}
		}


		std::cout << "front: " << front << " back: " << back << " left: " << left << " numLeft: " << numLeft << std::endl;

		//std::cout << "sp: " << sp << std::endl;
	
	//not starting a turn, must be in a turn or just moving
	//finished turning left
		if(turnLeft == 1 && abs(abs(yaw) - prevYaw) == 90){
			std::cout << "Finished turning left: " << std::endl;
			turnLeft = 0;
			straight = 1;
			speed = 5;
		}

	//finished turning right
		else if(turnRight == 1 && abs(abs(yaw) - prevYaw) == 90){
			std::cout << "Finished turning right: " << std::endl;
			turnRight = 0;
			straight = 1;
		}

	//turning right, keep doing until we are finished above
		else if(turnRight == 1 && straight == 0){
			speed = 0;
			yawSpeed = -dtor(30);
		}
		
	//turning left, keep doing until we are finished above
		else if(turnLeft == 1 && straight == 0){
			speed = 0;
			yawSpeed = dtor(30);
		}else{		
			//if the first 5 of our leftmost sonars don't see a wall, start turning left
			 if(numLeft == 5 && straight == 1){
				turnLeft = 1;
				straight = 0;
				prevYaw = abs(yaw);
				std::cout << "Turning left: " << std::endl;		
				turnDistance = 0;
			}

			//just hit a wall, back up (do this last, want to make sure we're not just spinning our wheels)
//			else
			if(front < 3 && front > 0){
				speed = -2;
				yawSpeed = 0;
				turnRight = 1;
				straight = 0;
				prevYaw = abs(yaw);
				std::cout << "Turning right: " << std::endl;
			}
		}
		

		if(ceil(x) > 46 && floor(y) < -45){
			std::cout << "Home! " << std::endl;
			return 0;
		}else{
			pp.SetSpeed(speed,yawSpeed);				
		}		

		prevX = x;
		prevY = y;
	}

	return 0;
}
