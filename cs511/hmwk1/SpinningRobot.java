// Robot Lab Spring 2010
// Robot example:  This robot spins in a circle and print its location and IR
// sensor readings.


import java.io.*;

import javaclient2.*;
import javaclient2.structures.*;
import javaclient2.structures.sonar.*;

public class SpinningRobot
{
	public static Position2DInterface position2d_interface = null;
	public static LaserInterface laser_interface = null;
	
	public static void main(String[] args)
	{
		PlayerClient pc = new PlayerClient("localhost", 6665);
		laser_interface = 
				pc.requestInterfaceLaser(0, PlayerConstants.PLAYER_OPEN_MODE);
		position2d_interface = 
				pc.requestInterfacePosition2D(0, 
						PlayerConstants.PLAYER_OPEN_MODE);
		pc.runThreaded (-1, -1);

		position2d_interface.setSpeed(0.5f,2.0f);
		while(true){

			//print data from the IR sensor
			if(laser_interface.isDataReady()){ //only print when valid
				float[] laser_data = laser_interface.getData().getRanges();
				System.out.println("== IR Sensor ==");
				System.out.println("Left Wall Distance: " + laser_data[360]);
				System.out.println("Right Wall Distance: " + laser_data[0]);

			//print out the robot's location
			System.out.println("X Position: " +
					position2d_interface.getX());
			System.out.println("Y Position: " +
					position2d_interface.getY());

			}

		}

	}
}
