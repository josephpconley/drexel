#include <stdio.h>
#include <libplayerc++/playerc++.h>

int main(int argc, char *argv[])
{
	/* need to do this line in c++ only */
	using namespace PlayerCc;

	printf("Getting robot \n");
	PlayerClient robot("localhost");

	Position2dProxy pp(&robot,0);
	LaserProxy lp(&robot,0);

	//control code
	for(;;)
	{
		robot.Read();

		printf("x = %f ",pp.GetXPos());	
		printf("min = %f",lp.GetMinLeft());
		printf("max = %f \n",lp.GetMinRight());

		
		pp.SetSpeed(.5,dtor(0));
	}

	return 0;
}
