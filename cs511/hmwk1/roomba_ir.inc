define roomba_ir laser
(
  range_min 0.0
  range_max 0.6
  fov 180.0
  samples 361

  color "blue"
  size [ 0.14 0.14 ]	
)

