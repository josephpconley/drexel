import time
import math
from ahoy.agents.predator import PredatorAgent

class PredatorAgentImpl(PredatorAgent) :
    
    '''
    Constructor class
    '''
    def __init__(self, uid) :
        PredatorAgent.__init__(self, uid)
        self.offset = None
        self.camera = {}
        self.timer = time.time()
        self.prey_timer = None
        
        #Scout stuff
        self.leadScout = -1
        self.isAgentAcks = {}
        self.agents = {}
        self.prey = []
        self.prey_heading = None
        
        #Prey
        #how fast to move once within range of prey (slightly faster than prey)
        self.PREY_VEL = 0.5
        self.preyCount = 4
        
        #time limit for killing a prey once spotting one (to avoid errors propagating)
        self.SHOT_CLOCK = 25
    '''
    Main method which is called at startup 
    '''
    def main(self) :

        rotation = math.radians(45)
        pos = self.get_position()
        BORDER = 26
        while self.preyCount > 0:

            #standard movement (TODO: should be smarter) 
            if self.leadScout < 0 or self.offset == None:
                self.set_speed(1, rotation)
                rotation *= -1
                
            elif self.leadScout == self.get_uid() and self.prey.__len__() > 0:
                self.hunt(self.prey,self.prey_heading)

                if self.prey_timer != None and time.time() - self.prey_timer > self.SHOT_CLOCK:
                    print 'CALLING OFF HUNT'
                    self.send_message(['STOP_HUNT'])
                    self.reset()
            
            x, y = self.get_position()
            if math.fabs(x) > BORDER or math.fabs(y) > BORDER:
                p = (0,0)
                self.set_speed(1,self.get_prey_heading(p))
                
            
            #update the lead scout as to my whereabouts if it's been a while
            if self.leadScout != self.get_uid() and self.distance(pos, self.get_position()) > 1:
                self.send_message(['AGENT_POSITION', self.leadScout, self.get_position()])
                pos = self.get_position()

            
        print 'All prey have been captured!'
        self.set_speed(0,0)
    '''
    This method is automatically invoked when a message from another predator is received.
    '''
    def on_message_recv(self, src, contents) :
        #print 'Predator %s is at %s and just heard that Predator %s found something at %s' % (self.get_uid(), self.get_position(), src, contents[1])
        
        #compare this predator's location to the location of a bogey, return Y or N
        if contents[0] == 'IS_AGENT':
            self.leadScout = src

            prey = contents[1]
            isAgent = self.camera_match(self.get_position(), prey)
            self.send_message(['IS_AGENT_ACK',src,isAgent,prey,self.get_position()])
       
        # if this is a response about prey and this predator was the one who asked the question, go ahead
        elif contents[0] == 'IS_AGENT_ACK' and contents[1] == self.get_uid():

            if len(self.agents) < 4:
                self.agents[src] = contents[4]
                
            self.isAgentAcks[src] = contents[2]
            if len(self.isAgentAcks) == 4:
                print 'Predator at %s found %s' % (self.get_position(), self.isAgentAcks)
                if 'Y' in self.isAgentAcks.values():
                    print 'I found a predator'
                    self.send_message(['STOP_HUNT'])
                    self.reset()
                else:
                    print 'I found prey'
                    self.prey = contents[3]
                    
                    #You have one minute to catch prey, otherwise let it go
                    self.prey_timer = time.time()
                    
        elif contents[0] == 'HUNT_PREY':
            prey = contents[1]
            heading = contents[2]
            self.hunt(prey,heading)
        
        elif contents[0] == 'OFFSET' and contents[1] == self.get_uid():
            self.offset = contents[2]
        
        elif contents[0] == 'STOP_HUNT':
            self.reset()
            #self.send_message(['STOP_HUNT_ACK',src,self.get_uid()])
            
        #elif contents[0] == 'STOP_HUNT_ACK' and contents[1] == self.get_uid():
            #del self.isAgentAcks[contents[2]]
            
        elif contents[0] == 'AGENT_POSITION' and contents[1] == self.get_uid():
            self.agents[src] = contents[2]

    '''
    This method is automatically invoked when a prey dies.
    '''
    def on_prey_death(self, pos, uid) :
        x, y = pos
        
        if self.offset != None:
            self.set_speed(2,math.radians(self.offset))
        
        if self.get_uid() == self.leadScout:
            self.set_speed(2,math.radians(180))
            
        self.preyCount -= 1
        self.reset()

        print 'Predator %s heard that prey %s died at %s seconds, preyCount is %s' % (self.get_uid(), uid, time.time() - self.timer, self.preyCount)
        
        if self.preyCount == 0:
            self.set_speed(0,0)
    '''
    Reset this agent to its default state
    '''
    def reset(self):
        self.leadScout = -1
        self.prey = []
        self.isAgentAcks = {}
        self.agents = {}
        self.prey_heading = None        
        self.offset = None
        self.prey_timer = None
    
    '''
    This method is automatically invoked when when the camera sees other predators/prey.
    Each agent keeps a history of what it sees in the camera dictionary.  Only if the reading is different from the previous
    will the agent act (I've noticed that the agent will often get multiple camera hits for one position)
    '''
    def on_camera(self, locations) :
        
        #only run through this when we get an updated camera position
        if len(self.camera) == 0 or self.distance(locations[0], self.camera[0]) > 0:
            self.camera[0] = locations[0]
        
            if self.get_uid() == self.leadScout or self.leadScout == -1:

                print 'Scout %s sees something at %s' % (self.get_uid(), locations[0])  

                #if this potential Scout doesn't have any prey nor any acks. make it a scout
                if self.prey.__len__() == 0 and len(self.isAgentAcks) == 0:
                    print 'Agent %s is nominating itself as leadScout' % self.get_uid()
                    self.leadScout = self.get_uid()
                    self.send_message(['IS_AGENT',locations[0]]);
                    self.offset = 0
                    
                    #wait for acks
                    #time.sleep(2)
                    #self.prey = locations[0]    #if first time scout, try the first one as prey
                elif self.prey.__len__() > 0:
                    x0, y0 = self.prey
                    min = 99
                    heading = None

                    #print 'prey is %s,%s ' % (x0, y0)
                    
                    for loc in locations:
                        dist = self.distance(self.prey, loc)
                        x1, y1 = loc
                        dy = math.fabs(y1 - y0)
                        dx = math.fabs(x1 - x0)
                        
                        ##must be the prey
                        if dist < min and dy < 1 and dx < 1 and self.is_decoy(loc) == False:
                            min = dist
                            self.prey = loc
                            
                            if dy > 0 and dx > 0:
                                heading = self.stdToAhoy(math.atan2((y1-y0), (x1-x0)))
                                
                                #print 'heading %s' % heading
                                #print 'prev heading %s' % self.prey_heading
    
                                #if this is first time getting prey heading, align the agents
                                if self.prey_heading == None and heading != None and len(self.agents) == 4:
                                    #print 'agents %s ' % self.agents
                                    self.assign_agents(self.prey,heading)
                                
                                self.prey_heading = heading
                                self.send_message(['HUNT_PREY',self.prey,self.prey_heading])
                                break
    '''
    This method is called to instruct this agent to hunt the given prey which is at the given heading
    The heading is in AHOY terms.
    '''                      
    def hunt(self,prey,heading):
        
        if (self.offset == None or heading == None) and self.get_uid() == self.leadScout:
            self.set_speed(2,0)
        
        if self.offset != None and heading != None:
            head = self.stdToAhoy(self.stdToAhoy(heading) + math.radians(self.offset))
            pos = self.get_pos_by_angle(prey,head)

            if self.leadScout == self.get_uid():
                pos = prey
            
            distance = self.distance(self.get_position(), pos)
            speed = 2

            if distance < 1.5:
                speed = self.PREY_VEL
            
            #if self.get_uid() != self.leadScout:
                #print 'Agent %s hunting prey at %s' % (self.get_uid(),pos)
            
            self.set_speed(speed,self.get_prey_heading(pos))
    
    '''
    This method determines the heading of the prey in relation to this agent.  
    This steers the agent in the right direction as the prey moves 
    '''    
    def get_prey_heading(self, prey):
        x0, y0 = self.get_position()
        x1, y1 = prey        
        
        dy = y1-y0
        dx = x1-x0
        
        rotation = self.stdToAhoy(math.atan2(dy, dx));
        diff = self.get_rotation() - rotation
        
        if math.fabs(math.degrees(diff)) > 180:
            if diff > 0:
                return math.radians(360) - diff
            else:
                return -(math.radians(360) - diff)
        else:
            return -diff

    '''
    This method is called initially by the Scout agent once prey is confirmed.
    This assigns the other agents to take the West, North, and East positions of the prey.
    The Scout takes the South position and sticks close to the prey
    '''
    def assign_agents(self,prey,heading):
        #WEST, NORTH, and EAST, respectively, in std math degrees
        angles = [90,0,270]
        agents = self.agents
        
        print 'Prey at %s, heading %s' % (prey, heading)        

        for a in angles:
        
            head = self.stdToAhoy(self.stdToAhoy(heading) + math.radians(a))
            pos = self.get_pos_by_angle(prey,head)
            
            print 'Prey at %s degrees is %s' % (math.degrees(head),pos)
            
            min = 999
            minId = -1
            for uid, loc in agents.items():
                dist = self.distance(loc, pos) 
             
                if dist < min:
                    min = dist
                    minId = uid
            
            print 'Send Agent %s an offset of %s' % (minId,a)
            self.send_message(['OFFSET',minId,a])
            del agents[minId]
        
        #handle the remaining agent
        
    '''
    Returns the Cartesian distance between two points
    '''        
    def distance(self, m, n):
        x0, y0 = m
        x1, y1 = n
        return math.sqrt(math.pow(y1 - y0,2) + pow(x1 - x0,2)) 
    
    '''
    For the given position and the potential prey, determine if they are "the same" entity
    Used to compare the position of another agent (i.e. non-Scout) to a potential prey to determine if 
        what the scout is seeing is actually prey or not
    '''    
    def camera_match(self,pos,prey):
        x0, y0 = pos
        x1, y1 = prey
        isAgent = 'N'
        
        #print 'prey %s %s' % (x1,y1)
        #print 'Agent %s %s' % (self.get_uid(), self.get_position())
        
        if (math.fabs(x0 - x1) < 0.5 and math.fabs(y0 - y1) < 0.5):
            print 'I am a predator: %s %s' % (self.get_position(), prey)
            isAgent = 'Y'
            
        return isAgent        
    
    '''
    This method is called by the Scout to double-check that what it sees in the camera 
    is not an agent
    '''    
    def is_decoy(self,prey):
        
        for uid, loc in self.agents.items():
            if self.camera_match(loc,prey) == 'Y':
                return True
        
        return False
    '''
    Takes standard angle measurement and converts into AHOY angle (and vice versa!!!)
    '''
    def stdToAhoy(self,angle):
        rotation = math.radians(90) - angle
        if rotation < 0:
            rotation = math.radians(360) + rotation
        
        return rotation
        
    '''
    Calculates the offsets of the prey to handle West, North, and East (the scout is always South)
    '''        
    def get_pos_by_angle(self,prey,heading):
        stdHeading = self.stdToAhoy(heading)
        BUFF = 2
        
        #NORTH
        x0 = BUFF * math.cos(stdHeading) + prey[0]
        y0 = BUFF * math.sin(stdHeading) + prey[1]
        
        pos = x0, y0        
        return pos
