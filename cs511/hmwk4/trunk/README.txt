Assignment 4 - PREADTORS V. PREY

Joe Conley


OVERVIEW

	There are 5 Predators(Agents) and 4 Prey.
	
	Any agent can find Prey, once one does that agent is the Scout
		
	Scout finds Prey (and does its best to ignore other Predators)

	Scout tells the appropriate agents to attack prey from West, North, and East
		- right now this starts from West, picks closest agent, then continues
		- this could be improved to attain a Nash Equilibrium (best choice for the group and the agents)

	Agents converge and kill prey
	
	They disperse, returning to default movement/config, and Scout resigns
	
	REPEAT until prey are all Dead.
	
BUGS

	Having prey/predator immediately in view at start causes most predators to freeze (false start)
	Handling prey around walls/Scout doing a 180
	Mistaking predators for prey
	Seeing object quickly then losing it causes issues
	
	To lessen the effect of these bugs, I've put in a 25 second shot clock.  
	Once a Scout picks a prey, it has 25 seconds to kill the prey, or otherwise must resign and start over
	
ADVANCED (i.e. wish I had time to do this)

	Better default movement strategy
	Handle multiple locations in camera better 