#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <libplayerc++/playerc++.h>

/*
*	client.cc
*	@author - Joe Conley
*
*	Used to complete the obstacles.cfg
*	Detects obstacles in a2-shapes, constructs an A* path based on those obstacles.
*	The robot then follows this path, looking for any unknown obstacles.
*	As noted in the README, only the first robot works.  Attempts to modify FindBestPath to accomodate for size were unsuccessful.  Furthermore, there is no detection being done by the laser.
*/

using namespace PlayerCc;

class Point{
	public:
		double x, y;
		Point(double x0, double y0)
			:x(x0), y(y0){}
};

class Line{	
	public:
		Point m;
		Point n;
		Line(const Point& x0, const Point& y0)
			:m(x0), n(y0){}
		int intersect(Line z);
};

const double SPEED = 1;
std::vector<Point> FindObstacles(std::vector<Line> lines, std::vector<Point> points);
std::vector<Point> FindBestPath(std::vector<Line> lines, std::vector<Point> points);
void MoveToPoint(double *yaw, double *speed, double *yawSpeed, Point here, std::vector<Point> *goals);
double getHeading(Point a, Point b);
double getAngleDiff(Point a, Point b, Point c);

/*CLRS algorithms to determine intersection*/
double direction(Point i, Point j, Point k);
int intersect(Line a, Line b);

double distance(Point a, Point b);
std::string getPoly(int n);
Line getParallelLine(Line l, double r); 

int main(int argc, char *argv[])
{
	Point start (-14,14);
	Point end (15,-15);

	std::vector<Line> lines;
	std::vector<Point> points;
	std::vector<Point> goals;

	//look at world, find obstacles and plot path
	goals = FindObstacles(lines, points);

	//get rid of start node to save time
	goals.erase(goals.begin());

	printf("Getting robot \n");
	PlayerClient robot(PLAYER_HOSTNAME);

	Position2dProxy pp(&robot,0);
	LaserProxy lp(&robot,0);

	while(true)
	{
		robot.Read();
		double yaw = rtod(pp.GetYaw());

		double speed = SPEED;		//by default, move forward
		double yawSpeed = 0;		//by default, go straight
		Point here(pp.GetXPos()-14,pp.GetYPos()+14);

std::cout << " Yaw: " << getHeading(here,end) << "(" << here.x << "," << here.y << ")" << std::endl;

		MoveToPoint(&yaw, &speed, &yawSpeed, here, &goals);
		pp.SetSpeed(speed, yawSpeed);					
	}

	return 0;
}

//Determine what polynomial is being read into the obstacles.
std::string getPoly(int n){
	if(n < 70){
		return "TRI";
	}else if(n > 70 && n < 100){
		return "RECT";
	}else{
		return "HEX";
	}
}

//Based on where the robot is (here) and its set of goals, advance toward the next goal
void MoveToPoint(double *yaw, double *speed, double *yawSpeed, Point here, std::vector<Point> *goals){
	double heading = getHeading(goals->front(), here);

	if(abs(*yaw - heading) < 4){
		*speed = 1;
		*yawSpeed = 0;		
	}else{
		*speed = 0;
		*yawSpeed = -0.5;
	}

	//check to see if we've arrived
	if(fabs(here.x - goals->front().x) < 0.1 && fabs(here.y - goals->front().y) < 0.1){

		*speed = 0;
		*yawSpeed = 0;
		goals->erase(goals->begin());	

		std::cout << "Next stop " << goals->front().x << " " << goals->front().y << std::endl;
	}
}

//Determine the yaw of one point with respect to the other
double getHeading(Point a, Point b){
	double x = a.x - b.x;
	double y = a.y - b.y;
	double h = sqrt(pow(x,2) + pow(y,2));
	return rtod(asin(y/h));
}

//Used to determine the angle p1p2p3
double getAngleDiff(Point p1, Point p2, Point p3){
	double a = distance(p2,p3);
	double b = distance(p1,p3);
	double c = distance(p1,p2);

	return rtod(acos( ((a*a) + (c*c) - (b*b))/ (2*a*c)  ));
}

//reads in obstacles from a2-shapes
std::vector<Point> FindObstacles(std::vector<Line> lines, std::vector<Point> points){
	using namespace std;

	printf("Finding obstacles \n");
	ifstream obs;
	obs.open("a2-shapes");

	string circ ("circ");
	string poly ("poly");	
	string line;

	double BUFFER = 0.5;

	while(!obs.eof()){
		getline(obs,line);
		string type = line.substr(0,4);
		double pointArray[12];		
		int i = 0;

		if(type.compare(circ) == 0){
			line.erase(0,5);

			size_t found = line.find_first_of("()");
			while(found != string::npos){
				line.erase(found,1);
				found = line.find_first_of("()");
			}

			
			found = line.find_first_of(' ');
			while(found != string::npos){
				pointArray[i] = atof(line.substr(0,found).c_str());
				i++;
				line.erase(0,found+1);	
				found = line.find_first_of(' ');
			}

			double r = atof(line.c_str());
			//cout << "circ " << line << endl;

			//getting center of circle and building a bounding box
			Point p(pointArray[0], pointArray[1]);
			Point a(p.x + r + BUFFER, p.y + r + BUFFER);
			Point b(p.x + r + BUFFER, p.y - r - BUFFER);
			Point c(p.x - r - BUFFER, p.y + r + BUFFER);
			Point d(p.x - r - BUFFER, p.y - r - BUFFER);

			points.push_back(a);
			points.push_back(b);
			points.push_back(c);
			points.push_back(d);

			lines.push_back(Line (a,b));
			lines.push_back(Line (b,c));
			lines.push_back(Line (c,d));
			lines.push_back(Line (d,a));
			lines.push_back(Line (a,c));
			lines.push_back(Line (b,d));

		}
		if(type.compare(poly) == 0){
			string poly = getPoly(line.length());
			line.erase(0,5);

			size_t found = line.find_first_of("()");
			while(found != string::npos){
				line.erase(found,1);
				found = line.find_first_of("()");
			}

			found = line.find_first_of(' ');
			while(found != string::npos){
				pointArray[i] = atof(line.substr(0,found).c_str());
				i++;
				line.erase(0,found+1);			
				found = line.find_first_of(' ');
			}

			pointArray[i] = atof(line.c_str());

			if(poly.compare(string ("TRI")) == 0){
				Point p1 (pointArray[0], pointArray[1] + BUFFER);
				Point p2 (pointArray[2], pointArray[3] - BUFFER);
				Point p3 (pointArray[4] + BUFFER, pointArray[5]);				
				points.push_back(p1);
				points.push_back(p2);			
				points.push_back(p3);						
	
				lines.push_back(Line (p1,p2));
				lines.push_back(Line (p2,p3));
				lines.push_back(Line (p3,p1));
			}

			if(poly.compare(string ("RECT")) == 0){
				Point p1 (pointArray[0] - BUFFER, pointArray[1] + BUFFER);
				Point p2 (pointArray[2] - BUFFER, pointArray[3] - BUFFER);
				Point p3 (pointArray[4] + BUFFER, pointArray[5] - BUFFER);
				Point p4 (pointArray[6] + BUFFER, pointArray[7] + BUFFER);
				points.push_back(p1);
				points.push_back(p2);			
				points.push_back(p3);				
				points.push_back(p4);		

				lines.push_back(Line (p1,p2));
				lines.push_back(Line (p2,p3));
				lines.push_back(Line (p3,p4));
				lines.push_back(Line (p4,p1));
				lines.push_back(Line (p1,p3));
				lines.push_back(Line (p2,p4));
			}

			if(poly.compare(string ("HEX")) == 0){
				Point p1 (pointArray[0] - BUFFER, pointArray[1] + BUFFER);
				Point p2 (pointArray[2] - BUFFER, pointArray[3]);
				Point p3 (pointArray[4] - BUFFER, pointArray[5] - BUFFER);
				Point p4 (pointArray[6] + BUFFER, pointArray[7] - BUFFER);
				Point p5 (pointArray[8] + BUFFER, pointArray[9]);
				Point p6 (pointArray[10] + BUFFER, pointArray[11] + BUFFER);
				points.push_back(p1);
				points.push_back(p2);			
				points.push_back(p3);
				points.push_back(p4);
				points.push_back(p5);
				points.push_back(p6);

				lines.push_back(Line (p1,p2));
				lines.push_back(Line (p2,p3));
				lines.push_back(Line (p3,p4));
				lines.push_back(Line (p4,p5));
				lines.push_back(Line (p5,p6));
				lines.push_back(Line (p6,p1));
				lines.push_back(Line (p3,p6));
				lines.push_back(Line (p1,p4));
			}		
		}	
	}
	obs.close();
	points.push_back(Point (15,-15));

	cout << "All points " << points.size() << endl;

	//we've got all the points, now plan path
	return FindBestPath(lines,points);
}

//Based on the given obstacles, map out a path from start to end
std::vector<Point> FindBestPath(std::vector<Line> lines, std::vector<Point> points){
	std::vector<Point> goals;
	goals.push_back(Point (-14,14));

	Point end (15,-15);	
	double WIDTH = 0.8 / 2;		//was supposed to be used to account for robot's shape, unsuccessful

	std::cout << "Finding best path" << std::endl;

	//Until the last goal is the end point, keep adding points to goals
	while(goals.back().x != 15 && goals.back().y != -15){
		std::cout << "At " << goals.back().x << " " << goals.back().y << std::endl;
		double min = 999;
		int m = -1;
		for(int i=0; i < points.size(); i++){
			double p = distance(goals.back(),points[i]);
			double diff = getAngleDiff(points[i],goals.back(),end);
			double value = p + diff;

			//Line segment for potential path
			int n = 0;
			int l = 0;
			Line x (goals.back(), points[i]);
			while(n == 0 && l < lines.size()){
				n = intersect(x,lines[l]);
				l++;
			}

			//Boundary lines to represent width of robot (unsuccessful)
			/*
			l = 0;
			Line y = getParallelLine(x,WIDTH);
			while(n == 0 && l < lines.size()){
				n = intersect(y,lines[l]);
				l++;
			}

			l = 0;
			Line z = getParallelLine(x,-WIDTH);
			while(n == 0 && l < lines.size()){
				n = intersect(z,lines[l]);
				l++;
			}

	std::cout << "Point " << i << " " << points[i].x << " " << points[i].y << " " << value << " " << n << std::endl;			
			*/
			if(value < min && n == 0){
				min = value;
				m = i;
			}
		}

		//we hit a dead end, remove offender and retry (unsuccessful)
		/*
		if(m == -1){
			std::cout << "Dead End " << std::endl;
			goals.pop_back();
			//sleep(10000);
		}else{
			
		}
		*/		

		std::cout << "Adding Point " << m << std::endl;
			goals.push_back(points[m]);
			points.erase(points.begin() + m);	
	}
	//add end goal
	goals.push_back(end);

	//debugging
	for(int k=1; k< goals.size(); k++){
		double d = distance(goals[k-1],goals[k]); 
		double h = getHeading(goals[k-1],goals[k]);
		std::cout << goals[k].x << " " << goals[k].y << " " << d << " " << h << std::endl;
	}


	return goals;
}

//used to help determine intersection
double direction(Point i, Point j, Point k){
	Point a(k.x - i.x, k.y - i.y);
	Point b(j.x - i.x, j.y - i.y);

	return (a.x * b.y) - (a.y * b.x);
}

//determine if two line segments intersect
int intersect(Line a, Line b){
	double d1 = direction(b.m, b.n, a.m);
	double d2 = direction(b.m, b.n, a.n);
	double d3 = direction(a.m, a.n, b.m);
	double d4 = direction(a.m, a.n, b.n);

	//skipping boundary conditions of ON-SEGMENT for now
	if(((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) && ((d3 > 0 and d4 < 0) || (d3 < 0 && d4 > 0))){
		return 1;
	}else{
		return 0;
	}
}

//return the distance between two points
double distance(Point a, Point b){
	return sqrt(pow(b.y - a.y,2) + pow(b.x - a.x,2));
}

//generate a parallel line translated distance r away
Line getParallelLine(Line l, double r){
	double slope = (l.m.y - l.n.y) / (l.m.x - l.n.x);

	if(slope > 0){
		Point m (l.m.x + r, l.m.y - r);
		Point n (l.n.x + r, l.n.y - r);
		return Line(m,n);
	}else{
		Point m (l.m.x + r, l.m.y + r);
		Point n (l.n.x + r, l.n.y + r);
		return Line(m,n);
	}
}
