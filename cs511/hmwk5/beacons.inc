# Robot Lab
# Spring 2009
# Drexel University
#
# Created by Chris Cannon and Marc Winners

# A beacon which represents food
define foodbeacon model 
(
  fiducial_return 2
  size [ 0.3 0.3 ]
  color "blue"
)

# A beacon which represents home
define homebeacon model 
(
  fiducial_return 1
  size [ 0.3 0.3 ]
  color "orange"
)
