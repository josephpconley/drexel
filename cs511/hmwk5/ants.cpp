/*
 * ants.cpp
 *
 * Joe Conley
 */
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <libplayerc++/playerc++.h>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>
using namespace PlayerCc;
using namespace std;

class Point{
	public:
		double x, y;
		Point(double x0, double y0)
			:x(x0), y(y0){}
};

const double SPEED = 2;

void decay(std::map<std::string,double> *toFood, std::map<std::string,double> *toHome);
double getHeading(Point a, Point b);
void MoveToPoint(double *yaw, double *speed, double *yawSpeed, Point here, Point goal);
double distance(Point a, Point b);
void UpdateMap(std::map<std::string,double> *map, Point p);
void printarray(double x[]);
double halfceil(double n);
double halffloor(double n);
string checkAdjacent(std::map<std::string,double> *map, Point p);
Point idToPoint(string id);

int main()
{
	PlayerClient robot("localhost");

	//pheromone maps
	std::map<std::string,double> toFood;
	std::map<std::string,double> toHome;

	// Number of robots
	const int size = 8;
	double x[size] = {0,0,0,0,0,0,0,0};
	double y[size] = {0,0,0,0,0,0,0,0};
	int hasFood[size] = {-1,-1,-1,-1,-1,-1,-1,-1};

	//proxies
	Position2dProxy *pos[size] = {new Position2dProxy(&robot,0), new Position2dProxy(&robot,1),
	new Position2dProxy(&robot,2), new Position2dProxy(&robot,3), new Position2dProxy(&robot,4),
	new Position2dProxy(&robot,5),new Position2dProxy(&robot,6),new Position2dProxy(&robot,7)};

	SonarProxy *sonar[size] = {new SonarProxy(&robot,0),new SonarProxy(&robot,1),new SonarProxy(&robot,2),
	new SonarProxy(&robot,3),new SonarProxy(&robot,4),new SonarProxy(&robot,5),new SonarProxy(&robot,6),
	new SonarProxy(&robot,7)};

	FiducialProxy *fiducial[size] = {new FiducialProxy(&robot,0),new FiducialProxy(&robot,1),
	new FiducialProxy(&robot,2),new FiducialProxy(&robot,3),new FiducialProxy(&robot,4),
	new FiducialProxy(&robot,5),new FiducialProxy(&robot,6),new FiducialProxy(&robot,7)};

	Point here(0,0);
	Point prev(0,0);
	int counter = 0;

	while(true){
		// Blocks and updates the data (10Hz by default)
		robot.Read();

		for(int i=0; i < size; i++){
			srand(time(NULL));

			//set defaults
			double speed = SPEED;
			double yawSpeed = 0;

			//get some readings, keeping history of poisition as each ant goes forward
			double yaw = rtod(pos[i]->GetYaw());
			here = Point (pos[i]->GetXPos(), pos[i]->GetYPos());
			prev = Point (x[i], y[i]);
			double d = sqrt(pow(here.y - prev.y,2) + pow(here.x - prev.x,2));

			x[i] = here.x;
			y[i] = here.y;

			//randomize motion every so often and if this robot hits something
			if(counter % 10 == 0){
				yawSpeed = dtor(rand() % 360);

				if(counter % 5 == 0){
					yawSpeed *= 1;
				}
			}

			//did this ant hit something or is it stuck?  if so back it up
			if(fabs(sonar[i]->GetScan(3)) < 0.05 || fabs(sonar[i]->GetScan(4)) < 0.05 || d == 0){
				yawSpeed = dtor(rand() % 360);
				if(counter % 2 == 0){
					yawSpeed *= 1;
				}

				speed = -1;
			}


			//follow pheromones if nearby and in correct mode
			string id = checkAdjacent(&toFood, here);
			if(hasFood[i] < 1 && id != "N"){
				MoveToPoint(&yaw, &speed, &yawSpeed, here, idToString(id));
			}else{
				id = checkAdjacent(&toHome, here);
				if(hasFood[i] > 0 && id != "N"){
					MoveToPoint(&yaw, &speed, &yawSpeed, here, idToString());
				}
			}


			cout << "Robot " << i << " @: " << here.x << " " << here.y <<  " speed " << speed << " id " << id << endl;
			pos[i]->SetSpeed(speed,yawSpeed);

			// Check if we're currently detecting home or food
			if(fiducial[i]->GetCount() > 0){
				// If the ID equals 1, you're at home
				// If the ID equals 2, you're at the food
				int fid = fiducial[i]->GetFiducialItem((unsigned)0).id;
				cout << "Robot " << i << " Fiducial ID: " << fid << endl;

				if(fid == 1 && hasFood[i] > -1){
					hasFood[i] = 0;
				}else{
					hasFood[i] = 1;			
				}
			}

			//update pheromone map if we have started food process
			if(hasFood[i] > -1){
				//this point should be normalized, every ant started in a different spot!!!!
				Point p (halfceil(here.x), halfceil(here.y));

				if(hasFood[i] == 0){
					UpdateMap(&toFood, p);
				}else{
					UpdateMap(&toHome, p);
				}
			}


			counter++;
		}

		//reduce the pheremone maps by 0.1
		decay(&toFood, &toHome);
	}
}

//return the distance between two points
//WOULDN'T WORK, WASTED MANY HOURS TRYING TO FIGURE OUT WHY
double distance(Point a, Point b){
	return sqrt(pow(b.y - a.y,2) + pow(b.x - a.x,2));
}

//Determine the yaw of one point with respect to the other
double getHeading(Point a, Point b){
	double x = a.x - b.x;
	double y = a.y - b.y;
	double h = sqrt(pow(x,2) + pow(y,2));
	return rtod(asin(y/h));
}

//Based on where the robot is (here) and its set of goals, advance toward point
//Did not have time to flesh out these details
void MoveToPoint(double *yaw, double *speed, double *yawSpeed, Point here, Point goal){
	/*	
	double heading = getHeading(goal, here);

	if(abs(*yaw - heading) < 4){
		*speed = 1;
		*yawSpeed = 0;		
	}else{
		*speed = 0;
		*yawSpeed = -0.5;
	}

	//check to see if we've arrived
	if(fabs(here.x - goal.x) < 0.1 && fabs(here.y - goal.y) < 0.1){
		*speed = 0;
		*yawSpeed = 0;
	}
	*/
}

//decay function for pheramone maps
void decay(std::map<std::string,double> *toFood, std::map<std::string,double> *toHome){

	std::map<std::string, double>::iterator fit;
	for(fit = toFood->begin(); fit != toFood->end(); fit++){
		if(fit->second > 0){		
			fit->second -= 0.1;			
		}
	}

	std::map<std::string, double>::iterator hit;
	for(hit = toHome->begin(); hit != toHome->end(); hit++){
		if(hit->second > 0){
			hit->second -= 0.1;			
		}
	}
}

//increase the pheromone for this map by 1
void UpdateMap(std::map<std::string,double> *map, Point p){
	stringstream id;
	id << "(" << p.x << "," << p.y << ")";
	map->operator[](id.str()) += 1;
}

//check neighboring cells for pheromones
string checkAdjacent(map<string,double> *map, Point p){
	Point *adj[4] = {new Point(halfceil(p.x), halfceil(p.y)), new Point(halfceil(p.x), halffloor(p.y)),
			 new Point(halffloor(p.x), halffloor(p.y)), new Point(halffloor(p.x), halfceil(p.y))};

	double pher = 0.0;
	string strong = "N";
	for(int i =0; i< 4; i++){
		stringstream id;
		id << "(" << adj[i]->x << "," << adj[i]->y << ")";
		double d = map->operator[](id.str());
		cout << "Entry " << id.str() << " has value of " << d << endl;
		if(d > pher){
			pher = map->operator[](id.str());
			strong = id.str();
		}
	}

	return strong;
}

//Should return a point from a string id
//Did not complete
Point idToPoint(string id){
	return Point(0,0);
}

//works just like ceil, but gets the next highest 0.5 double (used to locate Cells)
double halfceil(double n){
	double m = ceil(n);

	if(m == n){
		return m + 0.5;
	}else if(m - n <= 0.5){
		return m;
	}else{
		return m - 0.5;
	}
}

//works just like floor, but gets the next lowest 0.5 double (used to locate Cells)
double halffloor(double n){
	double m = floor(n);

	if(m == n){
		return m - 0.5;
	}else if(n - m <= 0.5){
		return m;
	}else{
		return m + 0.5;
	}
}

void printarray(double x[]){
	for(int i=0;i<8;i++){
		cout << x[i] << " ";
	}

	cout << endl;
}
