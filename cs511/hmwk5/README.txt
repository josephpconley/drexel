Homework 5 - Stigmergy
Joe Conley

I attempted to follow the model of using a pheromone map similar to the map of Cells I used in Assignment 3.  However, I recently realized that every ant starts from what it thinks is 0,0, so regrettably I have not found a good solution to normalize all of the ants' readings and could not derive a universal map that all ants understand well.  Due to this and other reasons (lack of time being one of them), this assignment does not work as intended.  There was no attempt to provide visualization; however, I believe I've established a good strategy for handling convergence, I just haven't had the time to make necessary adjustments.

FEEDBACK

Player/Stage and AHOY

Both environments were easy to setup. However, I've had little to no experience in either C++ or Python before this course, and as a result I spent the majority of my time trying to navigate the nuances of each langauge.  Another puzzling factor was having robots be oriented differently between the two environments.  I'd prefer the Player/Stage orientation as that makes more sense from a mathematical perspective with regard to measuring angles using the unit circle.  Ultimately, I preferred working in AHOY.  This might be due to the ease of use of Python in contrast to C++, but whatever the reason, I encountered less language-specific headaches using AHOY than Player/Stage.

My biggest disappointment is not being able to use a Java library for the client code.  I believe I would have enjoyed this class more and spent more time on robotics issues instead of langauge issues had I been allowed to use Java.  Despite that setback, however, I did find the course to be very interesting and the use of labs to dig through these concepts was very instructive.  I believe this class even helped me to land a recent job interview as I was able to talk about some of these concepts at length, so thanks!




