# Robot Lab
# Spring 2009
# Drexel University
#
# Created by Chris Cannon and Marc Winners

define roomba_bumper bumper
(
  bcount 2  
  blength 0.33
  bpose[0] [0.12  0.12  45]
  bpose[1] [0.12 -0.12 -45] 
)

define roomba_fiducial fiducialfinder
(
  range_min 0.0
  range_max 1.0
  fov 360.0
)

# The Pioneer2DX sonar array
define p2dx_sonar ranger
(
  scount 16

  # define the pose of each transducer [xpos ypos heading]
  spose[0] [ 0.075 0.130 90 ]
  spose[1] [ 0.115 0.115 50 ]
  spose[2] [ 0.150 0.080 30 ]
  spose[3] [ 0.170 0.025 10 ]
  spose[4] [ 0.170 -0.025 -10 ]
  spose[5] [ 0.150 -0.080 -30 ]
  spose[6] [ 0.115 -0.115 -50 ]
  spose[7] [ 0.075 -0.130 -90 ]
  spose[8] [ -0.155 -0.130 -90 ]
  spose[9] [ -0.195 -0.115 -130 ]
  spose[10] [ -0.230 -0.080 -150 ]
  spose[11] [ -0.250 -0.025 -170 ]
  spose[12] [ -0.250 0.025 170 ]
  spose[13] [ -0.230 0.080 150 ]
  spose[14] [ -0.195 0.115 130 ]
  spose[15] [ -0.155 0.130 90 ]	
		
  # define the field of view of each transducer [range_min range_max view_angle]
  sview [0 5.0 15]

  # define the size of each transducer [xsize ysize] in meters
  ssize [0.01 0.05]
)

define pioneer2dx position
(
  size [0.33 0.33]
  
  gui_nose 1

  # this polygon approximates the circular shape of a Roomba
  polygons 1
  polygon[0].points 16
  polygon[0].point[0] [ 0.225 0.000 ]
  polygon[0].point[1] [ 0.208 0.086 ]
  polygon[0].point[2] [ 0.159 0.159 ]
  polygon[0].point[3] [ 0.086 0.208 ]
  polygon[0].point[4] [ 0.000 0.225 ]
  polygon[0].point[5] [ -0.086 0.208 ]
  polygon[0].point[6] [ -0.159 0.159 ]
  polygon[0].point[7] [ -0.208 0.086 ]
  polygon[0].point[8] [ -0.225 0.000 ]
  polygon[0].point[9] [ -0.208 -0.086 ]
  polygon[0].point[10] [ -0.159 -0.159 ]
  polygon[0].point[11] [ -0.086 -0.208 ]
  polygon[0].point[12] [ -0.000 -0.225 ]
  polygon[0].point[13] [ 0.086 -0.208 ]
  polygon[0].point[14] [ 0.159 -0.159 ]
  polygon[0].point[15] [ 0.208 -0.086 ]

  # this bumper array VERY crudely approximates the Roomba's bumpers
  p2dx_sonar()  
  roomba_bumper()
  roomba_fiducial()
)
