# Robot Lab
# Spring 2009
# Drexel University
#
# Created by Chris Cannon and Marc Winners

define map model
(
  color "black"

  boundary 1

  gui_nose 0
  gui_grid 1
  gui_movemask 0
  gui_outline 0
 
  gripper_return 0
)
