%\documentstyle{article}
\documentclass[10pt,letterpaper]{article}

\oddsidemargin -0.25in
\textwidth 6.5in
\topmargin-1in
\textheight 9.0in
\headsep 0.6in
%\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{clrscode}

\begin{document}
\begin{center} {\Large \sc Computational Geometry (CS623)  \\
Winter 2011-2012\\
Homework I\\}
\end{center}

Joe Conley

\begin{enumerate}
\item[1.] The convex hull of a set $S$ is defined to be the
  intersection of all convex sets that contain $S$. For the convex
  hull of a set of points it was indicated that the convex hull is the
  convex set with smallest perimeter. We want to show that these are
  equivalent definitions.
\begin{enumerate}
\item[a.] (10 Pts.)  Prove that the intersection of two convex sets is
  again convex. This implies that the intersection of a finite family
  of convex sets is convex as well.

\vspace{5 mm}

Let $S$, $T$ be two convex sets.  If
$S \cap T$ has either 0 or 1 points then $S \cap T$ is trivially convex, so
assume $S \cap T$ has at least 2 points.  Let $x,y$ be any two points in $S
\cap T$.  Since $S$ is convex, line $XY \in S$.  Symmetrically, since $T$
is convex, line $XY \in T$.  Therefore,  $XY \in S \cap T$.  Since $x$ and $y$
are arbitrary points in $S \cap T$, then $S \cap T$ is convex.
 
\vspace{5 mm}

\item[b.] (10 Pts.)  Prove that the smallest perimeter polygon $\cal
  P$ containing a set of points $P$ is convex. 
  
\vspace{5 mm}

Let $\cal P$ be the smallest perimeter polygon containing a set of points $P$. 
Let $x, y$ be points in $P$.  Assume for contradiction's sake that $XY \ni
\cal P$.  Then there must be some point $z$ on $XY$ that's outside the perimeter
of $\cal P$.  If we were to take the two points $r, s$ on $XY$ that are
on the boundary of $\cal P$, we would find that the distance between $r$ and $s$
must be less than the distance it took to get from $r$ to $s$ by staying on the
boundary of $\cal P$ due to the Triangle Inequality.  This implies that $\cal P$
is in fact not a smallest perimeter polygon, which is a contradiction.  Thus, $XY \in \cal P$.  
Since $x, y$ are arbitrary points in $\cal P$, $\cal P$ is convex.
 
\vspace{5 mm}
  
\item[c.] (10 Pts.) Prove that any convex set containing the set of
  point $P$ contains smallest perimeter polygon $\cal P$.
  
\vspace{5 mm}
  
Let $C$ be a convex set and $P$ a set of points such that $P \subseteq C$, and
let $\cal P$ be the smallest perimeter polygon containing a set of points $P$.
Assume for contradiction's sake that $C$ does not contain $\cal P$, i.e. $\cal
P$ is not a subset of $C$.  Let $x$ be a point in $\cal P$ such that $x \ni C$. 
Since $P \subseteq C$, $x \ni P$.  It stands to reason that such an $x$ must lie
outside of $C$.  However, this would give $C$ a shorter perimeter around $P$
than $\cal P$, which is a contradiction.  Therefore, $\cal P$ $\subseteq C$.

\vspace{5 mm}

\end{enumerate}

\item[2.] (20 Pts.) Let $E$ be an unsorted set of $n$ segments that
  are the edges of a convex polygon. Describe an $O(n\log n)$
  algorithm that computes from $E$ a list containing all vertices of
  the polygon, sorted in clockwise order. 

\vspace{5 mm}

\begin{codebox}

\Procname{$\proc{Get-Clockwise-Vertices}(V,E)$}
\li $V \gets []$
\li \If $V \neq \emptyset$
\li 	$s \gets V.last$
\li \Else 
\li		$s \gets E.next$
	\End
\li \If $E \neq \emptyset$
\li 	$s \gets E.next$
\li		$t \gets E.next$
\li		\While $t.segments \cap s.segments = \emptyset$
\li		\Do	$t \gets E.next$
		\End
\li		$W \gets \proc{Get-Ordered-Vertices}(s.segments \cup t.segments)$
\li		$u \gets line(W[0], W[1])$
\li		$V.addAll(W)$
\li		$E.remove(u)$
\li 	\Return $\proc{Get-Clockwise-Vertices}(V,E)$
\li \Else
\li 	\Return V
	\End

\end{codebox}

This algorithm starts at the first segment in an unsorted set $E$, building an
ordered set of points $V$.  It scans through $E$ until it finds an adjacent
segment.  Once it does, it determines the proper clockwise order of endpoints
spanning the two segments using $\proc{Get-Ordered-Vertices}$, which checks
possible orderings of endpoints until it finds the ordering which is both
clockwise and ensures that adjacent points actually form a segment in $E$.  This
subprocess takes $O(1)$ as finding out if the points are clockwise is $O(1)$
per the left turn/right turn calculation and the check to verify if the adjacent
points are a segment can be done by using the two segments provided in the
argument.

Once the correct order of points are determined, they are added to the set $V$,
and the clockwise-most segment is used in the next iteration to determine the
next points.  This continues until $E$ is empty.  At worst all possible vertices
will be visited in every iteration, but the list of potential vertices shrinks
logarithmically, so $n$ iterations would require $O(\log n)$ time, making this
algorithm run in $O(n \log n)$.

\vspace{5 mm}

\item[3.] In many situations we need to compute convex hulls of
  objects other than points. 
\begin{enumerate}
\item[a.] (10 Pts.) Let $S$ be a set of $n$ line segments in the
  plane. Prove that the convex hull of $S$ is exactly the same as the
  convex hull of the $2n$ endpoints of the segment. 
  
\vspace{5 mm}
  
Let $E$ be the set of endpoints of the line segments in $S$, and let $CV(S),
CV(E)$ be the convex hulls of $S$ and $E$, respectively.  Let $x \in CV(E)$. 
Since $E \subset S$, it follows that $x$ must be in $CV(S)$ as well, implying
that $CV(E) \subseteq CV(S)$.  Now assume that $x \in CV(S)$.  From the results
above, $CV(S)$ is equivalent to a convex set with smallest perimeter.  However,
since we are dealing with the endpoints of these segments, every endpoints would
have to be either on or inside this perimeter.  Reusing the argument from above
regarding the Triangle Inequality, we could then show that $x \in CV(E)$.  Thus,
$CV(S) \subseteq CV(E)$, implying $CV(S) = CV(E)$.
  
\vspace{5 mm}
  
\item[b.] (10 Pts.) Let $\cal P$ be a non-convex polygon on $n$ vertices. Describe an
  algorithm that computes the convex hull of $\cal P$ in $O(n)$ time.
  
\vspace{5 mm}

Such an algorithm would be similar to Graham's scan.  Start at one point in the
polygon and move clockwise, adding each vertex to a set and calculating turns
when possible.  If at any point a left turn is made, remove the middle point
from the set.  Continue this until all vertices are reached.  The connecting
line segments will form the convex hull.  All vertices are visited once, making
this run in $O(n)$.

\vspace{5 mm}
   
\end{enumerate}

\item[4.] Let $S$ be a set of $n$ (possibly intersecting) {\bf unit} circles
  in the plane. We want to compute the convex hull of $S$.
\begin{enumerate}
\item[a.] (5 Pts.) Show that the boundary of the convex hull of $S$
  consists of straight line segments and pieces of circles in $S$.
  
\vspace{5 mm}
  
Firstly, there would be at least four vertices, representing the leftmost,
rightmost, topmost, and bottommost points in the set.  These points would
represent the boundary points of $CV(S)$ and need to be connected to form the
convex hull.  The boundary then becomes the arc of each circle stretching from the mentioned
vertex until a point occurs where the distance between neighboring circles is
minimized.  At this point, a line segment would form, bridging these circles. 
Continuing in this manner forms the convex hull.  The main goal would be to link
up the four ``extreme'' circles, by using either line segments or the arcs of any intermediary circles as part of the boundary as well.
  
\vspace{5 mm}
  
\item[b.] (5 Pts.) Show that each circle can occur at most once on
  the boundary of the convex hull. 
  
\vspace{5 mm}
	
Assuming that a circle occurs twice or more on the boundary would imply that
there is a gap somewhere on that circle where the circle is not part of the
boundary.  This gap would have to span at least one other circle, but given as
all circles have identical, fixed sizes, this is a contradiction, and as such a
circle can occur at most once on the boundary.

\vspace{5 mm}
  
\item[c.] (5 Pts.)  Let $S'$ be the set of points that are the center
  of circles in $S$. Show that a circle $S$ appears on the boundary of
  convex hull if and only if the center lies on the convex hull of
  $S'$.
  
\vspace{5 mm}
  
Assume that a circle $S$ appears on the boundary of a convex hull.  Then by
definition of boundary it is a local extrema relative to the other circles. 
Since all circles are of a fixed size, this means that the center of the
circle is also a local extrema, this point must be on the convex hull of $S'$. 

Now assume that the center of a unit circle lies on the convex hull of $S'$.  
  
\vspace{5 mm} 

\item[d.] (5 Pts.) Give an $O(n\log n)$  algorithm for computing the
  convex hull of $S$. 
  
\vspace{5 mm}

This can be done very similarly to Graham's scan.  Start with the leftmost
circle.  Find the next circle, and add the arcs/line segment combination
connecting the circles to a set.  Move to the next circle, and perform the same
turn checks as with Graham's scan using the center of each circle as the points
of comparison.  In this manner, the convex hull can be computed, with the same
running time as Graham, $O(n \log n)$.
  
\vspace{5 mm} 

  
\item[e.] (10 Pts.) Give an $O(n\log n)$  algorithm for the case in
  which the circles in $S$ have different radii. 
  
\vspace{5 mm}

Circles with different radii complicates matters from d) as our turn checks
worked based off the assumption of uniform size. Instead of using the center of
each circle as our basis for turns, we now use the point of each circle where
minimum distance between circles is achieved. So when comparing three circles,
we would use the ``exit'' point as the points used in our turn checks.  As these
points can be calculated in $O(1)$, this keeps the $O(n \log n)$ running time
intact.
  
\vspace{5 mm}   
   
\end{enumerate}

\end{enumerate}
\end{document}
