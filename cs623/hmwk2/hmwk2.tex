%\documentstyle{article}
\documentclass[10pt,letterpaper]{article}

\oddsidemargin -0.25in
\textwidth 6.5in
\topmargin-1in
\textheight 9.0in
\headsep 0.6in
%\usepackage{times}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\begin{document}
\begin{center} {\Large \sc Computational Geometry (CS623)  \\
Winter 2011-2012\\
Homework II }
\end{center}

Joe Conley

\begin{enumerate}
\item[1.]  (20 Pts.)  Let $S$ be a set of $n$ disjoint line segments
  whose upper endpoints lie on the line $y=1$ and whoe lower endpoints
  lie on line $y=0$. These segments partition the horizontal strip
  $[-\infty:\infty]\times[0:1]$ into $n+1$ regions. Give an $O(n\log
  n)$ time algorithm to build a binary search tree on the segments in
  $S$ that the region containing a query point can be determined in
  $O(\log n)$ time. Also, describe the query algorithm in detail.
  
\vspace{5 mm}

First note that since these segments are disjoint and the y-coordinates of the
topmost endpoints are all equal to 1 (likewise the bottommost endpoints are
equal to 0), we can compare and sort these segments by using the x-coordinate of
either the topmost or bottommost endpoint.  Let's take the x-coordinate
of the lowermost endpoint (i.e. the endpoint where y = 0) of each segment, and
let's also store the segment at each node.
Iterating over all segments $s \in S$, we take the first segment to be the root of the binary search tree.  We then simply
construct the binary search tree using the x-coordinate of the bottomost point
endpoint as the value of each node, performing the usual insert procedure on a
BST (i.e. for every insert, start at the root node, descend downward left or
right if the node value is less or greater, respectively).  We are iterating
over every segment, so we are performing $n$ insertions.  And, we know that an
insert into a BST is $O(\log n)$ as each insertion will at worst navigate the
height of the tree.  Thus, creating the BST is $O(n \log n)$.

Now, take a point $p \in [-\infty : \infty] \times [0:1]$.  We also declare two
initially-empty variables, $left$ and $right$, which we use as placeholders and
will eventually represent the two segments of the region containing $p$.  We
compare $p$ to the segment $s$ at every node, starting with the root.  By using
the cross product, we can determine if $p$ lies to
the left or right of $s$ by looking at its sign.  This is done by taking
the two endpoints of the segments and crossing it with the point in question (an
operation of $O(1)$ time).
If $p$ lies above/left (cross product is positive) of $s$, then set $right = s$
and traverse down to the left of the BST (if possible).  Likewise, if $p$ lies
to the right of $s$ (cross product is negative), set $left = s$ and traverse
down to the right of the BST (if possible).  Continue in this way until
traversal is no longer possible.  Then $left$ and $right$ represent the bounding
segments of the region in which $p$ lies.  To handle edge cases, if $left$ was
never populated  then the leftmost boundary must be $-\infty$, and likewise if
$right$ was never populated then the rightmost boundary must be $\infty$.  Since
the cross product and variable assignment are $O(1)$, each stop at a node costs
$O(1)$, and at worst we traverse the height of the BST, meaning that locating a point $p$ takes $O(\log n)$.

\vspace{5 mm}

\item[2.] (20 Pts.) Let $S$ be a set of $n$ circles in the
  plane. Describe a plane sweep algorithm to compute all intersection
  points between the circles. (Because we deal with circles, not discs,
  two circles do not intersect if one lies entirely inside the other.)
  Your algorithm should run in time $O((n+k)\log n)$ time, where $k$
  is the number of intersection points. 

\vspace{5 mm}

Assume that every circle $s \in S$ can be described using its center $c$ and its
radius $r$.  We can use the plane sweep algorithm for line segments as a
template for how to calculate all of the intersections of a set of circles.  

First, let's review the events.  The first type of a event is the
circle's ``endpoints'', its leftmost and rightmost points.  If $(x,y)$ is the
center of a circle with radius $r$, these endpoints take the form of $(x-r, y)$
and $(x+r,y)$, respectively.  The next type of events are intersections.  We can
use a simple geometrical formula to determine if two circles intersect.  If the
distance between the two centers is less than the sum of their
radii, then the circles must intersect twice (if the totals are equal, they
intersect once).  This simple check is made in $O(1)$ time, and is verified by
$d(a_0, b_0) \leq a_r + b_r$ for circles $a, b$.  If $d(a_0, b_0) > a_r +
b_r$, then the two circles are disjoint and do not intersect.  Finally, we must
consider the edge case where for two circles $a,b$, $a \subset b$ (if $a=b$, we
only take one unique circle).  Using similar geometric techniques, we can reason
that if the difference between two radii is greater than the distance between
the two centers, then one circle must enclose the other.  Since we can't know
which one encircles which offhand, taking the absolute value of the difference
between radii will suffice, i.e. $d(a_0, b_0) < |a_r - b_r|$.  In such a case,
we would still keep the inner circle available to compare to other circles until
its righmost endpoint comes along.

Having formalized the events and all possible cases, we can proceed with the
plane sweep algorithm similarly as we did with line segments.  
As with the previous algorithm, we will be using the same data structure (binary
balanced tree) to store events.  First, we can
identify all of the endpoint events and insert them into the event queue,
sorting them by x-coordinate (and then y-coordinate if necessary).  Then, while
the queue is not empty, we take an event from the queue and perform the
following actions (similar to line segment sweep):

\begin{enumerate}
  \item[a)] 
If the event is a left endpoint, we insert into the sweep line status based on
y-coordinate, and test for intersections in both directions until it reaches a
circle with which it is disjoint (we can have multiple intersections so we need
to look beyond adjacency).

  \item[b)]
If the event is a right endpoint, we delete from the sweep line status.

  \item[c)]
If the event is an intersection (determined by the $(O(1))$ process described
above), we simply add the calclated intersection points to its own data
structure (any generic list will do).  The calculation of these points is also
$O(1)$ as it requires simple application of triangular properties (triangle
forms between centers and intersection points).
\end{enumerate}

As with the previous algorithm, the data structures dictate the running time. 
There are $2n$ endpoint events, each of which is at most $O(\log n)$ (both
insertion into the tree and checking for intersections is logarithmic as well). 
There are $k$ intersection points at which binary tree deletion occurs in
$O(\log n)$ time.  So as it was with line segments, the running time is
$O(n \log n+k \log n) = O((n+k) \log n)$

\vspace{5 mm}

\item[3.] (20 Pts.)  A {\em rectilinear polygon} is a simple polygon of which all
  edges are horizontal or vertical. Let $\cal P$ be a rectilinear
  polygon with $n$ vertices. Give an example to show $\lceil n/4\rceil$
  cameras are sometimes necessary to guard it. 
  
\vspace{5 mm}

The example in Figure 1 below (next page) shows that subregions of a rectilinear
polygon can be constructed in such a way that one camera would be needed for each subregion. 
In this example, there are 12 edges, and given the lack of visibility afforded
by the minimal slots connecting each subregion, one camera is needed for each of
the 3 subregions.  For this example (crude artwork aside), it's important to
make the ``slots" separating each subregion smaller than the size of a camera to ensure that we
couldn't simply place a camera at the slot itself.  Assuming these
preconditions, since $\lceil 12/4 \rceil = \lceil 3 \rceil = 3$, this example
show that sometimes $\lceil n/4 \rceil$ cameras are necessary.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=60mm,height=60mm]{rectilinear.png}
		\caption{Example of a rectilinear polygon}		
	\end{center} 
\end{figure} 

\vspace{5 mm}  

\item[4.] (20 Pts.) Prove or disprove: the dual graph of the
  triangulation of a monotone polygon is always a chain, that is, any
  node in this graph has degree at most two. 
 
Counterexample:  Per Figure 2 below, a polygon can be drawn in such a way
that a node in the dual graph has degree greater than two.  This star is monotone
with respect to the $x$- and $y$-axis, and yet the two nodes in the center of
the dual graph have degree of 3.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=60mm,height=60mm]{chain.png}
		\caption{Example of a monotone polygon with a non-chain dual graph.}
	\end{center} 
\end{figure}
 
\item[5.] (20 Pts.) Give an efficient algorithm to determine whether a
  polygon $\cal P$ with $n$ vertices is monotone with respect to any
  line, not necessarily horizontal or vertical.  
  Note that the line is \emph{not} given as an input to the algorithm; the only input is
  the polygon.

The approach here is testing $n$ orientations of the polygon for
$y$-monotonicity.  Start by designating one half of the polygonal chain as the
``left'' and its corresponding half as the ``right''.  Start at the topmost edge
and walk along the left chain.  If at some point we move upward, then stop
descending, ``rotate'' the polygon by one vertex, and form new left and right
chains and start again. If for a given orientation, we always move downward or horizontally, then this
orientation must be $y$-monotone, and thus the original orientation must be
monotone in relation to the y-axis having been rotated by a certain degree.

Each walk down the half-chain takes at most $O(n/2)$, and at worst we could have
$n$ orientations to test.  In addition, we must account for the work needed to
switch orientations.  This could be done in a few different ways, so long as the main idea is to be able to
detect reversal of direction with respect to a given point.  Ideally one
would note the location of the topmost node and maintain direction by storing a variable for
overall degree of turns, causing a failure if a turn results in greater than the absolute value of $180^\circ$.  Thus, it should take $O(n^2)$ time to determine if any
monotonicity exists.

\end{enumerate}
\end{document}



