package nachos.network;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import nachos.vm.*;

/**
 * A <tt>VMProcess</tt> that supports networking syscalls.
 */
public class NetProcess extends UserProcess {
    /**
     * Allocate a new process.
     */
    public NetProcess() {
	super();
    }

    private static final int
	syscallConnect = 11,
	syscallAccept = 12;
    
    /**
     * Handle a syscall exception. Called by <tt>handleException()</tt>. The
     * <i>syscall</i> argument identifies which syscall the user executed:
     *
     * <table>
     * <tr><td>syscall#</td><td>syscall prototype</td></tr>
     * <tr><td>11</td><td><tt>int  connect(int host, int port);</tt></td></tr>
     * <tr><td>12</td><td><tt>int  accept(int port);</tt></td></tr>
     * </table>
     * 
     * @param	syscall	the syscall number.
     * @param	a0	the first syscall argument.
     * @param	a1	the second syscall argument.
     * @param	a2	the third syscall argument.
     * @param	a3	the fourth syscall argument.
     * @return	the value to be returned to the user.
     */
    public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
	switch (syscall) {
	case syscallConnect:
		return handleConnect(a0, a1);
	case syscallAccept:
		//Lib.debug('n', "Acceptingconnection on port " + a0);
		return handleAccept(a0);
		
	default:
	    return super.handleSyscall(syscall, a0, a1, a2, a3);
	}
    }
    
    protected int handleConnect(int dstHost, int dstPort){
    	int fileDescriptor = -1;
    	int srcPort = (Integer) ((NetKernel)Kernel.kernel).freePorts.removeFirst();
    	int srcLink = Machine.networkLink().getLinkAddress();
    	Socket newSocket = ((NetKernel)Kernel.kernel).protocol.createSocket(dstHost, dstPort, srcLink, srcPort);
    	    	
    	fileDescriptor = this.getFileIndex();
        if(fileDescriptor != -1) {
        	openFiles[fileDescriptor] = newSocket ;
        	if(!newSocket.open()){
        		fileDescriptor = -1;
        	}
        	
        }
    	return fileDescriptor;
    }
   
    protected int handleAccept(int srcPort){
    	//Lib.debug('n', "In Handle accept on port: " + srcPort);
    	KThread.currentThread().yield();
    	int fileDescriptor = -1;
    	//in handleAccept, the 
    	Socket socket = ((NetKernel)Kernel.kernel).getPending(srcPort);
        if(socket != null)
        	Lib.debug('n', "got client socket: " + socket.srcPort);
    	if(socket != null && socket.acceptRequest()){
    		fileDescriptor = this.getFileIndex();
    		if(fileDescriptor != -1) {
    			openFiles[fileDescriptor] = socket ;
    		}
       }
    	return fileDescriptor;
    }
}
