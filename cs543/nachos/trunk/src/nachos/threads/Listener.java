package nachos.threads;

public class Listener implements Runnable {

	private final Communicator communicator;
	
	public Listener(Communicator comm){
		communicator = comm;
	}
	@Override
	public void run() {
		int word = 0;
		for(int i = 0; i < 10; i++){
			System.out.println("About to listen");
			word = communicator.listen();
			System.out.println("Just said - " + word);
		}

	}

}
