package nachos.threads;

public class Speaker implements Runnable {

	private final Communicator communicator;
	
	public Speaker(Communicator comm){
		communicator = comm;
	}
	@Override
	public void run() {
		for(int i = 0; i < 10; i++){
			System.out.println("About to say - " + i);
			communicator.speak(i);
			System.out.println("Just said - " + i);
		}

	}

}
