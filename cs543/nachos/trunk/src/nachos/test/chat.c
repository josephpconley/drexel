//chat.c
//
//	Team 7 
//
//	Client program designed for N-way communication between clients

#include "syscall.h"
#include "stdio.h"
#include "stdlib.h"

#define MAXBUFFERSIZE 100


 main(int argc, char** argv){
    int len, mysocket;
    int dstHost;// = atoi(argv[1]);
    int dstPort = 15;
    int socket;
    char readBuffer[MAXBUFFERSIZE];
    char msgBuffer[MAXBUFFERSIZE];
    int msgLength;
    int length;
    char buffer[MAXBUFFERSIZE + 1]; // +1 so we can add null terminator
    int chat = 1;
    char ch;
    int char_count;
    int i;
	int bytesRead;
 
    dstHost = atoi(argv[1]);
    printf("Host: %d\n", dstHost);
	
    
    socket = connect(dstHost, dstPort);  	
    printf("Socket: %d\n",socket);
    

    while(chat){
	readline(buffer, MAXBUFFERSIZE);
        if(buffer[0] == '.'){
		chat = false;
	}else{
		write(socket, buffer, 10);
		bytesRead = read(socket, readBuffer, MAXBUFFERSIZE);
		for(i = 0; i<bytesRead; i++){
			printf("%c", readBuffer[i]);
		}
			 
	}
    }
 	
	

/*	while(exit_flag == 0){
		printf("Client >> \n");
		ch = getchar();
		char_count = 0;

		while((ch != '\n') && (char_count < MAXBUFFERSIZE)){
		   buffer[char_count++] = ch;
			ch = getchar();
		}
		buffer[char_count] = 0x00;

		if( buffer[0] == '.' && buffer[1] == 0x00) 
			exit_flag = 1;

		write(socket, buffer, char_count);
	}
*/	

	close(socket);
    return 0;	

}

