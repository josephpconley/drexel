/* sysTest.c
 *	
 *	Testing a handful of syscall functions.
 *
 *	NOTE: There were insufficient physical memory issues when running this program
*/
#include "syscall.h"

int 
main()
{
	char *a = "a";
	char *b = "b";
	char *args[] = {a,b};
  
	int n = exec("echo.coff", 2, args);
	
	int *k;
	int m = join(n,k);
	
	int j;
	for(j = 0;j<5;j++){
		int x = exec("execTest.coff",0,0);
	}
	
	exit(1);
	
	//Shouldn't reach here.
	return 0;
}
