/* execTest.c
 *	
 * Test the exec syscall (without join) with arguments using the echo.c function.
 *
*/
#include "syscall.h"

int 
main()
{
	char *a = "J";
	char *b = "O";
	char *c = "E";
	char *args[] = {a,b,c};
  
	int n = exec("echo.coff", 3, args);
	
	printf("Parent done");
	
	return 0;
}
