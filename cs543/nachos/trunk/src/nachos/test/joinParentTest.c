/* joinParentTest.c
 *	
 *	Testing a few joins with functions
*/
#include "syscall.h"

int 
main()
{
	int n = exec("joinChildTest.coff", 0, 0);
	
	int *k;
	int m = join(n,k);
	
	printf("Done join");
	
	int x = exec("exitTest.coff", 0, 0);
	
	int *j;
	int y = join(x,j);
	
	return 0;
}
