/* joinChildTest.c
*
*   Function used in testing joinParentTest.c
*
 */

#include "syscall.h"

int 
main()
{
	int i;
	for(i = 0; i < 5; i++){
		printf("Child Iteration %d",i);
	}
	
	return 0;
}
