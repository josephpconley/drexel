/* exitTest.c
 *
 * Test which interrupts a for loop using the exit() syscall
 *
 */

#include "syscall.h"

int 
main()
{
	
	int i;
	for(i = 0; i < 10; i++){
		printf("Exit Iteration %d",i);
		
		if(i > 5){
			exit(0);
		}
	}
	
	/*should not get here*/
	return 0;
}
