**********User Commands-*************

<command> [option]

a 	Begin prompting to add a product to the database.
d	Begin prompting to delete a product from the database.
u	Begin prompting to update a current product in the database.
s	Begin prompting to view a product(s). 
	(-all) Print all products.
	(-in_stock) Print all products in stock.
	(-taxable) Print all taxable products.
h	Print this readme file.
q	Quit this program.
	

