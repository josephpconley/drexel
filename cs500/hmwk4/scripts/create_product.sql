CREATE TABLE product
(
	prod_code VARCHAR(8) PRIMARY KEY,
	prod_name VARCHAR(40),
	prod_price NUMERIC(10,2) CHECK (prod_price >= 0),
	prod_is_taxable BOOLEAN,
	prod_in_stock INTEGER CHECK (prod_in_stock >= 0)
)
;