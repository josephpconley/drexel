package hmwk4;

import java.sql.* ;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DAO{
	static String hostname = "momjian.us";
	static String dbname = "grocerystore";
	static String username = "clerk";
	static String password = "leaveat5";

    public static List<Product> select(String where){
    	List<Product> prodList = new ArrayList<Product>();
        try{
            Class.forName( "org.postgresql.Driver" ).newInstance();
            try{
                Connection con = DriverManager.getConnection( "jdbc:postgresql://"+hostname+"/"+
                						dbname,username,password);
                try{
					PreparedStatement query = con.prepareStatement("select * from product " + where);
					ResultSet rs = query.executeQuery();
					while(rs.next()){
						Product prod = new Product();
						prod.setCode(rs.getString(1));
						prod.setName(rs.getString(2));
						prod.setPrice(rs.getDouble(3));
						prod.setTaxable(rs.getBoolean(4));
						prod.setInStock(rs.getInt(5));
						prodList.add(prod);
					}
			
				rs.close();
				query.close();		  
                }catch ( SQLException e ){
                    System.out.println( "JDBC error: " + e );
                }finally{
                    con.close();
                }
            }catch( SQLException e ){
                System.out.println( "could not get JDBC connection: " + e );
            }
        }catch( Exception e ){
            System.out.println( "could not load JDBC driver: " + e );
        }
        
        return prodList;
    }
    
    public static boolean insert(Product prod){
		boolean del = true;
        try{
            Class.forName( "org.postgresql.Driver" ).newInstance();
            try{
                Connection con = DriverManager.getConnection( "jdbc:postgresql://"+hostname+"/"+
						dbname,username,password);
                try{
					PreparedStatement query = con.prepareStatement("insert into product values(?,?,?,?,?)");
					query.setString(1,prod.getCode());
					query.setString(2,prod.getName());
					query.setDouble(3,prod.getPrice());
					query.setBoolean(4,prod.isTaxable());
					query.setInt(5,prod.getInStock());
					query.execute();
					query.close();		  
                }catch ( SQLException e ){
                	System.out.println( "JDBC error: " + e );
                	del = false;
                }finally{
                    con.close();
                }
            }catch( SQLException e ){
                System.out.println( "could not get JDBC connection: " + e );
            }
        }catch( Exception e ){
            System.out.println( "could not load JDBC driver: " + e );
        }
	
	return del;
    }

	public static boolean update(Product prod, String oldCode){
		boolean del = true;
        try{
            Class.forName( "org.postgresql.Driver" ).newInstance();
            try{
                Connection con = DriverManager.getConnection( "jdbc:postgresql://"+hostname+"/"+
						dbname,username,password);
                try{
					PreparedStatement query = con.prepareStatement("update product set prod_code = ?," +
										"prod_name = ?,prod_price = ?,prod_is_taxable = ?," +
										"prod_in_stock = ? where prod_code = ?");
					query.setString(1,prod.getCode());
					query.setString(2,prod.getName());
					query.setDouble(3,prod.getPrice());
					query.setBoolean(4,prod.isTaxable());
					query.setInt(5,prod.getInStock());
					query.setString(6,oldCode);
					query.execute();
					query.close();		  
                }catch ( SQLException e ){
                	del = false;
                    System.out.println( "JDBC error: " + e );
                }finally{
                    con.close();
                }
            }catch( SQLException e ){
                System.out.println( "could not get JDBC connection: " + e );
            }
        }catch( Exception e ){
            System.out.println( "could not load JDBC driver: " + e );
        }
	
	return del;
    }
	
	public static boolean delete(String prodCode){
		boolean del = true;
        try{
            Class.forName( "org.postgresql.Driver" ).newInstance();
            try{
                Connection con = DriverManager.getConnection( "jdbc:postgresql://"+hostname+"/"+
						dbname,username,password);
                try{
					PreparedStatement query = con.prepareStatement("delete from product where prod_code = ?");
					query.setString(1,prodCode);
					query.execute();
					query.close();		  
                }catch ( SQLException e ){
                    System.out.println( "JDBC error: " + e );
                }finally{
                    con.close();
                }
            }catch( SQLException e ){
            	del = false;
                System.out.println( "could not get JDBC connection: " + e );
            }
        }catch( Exception e ){
            System.out.println( "could not load JDBC driver: " + e );
        }
	
	return del;
    }

}