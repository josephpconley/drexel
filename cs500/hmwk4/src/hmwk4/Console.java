package hmwk4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Console{

	static BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
	
    public static void main(String[] args ) throws IOException{    	
    	String input = "";
    	boolean success = false;
    	System.out.println("********Welcome to Mom and Pop's grocery database*****");
    	validateUser();
    	
    	while(input.equals("q")==false){
	    	input = bin.readLine();
	    	StringTokenizer st = new StringTokenizer(input);
	    	String command = st.nextToken();
	    	
	    	if(command.equals("a")){
	    		//Add a product
	    		Product newProd = new Product();
	    		System.out.println("Adding a product...");
	    		System.out.println("Enter a product code: ");
	    			newProd.setCode(bin.readLine());
	    		System.out.println("Enter a product name: ");
	    			newProd.setName(bin.readLine());
	        	System.out.println("Enter a product price: ");
	    			newProd.setPrice(Double.valueOf(bin.readLine()));
	        	System.out.println("Enter if a product is taxable(true or false): ");
	        		boolean isTaxable = false;
	        		if(bin.readLine().equals("true"))
	        			isTaxable = true;
	    			newProd.setTaxable(isTaxable);
	    		System.out.println("Enter the quantity of stock of this product:");
	    			newProd.setInStock(Integer.valueOf(bin.readLine()));
	    			
	    		success = DAO.insert(newProd);
	    		
	    		if(success){
	    			System.out.println("Product "+newProd.getCode()+ " added to the database.");
	    		}else{
	    			System.out.println("Product "+newProd.getCode()+ "failed to add.");
	    		}
	    	}else if(command.equals("u")){
	    	   	//Update a product
	    		System.out.println("Updating a product...");
	    		System.out.println("Enter a product code: ");
	    		String oldCode = bin.readLine();
	    		Product prod = DAO.select("where prod_code = '"+oldCode+"'").get(0);
	    		showProduct(prod);
	    		System.out.println("**********UPDATING PRODUCT**********");
	    		System.out.println("Enter a new product code("+prod.getCode()+"):");
	    			prod.setCode(bin.readLine());
	    		System.out.println("Enter a new product name("+prod.getName()+"):");
	    			prod.setName(bin.readLine());
	    		System.out.println("Enter a new product price("+prod.getPrice()+"):");
	    			prod.setPrice(Double.valueOf(bin.readLine()));
	    		System.out.println("Enter if this product is taxable("+prod.isTaxable()+"):");
	        		boolean isTaxable = false;
	        		if(bin.readLine().equals("true"))
	        			isTaxable = true;
	    			prod.setTaxable(isTaxable);
	    		System.out.println("Enter the new quantity of stock of this product("+
	    													prod.getInStock()+"):");
	    			prod.setInStock(Integer.valueOf(bin.readLine()));
	    		success = DAO.update(prod,oldCode);
	    		if(success){
	    			System.out.println("Product "+prod.getCode()+ " successfully updated.");
	    		}else{
	    			System.out.println("Product "+prod.getCode()+ " failed to update.");
	    		}
	    	}else if(command.equals("d")){
	    		//Delete a product
				System.out.println("Deleting a product...");
				System.out.println("Enter the product code of the product you wish to delete: ");
					Product prod = DAO.select("where prod_code = '"+bin.readLine()+"'").get(0);
					showProduct(prod);
				System.out.println("Are you sure you wish to delete this product(Type y for Yes, n for No)?");
				if(bin.readLine().equals("y")){
					success = DAO.delete(prod.getCode());
		    		if(success){
		    			System.out.println("Product "+prod.getCode()+ " deleted from the database.");
		    		}else{
		    			System.out.println("Product "+prod.getCode()+ "failed to delete.");
		    		}
				}
			}else if(command.equals("s")){
				List<Product> prodList = null;
				if(st.hasMoreTokens()){
					String flag = st.nextToken();
					if(flag.equals("-all")){
						System.out.println("Selecting all products...");
			    		prodList = DAO.select("");	//no where clause
		    		
			    		for(Product prod : prodList){
				    		if(prod.getCode() != null){
				    			showProduct(prod);
				    		}
			    		}
					}else if(flag.equals("-in_stock")){
						System.out.println("Selecting all products in stock...");
			    		prodList = DAO.select("where prod_in_stock > 0");
			    		
			    		for(Product prod : prodList){
				    		if(prod.getCode() != null){
				    			showProduct(prod);
				    		}
			    		}
					}else if(flag.equals("-taxable")){
						
						System.out.println("Selecting all taxable products...");
			    		prodList = DAO.select("where prod_is_taxable = true");
			    		
			    		for(Product prod : prodList){
				    		if(prod.getCode() != null){
				    			showProduct(prod);
				    		}
			    		}
					}else{
						System.out.println("Invalid flag.  Type 'h' to view the list of user commands.");
					}
				}else{
			    	//Select & show a product
		    		System.out.println("Selecting a product...");
		    		System.out.println("Enter a column name: ");
		    			String col = bin.readLine();
		    			//only do conditional if column is not text 
		    			String conditional = "";
		    			String criteria = "";
		    			String where = "";
		    		if(!isText(col)){
		    			System.out.println("Enter a conditional( < , > , <> , = , <= , >= )");
		    			conditional = bin.readLine();
		    			System.out.println("Enter a criteria:");
		    			criteria = bin.readLine();
		    		}else{
		    			col = "UPPER(" + col + ")";
		    			conditional = "LIKE";
		    			System.out.println("Enter a criteria:");
		    			criteria = "UPPER('%" + bin.readLine() + "%')";
		    		}
		    		where = "where "+col+" "+conditional+" "+criteria;
		    		System.out.println(where);
		    		prodList = DAO.select(where);
		    		
		    		for(Product prod : prodList){
			    		if(prod.getCode() != null){
			    			showProduct(prod);
			    		}
		    		}
				}
	    		if(prodList.size() == 0){
	    			System.out.println("No products found!");
	    		}
	    	}else if(command.equals("h")){
	    		showHelp();
	    	}
			else if(command.equals("q")){				
				System.out.println("Goodbye clerk!");
	    	}else{
	    		System.out.println("Invalid command.  Type 'h' to view the list of user commands.");
	    	}
    	}
    }
    
    public static void validateUser() throws IOException{
    	System.out.println("Username: ");
    	String username = bin.readLine();
    	System.out.println("Password: ");
    	String password = bin.readLine();
    	
    	if(!username.equals(DAO.username) || !password.equals(DAO.password)){
    		System.out.println("Username or password incorrect!  Please try again");
    		validateUser();
    	}else{
    		System.out.println("Welcome "+username+", please type in a command, followed by the \"Enter\" key:");
    	}
    }
    
    public static void showProduct(Product prod){
    	System.out.println("***********PRODUCT INFO*************");
    	System.out.println("Code: "+prod.getCode());
    	System.out.println("Name: "+prod.getName());
    	System.out.println("Price: $"+prod.getPrice());
    	System.out.println("Taxable: "+prod.isTaxable());
    	System.out.println("In Stock: "+prod.getInStock());
    	System.out.println("************************************");
    	System.out.println();
    }
    
    public static void showHelp() throws FileNotFoundException{
    	InputStream in = Console.class.getClassLoader()
    						.getResourceAsStream("readme.txt");
    	Scanner s = new Scanner(in);
    	
    	while(s.hasNext()){
    		System.out.println(s.nextLine());
    	}    	
    }
    
    public static boolean isText(String col){
    	boolean isText = false;
    	
    	if(col.equals("prod_code") ||
    			col.equals("prod_name")){
    		isText = true;
    	}    	
    	return isText;
    }
}
