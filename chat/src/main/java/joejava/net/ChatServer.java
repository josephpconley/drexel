package joejava.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ChatServer {
	
	ArrayList<PrintWriter> clients;
	
	public class ClientHandler implements Runnable{
		BufferedReader reader;
		Socket sock;
		
		public ClientHandler(Socket clientSocket){
			try{
				sock = clientSocket;
				InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
				reader = new BufferedReader(isReader);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		public void run(){
			String msg;
			try{
				while((msg = reader.readLine()) != null){
					System.out.println("processing " + msg);
					for(PrintWriter c: clients){
						try{
							c.println(msg);
							c.flush();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws IOException{
		new ChatServer().run();
	}
	
	public void run(){
		clients = new ArrayList<PrintWriter>();
		try {
			ServerSocket serverSocket = new ServerSocket(4444);
			
			while(true){
				Socket clientSocket = serverSocket.accept();
				PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
				clients.add(writer);
				
				Thread t = new Thread(new ClientHandler(clientSocket));
				t.start();
				System.out.println("Got a connection");
			}
		}catch(IOException e) {
			System.out.println("Could not listen on port: 4444.");
		    System.exit(1);
		}
	}
}
