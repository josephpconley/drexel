package joejava.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient {
	Socket chatSocket = null;
	PrintWriter writer = null;
	BufferedReader reader = null;
	
	public static void main(String[] args) throws IOException{
		ChatClient c = new ChatClient();
		c.connect();
	}
	
	public void connect() throws IOException{
		try{
			chatSocket = new Socket("localhost",4444);
			reader = new BufferedReader(new InputStreamReader(chatSocket.getInputStream()));
			writer = new PrintWriter(chatSocket.getOutputStream(),true);
		}catch(UnknownHostException e){
			System.out.println("Unknown host!");
			System.exit(1);
		}catch(IOException e){
			System.out.println("No I/O connection!");
			System.exit(1);
		}
		
		Thread readerThread = new Thread(new IncomingReader());
		readerThread.start();
		
		String msg;
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		
		while(true){
			if((msg = stdIn.readLine()) != null){
				writer.println(msg);
				writer.flush();
			}
		}
		
	}
	
	public class IncomingReader implements Runnable{

		public void run() {
			String msg;
			try{
				while((msg = reader.readLine()) != null){
					System.out.println(msg);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
}
