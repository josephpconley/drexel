package joejava.net;

import java.rmi.Naming;

public class MyRemoteClient {
	public static void main (String[] args){
		new MyRemoteClient().go();
	}
	
	public void go(){
		try{
			//Could use JINI to establish a lookup service to find services by implementation name
			MyRemote service = (MyRemote)Naming.lookup("rmi://192.168.0.180/joe1");
			String s = service.sayHello();
			System.out.println(s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
