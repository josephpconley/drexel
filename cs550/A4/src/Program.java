/* this is the cup file for the mini language
 * at http://www.cs.drexel.edu/~jjohnson/2006-07/winter/cs360/lectures/lec6.html *
 * created by Xu, 2/5/07
 */

import java.util.*;
import java.io.*;

enum Ral {
    LDA ("LDA"),
    LDI ("LDI"),
    STA ("STA"),
    STI ("STI"),
    ADD ("ADD"),
    SUB ("SUB"),
    MUL ("MUL"),
    JMP ("JMP"),
    JMZ ("JMZ"),
    JMN ("JMN"),
    HLT ("HLT");

    Ral(String code)
    {
        myCode = code;
    }

    private String myCode;

    public String toString()
    {
        return myCode;
    }
}

class OpCode{

    private Ral myInstruction;
    private String myAddress;  // Takes in a temp that's t1 or a label - this is converted
                               // to a number later, ie like 22
    private String myLabel;    // This is the opcode label, L1, L2

    // static counters so that we end up with
    // unique temporaries, constants, and labels
    public static int TEMP;
    public static int LABEL;

    OpCode()
    {
        myInstruction = Ral.HLT;
        myAddress = "";
        myLabel = "";
    }

    OpCode(Ral instruction, String addr)
    {
        this(instruction, addr, "");
    }

    OpCode(Ral instruction, String addr, String label)
    {
        myInstruction = instruction;
        myAddress = addr;
        myLabel = label;
    }


    public String getLabel()
    {
        return myLabel;
    }

    public void setLabel(String label)
    {
        myLabel = label;
    }

    public void setAddress(String addr)
    {
        myAddress = addr;
    }

    public String getAddress()
    {
        return myAddress.toString();
    }

    public Ral getInstruction()
    {
        return myInstruction;
    }

    // Used for debugging
    public String toString()
    {
        return (((myLabel != null && !myLabel.isEmpty()) ? myLabel + ":\t" : "\t" )+
                myInstruction + " " + myAddress);
    }

    // Used to actually generate runnable RAL
    public String toRALString()
    {
        return (myInstruction + " " + myAddress);
    }
}

class Expr{
    protected Integer myInteger;

    protected String myAddress;// Takes in a temp that's t1 or a label - this is converted
                           // to a number later, ie like 22

    protected Type myType;

    Type type()
    {
        return myType;
    }

    int intValue()
    {
        return myInteger.intValue();
    }

    String getAddress()
    {
        return myAddress;
    }

    void setAddress(String addr)
    {
        myAddress = addr;
    }



    public Expr(int value, Type type, String addr)
    {
        myInteger = new Integer(value);
        myType = type;
        myAddress = addr;
    }

    public Expr() {
        myInteger = new Integer(-1);
        myType = Type.CONST;
        myAddress = "99";
    }

    public Expr eval(HashMap<String, Expr> hm){
	return this;
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label)
    {
    	return new ArrayList<OpCode>();
    }

    public String toString()
    {
        return (myInteger.toString() + "\t" + myAddress + "\t" +
    		   (myType==Type.CONST ? "C" : myType==Type.VAR ? "V" : "T"));
    }
}


enum Type {
    CONST,
    VAR,
    TEMP;
}

class Ident extends Expr{
    private String name;

    public Ident(String s){
    	super();
	name = s;
    }


    public Expr eval(HashMap<String, Expr> hm){
	//return new Number(new Integer(hm.get(name).toString()));
    	Expr returnValue = null;

        if (name != null && hm.containsKey(name)) {
            returnValue =  hm.get(name);
        }
        return returnValue;
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label)
    {
    	if(name != null && !hm.containsKey(name))
    	{
            hm.put(name, new Expr( 0, Type.VAR,"99"));
    	}
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	codes.add(new OpCode(Ral.LDA,name));

    	String tempAddress = new String((++OpCode.TEMP) + "t");
    	hm.put(tempAddress, new Expr( 0, Type.TEMP,"99"));

    	codes.add(new OpCode(Ral.STA,tempAddress));

    	return codes;
    }
}

class Number extends Expr{
    public Number(int n){
    	super();
	  myInteger = new Integer(n);
    }
    public Number(Integer n){
    	super();
    	myInteger = n;
    }
    public Expr eval(HashMap<String, Expr> hm){
	return this;
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label)
    {
    	String constAddress = new String((myInteger) + "c");
    	hm.put(constAddress, new Expr( myInteger, Type.CONST,"99"));

    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	codes.add(new OpCode(Ral.LDA,constAddress));

    	String tempAddress = new String((++OpCode.TEMP) + "t");
    	hm.put(tempAddress, new Expr( 0, Type.TEMP,"99"));

    	codes.add(new OpCode(Ral.STA,tempAddress));

    	return codes;
    }
}

class Times extends Expr{
    private Expr e1, e2;
    public Times (Expr op1, Expr op2){
    	super();
	e1 = op1;
	e2 = op2;
    }
    public Expr eval(HashMap<String, Expr> hm){
	return new Number(e1.eval(hm).intValue()*e2.eval(hm).intValue());
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label){

    	//Get evaluated expression
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	ArrayList<OpCode> expr1Code = e1.translate(hm,"");

        ArrayList<OpCode> expr2Code = e2.translate(hm,"");
    	codes.addAll(expr2Code);
		codes.addAll(expr1Code);

    	String t1 = new String((OpCode.TEMP-1) + "t");
    	hm.put(t1, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.LDA,  t1));

    	String t2 = new String((OpCode.TEMP) + "t");
    	hm.put(t2, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.MUL,  t2));

    	String t3 = new String((++OpCode.TEMP) + "t");
    	hm.put(t3, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.STA,  t3));

    	return codes;
    }
}

class Plus extends Expr{
    private Expr e1, e2;
    public Plus (Expr op1, Expr op2){
    	super();
	e1 = op1;
	e2 = op2;
    }
    public Expr eval(HashMap<String, Expr> hm){
	return new Number(e1.eval(hm).intValue()+e2.eval(hm).intValue());
    }
    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label){

    	//Get evaluated expression
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	ArrayList<OpCode> expr1Code = e1.translate(hm,"");

        ArrayList<OpCode> expr2Code = e2.translate(hm,"");
    	codes.addAll(expr2Code);
		codes.addAll(expr1Code);

    	String t1 = new String((OpCode.TEMP-1) + "t");
    	hm.put(t1, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.LDA,  t1));

    	String t2 = new String((OpCode.TEMP) + "t");
    	hm.put(t2, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.ADD,  t2));

    	String t3 = new String((++OpCode.TEMP) + "t");
    	hm.put(t3, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.STA,  t3));

    	return codes;
    }
}

class Minus extends Expr{

    private Expr e1, e2;
    public Minus (Expr op1, Expr op2){
    	super();
	e1 = op1;
	e2 = op2;
    }

    public Expr eval(HashMap<String, Expr> hm){
	return new Number(e1.eval(hm).intValue()-e2.eval(hm).intValue());
    }
    public ArrayList<OpCode> translate(HashMap<String, Expr> hm, String label){

    	//Get evaluated expression
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	ArrayList<OpCode> expr1Code = e1.translate(hm,"");

        ArrayList<OpCode> expr2Code = e2.translate(hm,"");
    	codes.addAll(expr2Code);
		codes.addAll(expr1Code);

    	String t1 = new String((OpCode.TEMP-1) + "t");
    	hm.put(t1, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.LDA,  t1));

    	String t2 = new String((OpCode.TEMP) + "t");
    	hm.put(t2, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.SUB,  t2));

    	String t3 = new String((++OpCode.TEMP) + "t");
    	hm.put(t3, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.STA,  t3));

    	return codes;
    }
}

class Statement{

    public Statement(){
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){

	//return new HashMap();
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm)
    {
       return new ArrayList<OpCode>();
    }
}

class AssignStatement extends Statement{
    private String name;
    private Expr expr;

    public AssignStatement(String id, Expr e){
	name = id;
	expr = e;
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){
	/* add name to the list of variable names */
	if (!var.contains(name))
	    var.add(name);
	hm.put(name,expr.eval(hm));
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm)
    {
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();

    	// If we wanted to do some sort of constant folding
    	// I think it would go here
    	// basically do the eval on the expression
    	// (for this it should resolve to an int)

    	// grab the expressions translation
    	ArrayList<OpCode> eval = expr.translate(hm,"");

    	// Add the codes at the front
    	// There shouldn't be anything in codes right now
    	// but if this moves I don't want it to break
    	codes.addAll(0, eval);

    	// Now add my codes
    	// LD t
    	// ST IDENT
    	// Don't advance the TEMP pointer (I'm just getting
    	// the result of the last instruction
    	String tempAddress = new String((OpCode.TEMP) + "t");
    	hm.put(tempAddress, new Expr( 0, Type.TEMP,"99"));
    	hm.put(name, new Expr( 0, Type.VAR,"99"));
    	codes.add(new OpCode(Ral.LDA,  tempAddress));
    	codes.add(new OpCode(Ral.STA, name));

    	return codes;
    }
}

class IfStatement extends Statement{
    private Expr expr;
    private StatementList s1, s2;

    public IfStatement(Expr e, StatementList list1, StatementList list2){
	expr = e;
	s1 = list1;
	s2 = list2;
    }

    public IfStatement(Expr e, StatementList list){
	expr = e;
	s1 = list;
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm){

    	//Get evaluated expression
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	ArrayList<OpCode> exprCode = expr.translate(hm,"");
    	codes.addAll(0, exprCode);

    	String tempAddr = new String((OpCode.TEMP) + "t");
    	hm.put(tempAddr, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.LDA,  tempAddr));

    	//Get statement list one from if clause
    	ArrayList<OpCode> s1Codes = s1.translate(hm);
    	String previousStatementLabel1 = "L"+OpCode.LABEL;
    	
    	
    	String label1 = "L"+(++OpCode.LABEL);
    	System.out.println("IF "+label1);
    	
    	//Assign the statements in else clause to a label
    	codes.add(new OpCode(Ral.JMN,label1));
    	codes.add(new OpCode(Ral.JMZ,label1));

        codes.addAll(s1Codes);

    	ArrayList<OpCode> s2Codes = s2.translate(hm);
    	String label2 = null;
    	
    	// If the label was from an if or a while I don't need
    	// to get another one
    	System.out.println(s2.getStatements());
    	
        if(s2.getStatements().get(s2.getStatements().size()-1) instanceof WhileStatement || s2.getStatements().get(s2.getStatements().size()-1) instanceof IfStatement)
        {
            label2 = "L"+(OpCode.LABEL);
            System.out.println("ELSE1 "+label2);
        }
        else
        {
        	label2 = "L"+(++OpCode.LABEL);
        	System.out.println("ELSE2 "+label2);
        }
    	
    	//Assign the statements in else clause to a label
    	OpCode jumpL2 = new OpCode(Ral.JMP,label2);
    	
    	
    	// Need to grab the label and save it for latter
        if(s1.getStatements().get(s1.getStatements().size()-1) instanceof WhileStatement || s1.getStatements().get(s1.getStatements().size()-1) instanceof IfStatement)
        {
            jumpL2.setLabel(previousStatementLabel1);
        }
    	codes.add(jumpL2);

    	//Give first instruction a label, statement list two
    	s2Codes.get(0).setLabel(label1);
		codes.addAll(s2Codes);

    	return codes;
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){
	if (expr.eval(hm).intValue()>0){
	    s1.eval(hm,var);
	}
	else{
	    s2.eval(hm,var);
	}
    }
}

class WhileStatement extends Statement{
    private Expr expr;
    private StatementList s;

    public WhileStatement(Expr e, StatementList list){
	expr = e;
	s = list;
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){
	while (expr.eval(hm).intValue()>0)
	    s.eval(hm,var);

    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm){
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();

			List<OpCode> stmts = s.translate(hm);
			String previousLabel = "L" + OpCode.LABEL;

			System.out.println("previous label: "+previousLabel);
			
    	codes.addAll(expr.translate(hm, ""));
        String startLabel = "L" + ++OpCode.LABEL;
        if (codes.size() > 0) {
            codes.get(0).setLabel(startLabel);
        }
        
        System.out.println("start label: " + startLabel);
        
        // LD t
        String tempAddr = new String((OpCode.TEMP) + "t");
    	hm.put(tempAddr, new Expr( 0, Type.TEMP,"99"));
    	codes.add(new OpCode(Ral.LDA,  tempAddr));
    	
        String endLabel = "L" + ++OpCode.LABEL;
        
        System.out.println("end label: " + endLabel);
        
        codes.add(new OpCode(Ral.JMN, endLabel));
        codes.add(new OpCode(Ral.JMZ, endLabel));

        codes.addAll(stmts);

		//handle nested while/if statements
		if (s.getStatements().get(s.getStatements().size()-1) instanceof WhileStatement || s.getStatements().get(s.getStatements().size()-1) instanceof IfStatement) {
			codes.add(new OpCode(Ral.JMP, startLabel, previousLabel));
		} else {
			codes.add(new OpCode(Ral.JMP, startLabel));
		}
            
        //L2 label handled in statementList class

    	return codes;
    }
}

class StatementList{

    private LinkedList list;

    public StatementList(Statement st){
	//System.out.println(st);
	list = new LinkedList();
	list.add(st);
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){
        int i;
        int l = list.size();
        Statement s;

        for (i=0;i<l;i++){
            s = (Statement)list.get(i);
            s.eval(hm,var);
        }
    }

    public void insert(Statement s){
	list.add(s);
    }

    public LinkedList getStatements(){
	return list;
    }

    public ArrayList<OpCode> translate(HashMap<String, Expr> hm)
    {
    	ArrayList<OpCode> codes = new ArrayList<OpCode>();
    	int l = list.size();
        Statement s;
        String previousStatementLabel = null;

        for (int i=0;i<l;i++){
            s = (Statement)list.get(i);
            ArrayList<OpCode> statementCode = s.translate(hm);

            //label passing between statements
            if(previousStatementLabel != null)
            {
                statementCode.get(0).setLabel(previousStatementLabel);
                previousStatementLabel = null;
            }

            // Need to grab the label and save it for latter
            if(s instanceof WhileStatement || s instanceof IfStatement)
            {
                previousStatementLabel = "L"+OpCode.LABEL;
            }

            System.out.println("previous statement label: " + previousStatementLabel);
            
            codes.addAll(statementCode);
        }      
        
    	return codes;
    }

}

class Program{
    private StatementList s;

    public Program(StatementList list){
	s = list;
    }

    public void eval(HashMap<String, Expr> hm, LinkedList var){
        s.eval(hm, var);
    }
    
    public void compile(HashMap<String, Expr> hm, LinkedList var){
        ArrayList<OpCode> codes = s.translate(hm);
        String previousStatementLabel = null;
        OpCode halt = new OpCode();
        
        
        
        // Need to grab the label and save it for latter
        // if the last statement was a while or if
        if(s.getStatements().get(s.getStatements().size()-1) instanceof WhileStatement || s.getStatements().get(s.getStatements().size()-1) instanceof IfStatement)
        {
            previousStatementLabel = "L"+OpCode.LABEL;
            halt.setLabel(previousStatementLabel);
        }

        codes.add(halt); // empty constructor = HLT
        
        if ("true".equalsIgnoreCase(System.getProperty("optimize"))) {
            System.out.println("Optimization turned on");
            codes = (ArrayList) optimize(codes);
        }

        link(hm, codes);
    }


    public void dump(HashMap<String, Expr> hm, LinkedList var){
	//System.out.println(hm.values());
	int n = var.size();
	int i;
	String s;
	System.out.println("Dumping out all the variables...");
	System.out.println("Symbol\tValue\tAddr\tType");


	//link(testSymbol(), testInstr());
	Set<String> names = hm.keySet();
	for (String name : names){
	   // s = (String)var.get(i);
	    System.out.println(name + "\t" + hm.get(name).toString());
	}
    }


    //Perform peephole optimization
    public List<OpCode> optimize(List<OpCode> instructions) {
        List<OpCode> optimizedInstructions = new ArrayList<OpCode>();

        if (instructions.size() > 0) {
            OpCode previous = instructions.get(0);
            optimizedInstructions.add(previous);
            OpCode current = null;
            boolean skip = false;
            Map<String, String> labelMap = new HashMap<String, String>();

            for (int i = 1; i < instructions.size(); i++, previous = optimizedInstructions.get(optimizedInstructions.size() - 1)) {
                current = instructions.get(i);
                skip = false;
                if (previous.getInstruction() == Ral.STA) {
                    if (current.getInstruction() == Ral.LDA) {
                        if (previous.getAddress().equalsIgnoreCase(current.getAddress())) {
                            skip = true;
                        }
                    }
                } else if (previous.getInstruction() == Ral.STI) {
                    if (current.getInstruction() == Ral.LDI) {
                        if (previous.getAddress().equalsIgnoreCase(current.getAddress())) {
                            skip = true;
                        }
                    }
                }

                if (skip) {
                    if (current.getLabel() != null && current.getLabel().length() > 0) {
                        //if there is already a label on previous, map the two labels together
                        //to be relabeled later. Otherwise, just move the label up 1 line.
                        if (previous.getLabel() != null && previous.getLabel().length() > 0) {
                            labelMap.put(current.getLabel(), previous.getLabel());
                        } else {
                            previous.setLabel(current.getLabel());
                        }
                    }
                } else {
                    optimizedInstructions.add(current);
                }

            }


            for (String oldLabel : labelMap.keySet()) {
                for (OpCode instruction : optimizedInstructions) {
                    if (oldLabel.equals(instruction.getAddress())) {
                        instruction.setAddress(labelMap.get(oldLabel));
                    }
                }
            }
        }
        return optimizedInstructions;
    }



    // Take in the symbol table and the opcodes
    public void link(HashMap<String, Expr> nameTable, List<OpCode> instructions)
    {
    	Integer address = new Integer(1);
    	Set<String> nameSet = nameTable.keySet();

    	//Find the Constants - and assign addresses
    	// I want them all in the same block
    	for(String name : nameSet)
    	{
            Expr expr = nameTable.get(name);
            if(expr.type() == Type.CONST)
            {
                expr.setAddress((address++).toString());
            }
    	}

    	//Find the variables - and assign addresses
    	for(String name : nameSet)
    	{
            Expr expr = nameTable.get(name);
            if(expr.type() == Type.VAR)
            {
                expr.setAddress((address++).toString());
            }
    	}

    	//Find the variables - and assign addresses
    	for(String name : nameSet)
    	{
            Expr expr = nameTable.get(name);
            if(expr.type() == Type.TEMP)
            {
                expr.setAddress((address++).toString());
            }
    	}

    	//okay all addresses have been assigned
    	// walk through all of the instructions and
    	// replace the addresses
    	// When I get to a label push it into a hashmap for later
    	// don't want to pollute symbol table.
    	HashMap<String, String> labelTable = new HashMap<String, String>();
    	Integer count = new Integer(1);
    	for(OpCode code : instructions)
    	{
            // have a label
            if(!code.getLabel().isEmpty())
            {
            	System.out.println(code.toRALString());
            	
            	//code.setAddress(count.toString());
                labelTable.put(code.getLabel(), count.toString());
            }
            count++;

    	}

    	System.out.println(labelTable);
    	
    	for(OpCode code : instructions)
    	{
            if(code.getInstruction() != Ral.HLT)
            {
                // If it's a jump instruction, I need
                // to get the address from the labelTable
                if(code.getInstruction() == Ral.JMZ ||
                   code.getInstruction() == Ral.JMN ||
                   code.getInstruction() == Ral.JMP )
                {

                    // If it's a jump the address is a label
                    code.setAddress(labelTable.get(code.getAddress()));
                }
                else  // Else the address should be in the symbol table
                {
                    //Retrieve the address
                    Expr expr = nameTable.get(code.getAddress());

                    //Update instruction with new address
                    code.setAddress(expr.getAddress());
                }
            }
    	}


    	StringBuffer memory = new StringBuffer();
    	StringBuffer program = new StringBuffer();

    	for(String name : nameSet)
    	{
            Expr expr = nameTable.get(name);
            memory.append(expr.getAddress() + "\t" + expr.intValue()+ "\n");
    	}

    	for(OpCode code : instructions)
    	{
            program.append(code.toRALString() + "\n");
    	}

    	// Write out the RAL code
    	System.out.println("Creating mem.txt and prog.txt files");
    	File mem = new File("mem.txt");
    	File prog = new File("prog.txt");
    	writeFile(mem, memory);
    	writeFile(prog, program);
    }

    private void writeFile(File aFile, StringBuffer contents)
    {
        try {
            Writer output = new BufferedWriter(new FileWriter(aFile));
            output.write( contents.toString() );
            output.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
