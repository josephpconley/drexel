import java.util.*;
/**
 * Test class that exercises RAL compilier functionality outside of the Program.java
 */
public class RalTest {


	public RalTest() {
	}

	private void test1() {
	    Program testProg = new Program(null);

	    testProg.link(testSymbol(), testInstr());
	}

	private void test2() {
	    Program testProg = new Program(null);

		List<OpCode> initialList = testInstrForTest2();
		List<OpCode> optimizedList = testProg.optimize(initialList);

	    System.out.println("Original number of instructions: " + initialList.size());
	    System.out.println("Optimized number of instructions: " + optimizedList.size());

	    for (OpCode o : optimizedList) {
			System.out.println(o);
		}
	}

    public ArrayList<OpCode> testInstrForTest2()
    {
	    ArrayList<OpCode> temp = new ArrayList<OpCode>();
		temp.add(new OpCode(Ral.LDA, "ZERO"));
		temp.add(new OpCode(Ral.STA, "T1"));
		temp.add(new OpCode(Ral.LDA, "FIVE"));
		temp.add(new OpCode(Ral.STA, "T2"));
		temp.add(new OpCode(Ral.LDA, "T1"));
		temp.add(new OpCode(Ral.SUB, "T2"));
		temp.add(new OpCode(Ral.STA, "T3"));
		temp.add(new OpCode(Ral.LDA, "T3"));
		temp.add(new OpCode(Ral.STA, "n"));
		temp.add(new OpCode(Ral.LDA, "n"));
		temp.add(new OpCode(Ral.STA, "T4"));
		temp.add(new OpCode(Ral.LDA, "T4"));
		temp.add(new OpCode(Ral.JMN, "L1"));
		temp.add(new OpCode(Ral.JMZ, "L1"));
		temp.add(new OpCode(Ral.LDA, "n"));
		temp.add(new OpCode(Ral.STA, "T5"));
		temp.add(new OpCode(Ral.LDA, "T5"));
		temp.add(new OpCode(Ral.STA, "i"));
		temp.add(new OpCode(Ral.JMP, "L2"));
		temp.add(new OpCode(Ral.LDA, "ZERO", "L1"));
		temp.add(new OpCode(Ral.STA, "T6"));
		temp.add(new OpCode(Ral.LDA, "n"));
		temp.add(new OpCode(Ral.STA, "T7"));
		temp.add(new OpCode(Ral.LDA, "T6"));
		temp.add(new OpCode(Ral.SUB, "T7"));
		temp.add(new OpCode(Ral.STA, "T8"));
		temp.add(new OpCode(Ral.LDA, "T8"));
		temp.add(new OpCode(Ral.STA, "i"));
		temp.add(new OpCode(Ral.LDA, "ONE", "L2"));
		temp.add(new OpCode(Ral.STA, "T9"));
		temp.add(new OpCode(Ral.LDA, "T9"));
		temp.add(new OpCode(Ral.STA, "fact"));
		temp.add(new OpCode(Ral.LDA, "i", "L3"));
		temp.add(new OpCode(Ral.STA, "T10"));
		temp.add(new OpCode(Ral.JMN, "L4"));
		temp.add(new OpCode(Ral.JMZ, "L4"));
		temp.add(new OpCode(Ral.LDA, "fact"));
		temp.add(new OpCode(Ral.STA, "T11"));
		temp.add(new OpCode(Ral.LDA, "i"));
		temp.add(new OpCode(Ral.STA, "T12"));
		temp.add(new OpCode(Ral.LDA, "T11"));
		temp.add(new OpCode(Ral.MUL, "T12"));
		temp.add(new OpCode(Ral.STA, "T13"));
		temp.add(new OpCode(Ral.LDA, "T13"));
		temp.add(new OpCode(Ral.STA, "fact"));
		temp.add(new OpCode(Ral.LDA, "i"));
		temp.add(new OpCode(Ral.STA, "T14"));
		temp.add(new OpCode(Ral.JMZ, "LGone"));
		temp.add(new OpCode(Ral.LDA, "ONE"));
		temp.add(new OpCode(Ral.STA, "T15"));
		temp.add(new OpCode(Ral.LDA, "T14"));
		temp.add(new OpCode(Ral.SUB, "T15"));
		temp.add(new OpCode(Ral.STA, "T16", "Moved"));
		temp.add(new OpCode(Ral.LDA, "T16", "LGone"));
		temp.add(new OpCode(Ral.STA, "i"));
		temp.add(new OpCode(Ral.JMP, "L3"));
		temp.add(new OpCode(Ral.HLT, "", "L4"));
	    return temp;
    }

    public HashMap<String, Expr> testSymbol()
    {
    	HashMap<String, Expr> temp = new HashMap<String, Expr>();
    	temp.put("ZERO", new Expr( 0, Type.CONST,"99"));
    	temp.put("FIVE", new Expr( 5, Type.CONST,"99"));
    	temp.put("n",  new Expr( 0, Type.VAR,"99"));
    	temp.put("T1", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T2", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T3", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T4", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T5", new Expr( 0, Type.TEMP,"99"));
    	temp.put("i",  new Expr( 0, Type.VAR,"99"));
    	temp.put("T6", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T7", new Expr( 0, Type.TEMP,"99"));
    	temp.put("T8", new Expr( 0, Type.TEMP,"99"));
    	temp.put("ONE", new Expr( 1, Type.CONST,"99"));
    	temp.put("T9", new Expr( 0, Type.TEMP,"99"));
    	temp.put("fact", new Expr( 0, Type.VAR,"99"));
    	temp.put("T10", new Expr( 0, Type.TEMP,"99"));
		temp.put("T11", new Expr( 0, Type.TEMP,"99"));
		temp.put("T12", new Expr( 0, Type.TEMP,"99"));
		temp.put("T13", new Expr( 0, Type.TEMP,"99"));
		temp.put("T14", new Expr( 0, Type.TEMP,"99"));
		temp.put("T15", new Expr( 0, Type.TEMP,"99"));
		temp.put("T16", new Expr( 0, Type.TEMP,"99"));

		return temp;
    }

    public ArrayList<OpCode> testInstr()
    {
	    ArrayList<OpCode> temp = new ArrayList<OpCode>();

    	temp.add(new OpCode(Ral.LDA, "ZERO"));
    	temp.add(new OpCode(Ral.STA, "T1"));
        temp.add(new OpCode(Ral.LDA, "FIVE"));
        temp.add(new OpCode(Ral.STA, "T2"));
        temp.add(new OpCode(Ral.LDA, "T1"));
        temp.add(new OpCode(Ral.SUB, "T2"));
        temp.add(new OpCode(Ral.STA, "T3"));
        temp.add(new OpCode(Ral.LDA, "T3"));
	    temp.add(new OpCode(Ral.STA, "n"));
	    temp.add(new OpCode(Ral.LDA, "n"));
	    temp.add(new OpCode(Ral.STA, "T4"));
	    temp.add(new OpCode(Ral.LDA, "T4"));
	    temp.add(new OpCode(Ral.JMN, "L1"));
	    temp.add(new OpCode(Ral.JMZ, "L1"));
	    temp.add(new OpCode(Ral.LDA, "n"));
	    temp.add(new OpCode(Ral.STA, "T5"));
	    temp.add(new OpCode(Ral.LDA, "T5"));
	    temp.add(new OpCode(Ral.STA, "i"));
	    temp.add(new OpCode(Ral.JMP, "L2"));
	    temp.add(new OpCode(Ral.LDA, "ZERO", "L1"));
	    temp.add(new OpCode(Ral.STA, "T6"));
	    temp.add(new OpCode(Ral.LDA, "n"));
	    temp.add(new OpCode(Ral.STA, "T7"));
	    temp.add(new OpCode(Ral.LDA, "T6"));
	    temp.add(new OpCode(Ral.SUB, "T7"));
	    temp.add(new OpCode(Ral.STA, "T8"));
	    temp.add(new OpCode(Ral.LDA, "T8"));
	    temp.add(new OpCode(Ral.STA, "i"));
	    temp.add(new OpCode(Ral.LDA, "ONE", "L2"));
	    temp.add(new OpCode(Ral.STA, "T9"));
	    temp.add(new OpCode(Ral.LDA, "T9"));
	    temp.add(new OpCode(Ral.STA, "fact"));
	    temp.add(new OpCode(Ral.LDA, "i", "L3"));
	    temp.add(new OpCode(Ral.STA, "T10"));
	    temp.add(new OpCode(Ral.JMN, "L4"));
	    temp.add(new OpCode(Ral.JMZ, "L4"));
	    temp.add(new OpCode(Ral.LDA, "fact"));
	    temp.add(new OpCode(Ral.STA, "T11"));
	    temp.add(new OpCode(Ral.LDA, "i"));
	    temp.add(new OpCode(Ral.STA, "T12"));
	    temp.add(new OpCode(Ral.LDA, "T11"));
	    temp.add(new OpCode(Ral.MUL, "T12"));
	    temp.add(new OpCode(Ral.STA, "T13"));
	    temp.add(new OpCode(Ral.LDA, "T13"));
	    temp.add(new OpCode(Ral.STA, "fact"));
	    temp.add(new OpCode(Ral.LDA, "i"));
	    temp.add(new OpCode(Ral.STA, "T14"));
	    temp.add(new OpCode(Ral.LDA, "ONE"));
	    temp.add(new OpCode(Ral.STA, "T15"));
	    temp.add(new OpCode(Ral.LDA, "T14"));
	    temp.add(new OpCode(Ral.SUB, "T15"));
	    temp.add(new OpCode(Ral.STA, "T16"));
	    temp.add(new OpCode(Ral.LDA, "T16"));
	    temp.add(new OpCode(Ral.STA, "i"));
	    temp.add(new OpCode(Ral.JMP, "L3"));
	    temp.add(new OpCode(Ral.HLT, "", "L4"));
	    return temp;
    }

    public void execute() {
		test1();
		test2();
	}

	public static void main(String[] args) throws Exception {
		RalTest test = new RalTest();
		test.execute();
	}

}