a := [1,2,3,4,5];
b := a || [6,7,8,9,0];
c := 15 + 2;
d := car(b);
e := cdr(a);

listpA := listp(a);
listpB := listp(b);
listpC := listp(c);
listpD := listp(d);
listpE := listp(e)