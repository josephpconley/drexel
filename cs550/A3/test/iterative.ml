define getListSize 
proc (L)
    x:=0;
	repeat 
		c := cdr(L);
		L := c;
		x := x + 1;
		y := nullp(L);
		z := 1 - (y * 1)
	until z;
	return x
end;

alist := [1,2,[3,4,5]];
ANSWER := getListSize(alist)


