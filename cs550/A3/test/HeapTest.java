import java.util.ArrayList;
import java.util.*;

/**
 * Test class that exercises Heap functionality outside of the Program.java
 */
public class HeapTest {
	private Heap heap;
	private HashMap<String, Element> nameTable;

	public HeapTest() {
		heap = null;
	}

	/**
	 * Heap Size: 2
	 * Add a 1 element list [1]
	 * Add the 1 element list pointer to the nametable
	 * so it's in scope. Try adding another list to the
	 * heap, it should not be able to be added because
	 * the heap is full of in scope lists.
	 */
	private void test1() {
		nameTable = new HashMap<String, Element>();
		heap = new Heap(2);
		List returnList = new List();
	  	List list1 = new List();
		heap.malloc(new Number(1), list1, nameTable);
		boolean success = heap.malloc(list1, returnList, nameTable);

		if (success) {
			System.out.println("Successfully added [1] to the heap.");
			System.out.println("returnList.getAddress()==" + returnList.getAddress());
			nameTable.put("list1", returnList);
		} else {
			System.out.println("Failed adding [1] to the heap.");
		}

		heap.dump();

		System.out.println("Attempting to allocate a new list onto the heap. It should fail.");

		Element number2 = new Number(2);
		ArrayList aList2 = new ArrayList<Element>();
		aList2.add(number2);
		Element list2 = new List(aList2);
		List returnList2 = new List();
		success = heap.malloc(list2, returnList2, nameTable);

		if (success) {
			System.out.println("Should not get here, heap should be full");
		} else {
			System.out.println("Heap was full of in-scope lists, did not add 2nd list");
		}
		heap.dump();
	}

	/**
	 * Heap size: 2
	 * Add list1 [1] to the heap, do not add to name table
	 * simulating it is out of scope.
	 * Add list2 [2,3] to the heap. It should have garbage
	 * collected list1 and successfully added the elements
	 * of list 2.
	 */
	private void test2() {
		heap = new Heap(2);
		nameTable = new HashMap<String, Element>();
		ArrayList l1 = new ArrayList<Element>();
		List list1 = new List(l1);
		l1.add(new Number(1));
		ArrayList l2 = new ArrayList<Element>();
		l2.add(new Number(2));
		l2.add(new Number(3));
		List list2 = new List(l2);

		System.out.println("Allocating list1 [1] to heap, however list1 doesn't exist in the name table");
		List returnList1 = new List();
		List returnList2 = new List();
		heap.malloc(new Number(1), list1, nameTable);

		nameTable.put("list-2", returnList2);
		heap.malloc(list2.eval().get(1), returnList2, nameTable);
		heap.malloc(list2.eval().get(0), returnList2, nameTable);
		System.out.println("Should have garbage colected list1 and only store list2 [2,3]");

		heap.dump();
	}

	/**
	 * Heap size: 5
	 * Add list1 as [1,[2]] in order to show a 2
	 * list pointers in the heap representing a list
	 * that contains another list
	 */
	private void test3(){
		heap = new Heap(5);
		nameTable = new HashMap<String, Element>();
		ArrayList l1 = new ArrayList<Element>();
		l1.add(new Number(2));
		List list1 = new List(l1);
		ArrayList l2 = new ArrayList<Element>();
		l2.add(new Number(1));
		List list2 = new List(l2);
		List returnList = new List();
		List returnList1 = new List();
		System.out.println("List added is [1, [2]]. Should show a list pointer in the heap.");
		heap.malloc(list2.eval().get(0), returnList, nameTable);
		heap.malloc(list1.eval().get(0), returnList1, nameTable);
		heap.malloc(returnList1, returnList, nameTable);
		heap.dump();
	}

	/**
	 * Heap size: 5
	 * Add list1 [1,2,3]
	 * Set list1 in scope
	 * Try adding list2 [4,5,6]
	 * should fail, because there is nothing
	 * to be GCed. However, whatever the last 2 things
	 * that were allocated will be on the heap
	 * waiting to be garbage collected the next
	 * time space is needed to be allocated.
	 */
	private void test4() {
		heap = new Heap(5);
		nameTable = new HashMap<String, Element>();
		System.out.println("\n\nHeap is size 5");
		System.out.println("Adding list-1 as [1,2,3]" );
		List returnList = new List();
		List list1 = new List();
		heap.malloc(new Number(3), list1, nameTable);
		heap.malloc(new Number(2), list1, nameTable);
		heap.malloc(new Number(1), list1, nameTable);
		heap.malloc(list1, returnList, nameTable);
		heap.dump();
		nameTable.put("list-1", returnList);
		System.out.println("list-1 is added to the nameTable, so it's in scope");
		List list2 = new List();
		List returnList2 = new List();
		heap.malloc(new Number(23), list2, nameTable);
		heap.malloc(new Number(22), list2, nameTable);
		heap.malloc(new Number(21), list2, nameTable);
		heap.malloc(list2, returnList2, nameTable);

		if (returnList2.getAddress() == -1) {
			System.out.println("Did not allocate space for List2, heap didn'thave enough room to store the whole list");
		} else {
			System.out.println("Allocated space on the heap for part of list2. It will be GCed next time space is needed.");
		}
		heap.dump();
	}
	public void execute() {
		test1();
		System.out.println("============================");
		test2();
		System.out.println("============================");
		test3();
		System.out.println("============================");
		test4();
		System.out.println("============================");
	}

	public static void main(String[] args) throws Exception {
		HeapTest test = new HeapTest();
		test.execute();
	}
}