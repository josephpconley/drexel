define size 
proc( L )
   x:=0;  
   if nullp(L) then 
        x := 0 
    else 
      z:= cdr(L);
      y:=size( z );
      x:=y+1   
    fi;
        return x
end;
cs := [1,2,3,4,[5,6]];
ANS := size(cs)