/* this is the cup file for the mini language
 * at http://www.cs.drexel.edu/~jjohnson/2006-07/winter/cs360/lectures/lec6.html *
 * created by Xu, 2/5/07
 *
 * Modified by Mike Kopack for CS550, 2009 Spring Qtr.
 * Should be at the same level of completeness as the Lecture 2c
 * C++ version.
 *
 * Last Modification by group 5 for CS550, 2010 Spring Qtr
 * Matt Caporali
 * Joe Conley
 * John Evans
 * Aaron Miller
 */

import java.util.*;

/*
 * Base class for Lists and Numbers
 *
 */
class Element {
    
    public static final String tempListName = "9999BADNAME";
    protected Integer myInteger;
    protected ArrayList<Element> myArrayList;

    //The address of this element
    protected int myAddress;

    public Element(){
        myAddress = -1;
        myInteger = new Integer(-1);
        myArrayList = new ArrayList<Element>();
    }

    public Element(int value){
        myAddress = -1;
        myInteger = new Integer(value);
        myArrayList = new ArrayList<Element>();
    }

    public Element(Element value){
        myAddress = -1;
        myInteger = new Integer(-1);
        myArrayList =  new ArrayList<Element>();
        myArrayList.add(value);
    }

    int intValue()
    {
        return myInteger.intValue();
    }

    ArrayList<Element> array()
    {
        return myArrayList;
    }

    public String toString()
    {
        if(myArrayList.size() > 0)
        {
            return myArrayList.get(0).toString();
        }
        else
        {
           return myInteger.toString();
        }
    }

    public int getAddress()
    {
        return myAddress;
    }

    public void setAddress(int address)
    {
        myAddress = address;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Element" );
        if(myArrayList.size() > 0)
        {
            Element temp = myArrayList.get(0).eval(nametable, functiontable, var, heap);

            // if the element is a list we need to set it's list pointer
            myAddress = temp.getAddress();
            System.out.println("Leaving element " + temp.getAddress() + " : " + myAddress + " " + temp.getClass().getName());
            return temp;
        }
        else
        {
            return new Element(-1);
        }
    }
}

/*
 * This class handles a concatenation of two lists
 *
 */
class Concat extends List
{
    Element expr1;
    Element expr2;

    public  Concat(Element l1, Element l2) {
            expr1 = l1;
            expr2 = l2;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {

        System.out.println("Evaluate Concat");
        Element eval1 = expr1.eval(nametable, functiontable, var, heap);
        Element eval2 = expr2.eval(nametable, functiontable, var, heap);

        Element ans;

        if(!(eval1 instanceof List) || !(eval2 instanceof List))
        {
             System.out.println("Not a list error:  expr1 is " + eval1.getClass().getName() +
                                        " expr2 is " + eval2.getClass().getName());
             ans = new Element(-1);
        }
        else
        {
             ans = new List();
            ((List)ans).add(eval1);

            //Adding temporary list variable
            nametable.put(tempListName, ans);
            if(!heap.malloc(eval1, (List)ans, nametable))
            {
                System.err.println("Unable to allocate space");
                nametable.remove(tempListName);
                return new List();
            }
            nametable.remove(tempListName);

            ((List)ans).add(eval2);

            //Adding temporary list variable
            nametable.put(tempListName, ans);
            if(!heap.malloc(eval2, (List)ans, nametable))
            {
                System.err.println("Unable to allocate space");
                nametable.remove(tempListName);
                return new List();
            }
            nametable.remove(tempListName);
        }


        return ans;
    }
}

/*
 * This class implements the listp operator.
 * the eval returns a 1 if the given expression is
 * a list and returns 0 if it an number
 */
class Listp extends Expr
{
     private Element expr1;

     public Listp(Element op1) {
        expr1 = op1;
     }

     public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {

    	System.out.println("Evaluate Listp");
		Element eval1 = expr1.eval(nametable, functiontable, var, heap);
		Number ans;

    	if(eval1 instanceof List){
            ans = new Number(1);
        }
        else
        {
            ans = new Number(0);
        }

    	return ans;
     }
}

/*
 * This class implements the intp operator.
 * the eval method returns a 0 if the given expression
 * is a list or 1 if it is an number
 */
class Intp extends Expr
{
    private Element expr1;

    public Intp(Element e) {
        expr1 = e;
    }

    public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Intp");
        Element e = expr1.eval(nametable,functiontable,var, heap);
        Number ans;
        if(e instanceof Number){
            ans = new Number(1);
        }else{
            ans = new Number(0);
        }

        return ans;
    }
}

/*
 * This class implements the nullp operator.
 * the eval method returns a 1 if the given list is null
 * otherwise it returns 0
 */
class Nullp extends Expr
{
    private Element expr1;

    public Nullp(Element e) {
        expr1 = e;
    }

    public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Nullp");
        Number ans;
        Element e = expr1.eval(nametable,functiontable,var,heap);

        if(e instanceof List)
        {
            ans = new Number(heap.nullp((List)e));
        }
        else
        {
            //TODO throw violent exception
            System.out.println("Nullp evaluating " + e.getClass().getName() + " should be List ");
            ans = new Number(-1);
        }
        return ans;
    }
}

/*
 * This class implements the cdr operator.
 * the eval method returns a sublist of
 * an input list, returning the list minus the head element
 */
class Cdr extends List
{
    private Element expr1;

    public Cdr(Element op1) {
        expr1 = op1;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Cdr");
        Element eval1 = expr1.eval(nametable, functiontable, var, heap);
        Element ans;

        if(eval1 instanceof List)
        {
             ArrayList<Element> newList = ((List)eval1).eval();
             if (newList.size() > 0) {
                System.out.println("\tNew List Size " + newList.size());
                ans = new List(new ArrayList<Element>(newList.subList(1, newList.size())));
                ans.setAddress(heap.cdr((List)eval1));
             } else {
                System.out.println("Cannot retrieve cdr value of a list with length of 1 element.");
                ans = new List();
             }
        }
        else
        {
             // TODO: This should probably throw a violent parser killing exception
             System.out.println("Not a list error:  expr1 is " + eval1.getClass().getName());
             ans = new Element(-1);
        }

        return ans;
    }
}

/*
 * This class implements the car operator.
 * The eval method returns the first element in the list
 */
class Car extends Expr
{
    private Element expr1;

    public Car(Element op1) {
        expr1 = op1;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Car");
        Element eval1 = expr1.eval(nametable, functiontable, var, heap);

        Element ans;

        if(eval1 instanceof List)
        {
            ArrayList<Element> newList = ((List)eval1).eval();
            ans = newList.get(0);

            // Calling heap car method
            heap.car(ans, (List)eval1);
        }
        else
        {
            // TODO: This should probably throw a violent parser killing exception
            System.out.println("Not a list error:  expr1 is " + eval1.getClass().getName());
            ans = new Element(-1);
        }

        return ans;
    }
}

/*
 * This class implements the cons operator.
 */
class Cons extends Expr
{
    private Element expr1;
    private Element expr2;

    public Cons(Element op1, Element op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        Element ans;

    	Element eval2 = expr2.eval(nametable, functiontable, var, heap);

    	if(eval2 instanceof List)
    	{
            Element eval1 = expr1.eval(nametable, functiontable, var, heap);

            // Need temporary list to prevent overwritting
            // an ident list
            ArrayList<Element> tempArray = new ArrayList<Element>(((List)eval2).eval());
            List temp = new List(tempArray, eval2.getAddress());

            temp.eval().add(0,eval1);

            ans = temp;

            //Adding temporary list variable
            nametable.put(tempListName, temp);
            if(!heap.malloc(eval1, temp, nametable))
            {
                System.err.println("Unable to allocate space");
                nametable.remove(tempListName);
                return new List();
            }
            nametable.remove(tempListName);

    	}
    	else
    	{
            // TODO: This should probably throw a violent parser killing exception
            System.out.println("Not a list error:  expr2 is " + eval2.getClass().getName());
            ans = new Element(-1);
    	}

        return  ans;
    }
}


class List extends Element{

    public ArrayList<Element> eval() {
        return myArrayList;
    }

    public List(Element l, Element e)
    {
        System.out.println("Constructing from 2 element: " + l.getClass().getName() + " and "
                                   + e.getClass().getName());

        if(e instanceof List)
        {
            this.myArrayList =  ((List)e).eval();
        }
        else
        {
            this.myArrayList = new ArrayList<Element>();
            this.myArrayList.add(e);
        }
        this.myArrayList.add(0,l);


    }


    public List(Element l){
        super();

        System.out.println("Constructing List from element: " + l.getClass().getName());
        if(l instanceof List)
        {
            this.myArrayList  = ((List)l).eval();
        }
        else
        {
            this.myArrayList = new ArrayList<Element>();
            this.myArrayList.add(l);
        }

    }

    public List(ArrayList<Element> list){
        this.myArrayList = list;
    }

    public List(ArrayList<Element> list, int address){
        this.myArrayList = list;
        this.myAddress = address;
    }

    public List(){
        this.myArrayList = new ArrayList<Element>();
    }

    public void add(Element l)
    {
        this.myArrayList.add(l);
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        // recursive eval of the Elements in the list , could be expr(or ultimately Number) could be list(combine)
        System.out.println("Evaluate List of size " + myArrayList.size());

        ArrayList<Element> evalList = new ArrayList<Element>();
        List returnedList = new List();

        for(int i = myArrayList.size() - 1; i > -1; i--)
        {
            Element v = myArrayList.get(i).eval(nametable, functiontable, var, heap);
            //evalList.add(v);
            //Need to make sure the add is working the same as above or use returnedList.eval().add(0,v);
            if(v instanceof List)
            {
                System.out.println("Returned List Address " + ((List)v).getAddress());

            }
            evalList.add(0, v);
            nametable.put(tempListName, returnedList);
            if(!heap.malloc(v, returnedList, nametable))
            {
                System.err.println("Unable to allocate space");
                nametable.remove(tempListName);
                return new List();
            }
            nametable.remove(tempListName);
        }
        return new List(evalList, returnedList.getAddress());
    }


    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < myArrayList.size(); i++) {
            if (i > 0) {
                    builder.append(",");
            }

            builder.append(myArrayList.get(i).toString());
        }
        builder.append("]");
        return builder.toString();
    }
}


class Expr extends Element
{
    int intValue()
    {
        return myInteger.intValue();
    }

    public Expr() {
        myInteger = new Integer(-1);
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        System.out.println("Evaluate Expr");
        return this;
    }
}

class Ident extends Expr {

    private String name;

    public Ident(String s) {
        name = s;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
    	System.out.println("Evaluate Ident " + name);
    	Element returnValue = null;

        if (name != null && nametable.containsKey(name)) {
            returnValue =  nametable.get(name);
        }
        System.out.println("Returning Type : " + returnValue.getClass().getName());
        return returnValue;
    }
}

class Number extends Expr {

    public Number(int n) {
    	super();
        myInteger = new Integer(n);
    }

    public Number(Integer n) {
    	super();
        myInteger = n;
    }

   public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
       System.out.println("Evaluate Number " + myInteger);
       return this;
    }

    public int intValue() {
        return myInteger.intValue();
    }
}

class Times extends Expr {

    private Expr expr1,  expr2;

    public Times(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
    	System.out.println("Evaluate Times");
    	return new Number(expr1.eval(nametable, functiontable, var, heap).intValue() * expr2.eval(nametable, functiontable, var, heap).intValue());
    }
}

class Plus extends Expr {

    private Expr expr1,  expr2;

    public Plus(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
    	System.out.println("Evaluate Plus");
    	return new Number(expr1.eval(nametable, functiontable, var, heap).intValue() + expr2.eval(nametable, functiontable, var, heap).intValue());
    }
}

class Minus extends Expr {

    private Expr expr1,  expr2;

    public Minus(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Number eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
    	//TODO: Add check to make sure intValue is -1 otherwise I'm trying to do something with a list
    	System.out.println("Evaluate Minus");
        return new Number(expr1.eval(nametable, functiontable, var, heap).intValue() - expr2.eval(nametable, functiontable, var, heap).intValue());
    }
}

//added for 2c
class FunctionCall extends Expr {

    private String funcid;
    private ExpressionList explist;

    public FunctionCall(String id, ExpressionList el) {
        funcid = id;
        explist = el;
    }

    public Element eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        return functiontable.get(funcid).apply(nametable, functiontable, var, heap, explist);
    }
}

abstract class Statement {

    public Statement() {
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {
    }
}

// added for 2c
class DefineStatement extends Statement {

    private String name;
    private Proc proc;
    private ParamList paramlist;
    private StatementList statementlist;

    public DefineStatement(String id, Proc process) {
        name = id;
        proc = process;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functable, LinkedList var, Heap heap) {
        // get the named proc object from the function table.
        //System.out.println("Adding Process:"+name+" to Functiontable");
        functable.put(name, proc);
    }
}

class ReturnStatement extends Statement {

    private Expr expr;

    public ReturnStatement(Expr e) {
        expr = e;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {
        //Java can't throw exceptions of numbers, so we'll convert it to a string
        //and then on the other end we'll reconvert back to Integer..
        throw new Exception(Integer.toString(expr.eval(nametable, functiontable, var, heap).intValue()));
    }
}

class AssignStatement extends Statement {

    private String name;
    private Element expr;

    public AssignStatement(String id, Element e) {
        name = id;
        expr = e;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
    	System.out.println("Evaluate Assign " + name);
    	/* add name to the statementlist of variable names */
        if (!var.contains(name)) {
            var.add(name);
        //insert the variable with the specified name into the table with the
        // evaluated result (which must be an integer
        }
        
        nametable.put(name, expr.eval(nametable, functiontable, var, heap));
        
    }
}

class IfStatement extends Statement {

    private Expr expr;
    private StatementList stmtlist1,  stmtlist2;

    public IfStatement(Expr e, StatementList list1, StatementList list2) {
        expr = e;
        stmtlist1 = list1;
        stmtlist2 = list2;
    }

    public IfStatement(Expr e, StatementList list) {
        expr = e;
        stmtlist1 = list;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {
    	System.out.println("Start Evaluate IfStatement ");
        if (expr.eval(nametable, functiontable, var, heap).intValue() > 0) {
            stmtlist1.eval(nametable, functiontable, var, heap);
        } else {
            stmtlist2.eval(nametable, functiontable, var, heap);
        }
        System.out.println("End Evaluate IfStatement ");
    }
}

class WhileStatement extends Statement {

    private Expr expr;
    private StatementList stmtlist;

    public WhileStatement(Expr e, StatementList list) {
        expr = e;
        stmtlist = list;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {
    	System.out.println("Start Evaluate WhileStatement ");
        while (expr.eval(nametable, functiontable, var, heap).intValue() > 0) {
            stmtlist.eval(nametable, functiontable, var, heap);
        }
        System.out.println("End Evaluate WhileStatement ");
    }
}

class RepeatStatement extends Statement {

    private Expr expr;
    private StatementList sl;

    public RepeatStatement(StatementList list, Expr e) {
        expr = e;
        sl = list;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {
    	System.out.println("Start Evaluate RepeatStatement ");
    	do {
            sl.eval(nametable, functiontable, var, heap);
        } while (expr.eval(nametable, functiontable, var, heap).intValue() > 0);
    	System.out.println("End Evaluate RepeatStatement ");
    }
}

//added for 2c
class ParamList {

    private LinkedList<String> parameterlist;

    public ParamList(String name) {
        parameterlist = new LinkedList<String>();
        parameterlist.add(name);
    }

    public ParamList(String name, ParamList parlist) {
        parameterlist = parlist.getParamList();
        parameterlist.add(name);
    }

    public LinkedList<String> getParamList() {
        return parameterlist;
    }
}

// Added for 2c
class ExpressionList {

    private LinkedList<Expr> list;

    public ExpressionList(Expr ex) {
        list = new LinkedList<Expr>();
        list.add(ex);
    }

    public ExpressionList(Expr ex, ExpressionList el) {
        list = new LinkedList<Expr>();
        //we need ot add the expression to the front of the list
        list.add(0, ex);

    }

    public LinkedList<Expr> getExpressions() {
        return list;
    }
}

class StatementList {

    private LinkedList<Statement> statementlist;

    public StatementList(Statement statement) {
        statementlist = new LinkedList<Statement>();
        statementlist.add(statement);
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) throws Exception {

    	System.out.println("Evaluate StatementList");
        for (Statement stmt : statementlist) {
            stmt.eval(nametable, functiontable, var, heap);

        }
    }

    public void insert(Statement s) {
        // we need to add it to the front of the list
        statementlist.add(0, s);
    }

    public LinkedList<Statement> getStatements() {
        return statementlist;
    }
}

class Proc {

    private ParamList parameterlist;
    private StatementList stmtlist;

    public Proc(ParamList pl, StatementList sl) {
        parameterlist = pl;
        stmtlist = sl;
    }

    public Element apply(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap, ExpressionList expressionlist) {
        //System.out.println("Executing Proceedure");
        HashMap<String, Element> newnametable = new HashMap<String, Element>();

        // bind parameters in new name table
        // we need to get the underlying List structure that the ParamList uses...
        Iterator<String> p = parameterlist.getParamList().iterator();
        Iterator<Expr> e = expressionlist.getExpressions().iterator();

        if (parameterlist.getParamList().size() != expressionlist.getExpressions().size()) {
            System.out.println("Param count does not match");
            System.exit(1);
        }
        while (p.hasNext() && e.hasNext()) {

            // assign the evaluation of the expression to the parameter name.
            newnametable.put(p.next(), e.next().eval(nametable, functiontable, var, heap));
        //System.out.println("Loading Nametable for procedure with: "+p+" = "+nametable.get(p));

        }
        // evaluate function body using new name table and
        // old function table
        // eval statement list and catch return
        //System.out.println("Beginning Proceedure Execution..");
        try {
            stmtlist.eval(newnametable, functiontable, var, heap);
        } catch (Exception result) {
            // Note, the result shold contain the proceedure's return value as a String
            //System.out.println("return value = "+result.getMessage());
            return new Element(Integer.parseInt(result.getMessage()));
        }
        System.out.println("Error:  no return value");
        System.exit(1);
        // need this or the compiler will complain, but should never
        // reach this...
        return new Element(-1);
    }
}

class Program {

    private StatementList stmtlist;

    public Program(StatementList list) {
        stmtlist = list;
    }

    public void eval(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        try {
            stmtlist.eval(nametable, functiontable, var, heap);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void dump(HashMap<String, Element> nametable, HashMap<String, Proc> functiontable, LinkedList var, Heap heap) {
        //System.out.println(hm.values());
        System.out.println("Dumping out all the variables...");
        if (nametable != null) {
            for (String name : nametable.keySet()) {
            	if(nametable.get(name) instanceof List)
            	{
                   System.out.println(name + "=" + nametable.get(name));
                   System.out.println("\t" + name + " points to heap location " + nametable.get(name).getAddress() );
            	}
            	else
            	{
            	   System.out.println(name + "=" + nametable.get(name));
            	}
            }
        }
        if (functiontable != null) {
            for (String name : functiontable.keySet()) {
                System.out.println("Function: " + name + " defined...");
            }
        }
        heap.dump();
    }
}
