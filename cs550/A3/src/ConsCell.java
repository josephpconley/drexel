/**
 * This class represents a single, fixed-size memory location
 * on our fictional memory heap.
 *
 */
public class ConsCell {

	/**
	 * car represents the data to be stored
	 * storing the value -1 indicates the cell is free
	 */
	private int car;

	/**
	 * Points to the next cell in a list
	 * if <code>list</code> is <code>true</code>.
	 *
	 * If <code>list</code> is <code>true</code>
	 * and the value is -1 it indicates the end of the list.
	 *
	 * Otherwise, if <code>list</code> is <code>false</code>
	 * this field should be set to -1 and ignored.
	 */
	private int cdr;

	/**
	 * Indicates that this cell is pointer to a list or not.
	 */
	private Boolean pointer;

	/*
	 * Indicates that a cell is marked as reachable
	 */
	private Boolean reachableFlag;


	public ConsCell() {
		this(-1, -1, false);
	}

	public ConsCell(int car) {
		this(car, -1, false);
	}

	public ConsCell(int car, int cdr) {
		this(car, cdr, false);
	}

	public ConsCell(int car, int cdr, Boolean pointer) {
		this.car = car;
		this.cdr = cdr;
		this.pointer = pointer;
		this.reachableFlag = false;
	}

	public int getCar() {
		return car;
	}
	public void setCar(int car) {
		this.car = car;
	}
	public int getCdr() {
		return cdr;
	}
	public void setCdr(int cdr) {
		this.cdr = cdr;
	}

	/**
	 * resets the values stored in the memory represented by this cell.
	 */
	public void reset() {
		setCar(-1);
		setCdr(-1);
	}

	public boolean isList() {
		return pointer;
	}

	public void setListPointerFlag()
	{
		pointer = true;
	}

	public void setReachableFlag()
	{
		reachableFlag = true;
	}

	public void unSetReachableFlag()
	{
		reachableFlag = false;
	}

	public boolean isReachable()
	{
		return reachableFlag;
	}

	public boolean isLastElementInList() {
		return (cdr == 1);
	}
	public void setList(boolean list) {
		this.pointer = list;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder("\t");
		builder.append(car);
		builder.append("\t");
		//builder.append("cdr='");
		builder.append(cdr);
		builder.append("\t");
		//builder.append("isPointer='");
		builder.append(pointer ? "X" : " ");
		builder.append("\t");
		//builder.append("isReachable='");
		builder.append(reachableFlag ? "X" : " ");
		builder.append("\t");
		return builder.toString();
	}
}