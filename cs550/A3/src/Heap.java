import java.util.*;

public class Heap {
    private int MAX_SIZE;
    private ConsCell[] heap;

    private boolean[] availableCells;

    /**
     * Creates a heap of the default size
     */
    public Heap() {
            this(25);
    }

    /**
    * Creates a heap of the size specified in <code>maxHeapSize</code>
    * @param maxHeapSize - maximum size of the heap
    */
    public Heap(int maxHeapSize) {
        MAX_SIZE = maxHeapSize;
        availableCells = new boolean[MAX_SIZE];
        heap = new ConsCell[MAX_SIZE];

        for (int i = 0; i < MAX_SIZE; i++) {
                heap[i] = new ConsCell();
                availableCells[i] = true;
        }
    }

    /*
    * This takes in the List from the Cdr class
    * and returns a new list with it's address
    * set to the next element in the list
    */
    public int cdr(List list)
    {
        int address = list.getAddress();

        //List returnList = new List();

        //Check to see if this is list in a list
        if(heap[address].isList())
        {
            return heap[address].getCar();
        }
        else // no it's an element
        {
            return heap[address].getCdr();
        }
    }


    /*
     * This takes in the List and checks its address to see if its valid
     */
    public void car (Element returnElement, List list) {

        int address = list.getAddress();

        if(heap[address].isList())
        {
            returnElement.setAddress( heap[address].getCar() );
        }
        // The first element is not a list
        // it is just a number, we don't need to
        // set the address of the element
    }

    /*
     * This takes in the List and checks its address to see if its valid
     */
    public int nullp(List list) {
        int nullList = 0;

        if(list.getAddress() < 0)
        {
            nullList = 1;
        }

        return nullList;
    }

    /**
    * Allocate a new int cell.
    * @return
    */
    public boolean malloc(Element e, List l, HashMap<String, Element> nametable)
    {
        int startIndex = firstFitAllocation();
        if (startIndex == -1) {
            System.out.println("Failed allocating 1 space. Attempting to Garbage collect.");
            gc(nametable);
            startIndex = firstFitAllocation();
        }

        if (startIndex != -1) {
            System.out.println("Allocated 1 cells, storing data and marking cells as used for " + startIndex + " : " + l.getAddress());
            markCellsAsUsed(startIndex, 1);

        }
        if (startIndex != -1) {
            // When it's a number
            if(e instanceof Number)
            {
                    heap[startIndex] = new ConsCell(((Number) e).intValue(), l.getAddress(), false);
                    l.setAddress(startIndex);
            }
            else// When it's a list, this isn't quite right there are intermediate steps where a
                    // list is returned
            {

                    //Check to see if the address has been intialized
                    // if it hasn't been set then the value for cdr is
                    // the same as car

                    int listAddress = l.getAddress();
                    if (listAddress == -1)
                    {
                            listAddress = ((List) e).getAddress();
                    }

                    System.out.println("Setting address " + ((List) e).getAddress() + " : " + l.getAddress());
                    heap[startIndex] = new ConsCell(((List) e).getAddress(), listAddress, true);
                    l.setAddress(startIndex);
            }
        }
        return (startIndex != -1);
    }

    private void storeData(int startIndex, int[] input, boolean isList) {
        int nextIdx = -1;
        for (int i = startIndex; i < (startIndex + input.length); i++) {
            if (isList) {
                if (i < (startIndex + input.length - 1)) {
                        nextIdx = i + 1;
                } else {
                        //end of list
                        nextIdx = -1;
                }
            }
            heap[i] = new ConsCell(input[i - startIndex], nextIdx, isList);
        }
    }

    private void markCellsAsUsed(int startIndex, int size) {
        for (int i = startIndex; i < (startIndex + size); i++) {
            availableCells[i] = false;
        }
    }

    /*
     *  I have a starting index into the Heap
     */
    private void markCells(int address)
    {
        if(address != -1)  // if it's not null;
        {
            // paint as reachable
            heap[address].setReachableFlag();

            //check to see if this a list in alist
            if(heap[address].isList())
            {
                //recurse on the pointed list
                markCells(heap[address].getCar());
            }

            // go to the next cell
            int nextAddress = heap[address].getCdr();

            if(nextAddress != -1)
            {
                // recurse
                markCells(nextAddress);
            }
            else  // it's -1 (I have reached the end)
            {
                return;
            }
        }
    }

    /*
    * go through and ignore all that were marked
    * returns the number of cells freed
    */
    private int sweepAndClear()
    {
        int freedCells = 0;
        for (int i = 0; i < MAX_SIZE; i++)
        {
            if(heap[i].isReachable())
            {
                // clear
                heap[i].unSetReachableFlag();
            }
            else
            {
                //sweep
                heap[i] = new ConsCell();
                availableCells[i] = true;
                freedCells++;
            }
        }
        return freedCells;
    }

    /**
    * Will only ever need to allocate 1 cell at a time, but
    * uses a first fit allocation algorithm
    * @param size
    * @return
    */
    private int firstFitAllocation() {
        int startIndex = -1;

        for (int i = 0; (startIndex == -1) && (i < MAX_SIZE); i++) {
            if (availableCells[i]) {
                    startIndex = i;
            }
        }
        return startIndex;
    }

    /**
    * Garbage collect the heap, return true if any
    * ConsCells were freed up on this iteration of garbage
    * collection
    * @return
    */

    // As they are removed from the nametable
    // Or when they are asigned to something else
    private boolean gc(HashMap<String, Element> nametable) {
        boolean reclaimedCells = false;
        System.out.println("Initiating garbage collection: ");
        Set<String> namesSet = nametable.keySet();
        // Go through all of the cells
        for(String names : namesSet)
        {
            Element e = nametable.get(names);
            // if it's a list I need to mark what cells it can reach
            if(e instanceof List)
            {
                    // Mark the cells, only care about lists
                markCells(e.getAddress());
            }
        }

        // Now sweep out what is not marked
        Integer numberCleared = sweepAndClear();

        System.out.println("Completed garbage collection: " +numberCleared + " cells were reclaimed");
        return reclaimedCells;
    }

    public void dump() {
        System.out.print("Available cells list: [");
        for (int i = 0; i < MAX_SIZE; i++) {
            if (i != 0) {
                    System.out.print(", ");
            }
            System.out.print(availableCells[i]);
        }
        System.out.println("]");

        System.out.println("Heap contents: ");
        System.out.println("\tMem Loc\tCAR\tCDR\tListPtr\t");

        for (int i = 0; i < MAX_SIZE; i++) {
            System.out.println("\t" + i + heap[i]);
        }
    }
}
