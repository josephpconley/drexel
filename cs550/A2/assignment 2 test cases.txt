This assignment extends the mini language and interpreter from Lecture 2. It also builds on the Assignment 1. You are to extend the mini language to support lists. That is variables may be assigned lists, lists may be returned by functions, and lists can occur in expressions. First you must change the symbol table so that variables may be bound to either integers or lists (use a class called Element which can be an Integer or List). Second you must extend the syntax of the language to support list constants (use "[" and "]" to enclose lists so as not to be confused with "(" and ")" which are used to group arithmetic expressions). E.G. [1,2,[3,4]].

You must also support the following functions

    * cons( e, L ) - appends element e to the front of list
    * car( L ) - returns the first element in the list
    * cdr( L ) - returns the rest of the list (minus the first element)
    * nullp( L ) - returns 1 if L is null, 0 otherwise
    * intp( e ) - returns 1 if e is an integer, 0 otherwise
    * listp( e ) - returns 1 if e is a list, 0 otherwise to allow construction and access to lists.


===================================================================================
==== after this runs should result in:
==== a = 0
==== b = 24
===================================================================================
a:=0-4;
if a then a:=a else a:=0-a fi;
b := 1;
while a do
b:=b*a;
a:=a-1
od
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := [1,2,3,4];
b := 5;
c := b || a;
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := 7;
b := 5;
c := b || a;
===================================================================================
==== after this runs should result in:
==== [5,6,1,2,3,4]
===================================================================================
a := [1,2,3,4];
b := [5,6];
c := b || a;
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := [1,2,3,4];
b := [5,6];
c := b - a;
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := [1,2,3,4];
b := [5,6];
c := b + a;
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := [1,2,3,4];
b := [5,6];
c := b * a;
===================================================================================
==== after this runs should result in:
==== c = 3
===================================================================================
a := 3;
b := 6;
c := b - a;
===================================================================================
==== after this runs should result in:
==== c = 9
===================================================================================
a := 3;
b := 6;
c := b + a;
===================================================================================
==== after this runs should result in:
==== c = 18
===================================================================================
a := 3;
b := 6;
c := b * a;
===================================================================================
==== after this runs should result in:
==== a = [5,1,2,3,4]
===================================================================================
a := [1,2,3,4];
b := 5;
cons(b,a);
===================================================================================
==== after this runs should result in:
==== a = [[5,6],1,2,3,4]
===================================================================================
a := [1,2,3,4];
b := [5,6];
cons(b,a);
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := [1,2,3,4];
b := 5;
cons(a,b);
===================================================================================
==== after this runs should result in:
==== b = 1
===================================================================================
a := [1,2,3,4];
b := car(a);
===================================================================================
==== after this runs should result in:
==== b = [1,2]
===================================================================================
a := [[1,2],3,4];
b := car(a);
===================================================================================
==== after this runs should result in:
==== b = ERROR
===================================================================================
a := 1;
b := car(a);
===================================================================================
==== after this runs should result in:
==== b = [2,3,4]
===================================================================================
a := [1,2,3,4];
b := cdr(a);
===================================================================================
==== after this runs should result in:
==== b = [3,4,[5,6]];
===================================================================================
a := [[1,2],3,4,[5,6]];
b := cdr(a);
===================================================================================
==== after this runs should result in:
==== b = ERROR
===================================================================================
a := 1;
b := cdr(a);
===================================================================================
==== after this runs should result in:
==== b = 0
===================================================================================
a := [[1,2],3,4,[5,6]];
b := nullp(a);
===================================================================================
==== after this runs should result in:
==== b = 1
===================================================================================
a := [];
b := nullp(a);
===================================================================================
==== after this runs should result in:
==== ERROR
===================================================================================
a := 0;
b := nullp(a);
===================================================================================
==== after this runs should result in:
==== b = 0
===================================================================================
a := [[1,2],3,4,[5,6]];
b := intp(a);
===================================================================================
==== after this runs should result in:
==== b = 1
===================================================================================
a := 0;
b := intp(a);
===================================================================================
==== after this runs should result in:
==== b = 1
===================================================================================
a := [[1,2],3,4,[5,6]];
b := listp(a);
===================================================================================
==== after this runs should result in:
==== b = 1
===================================================================================
a := [];
b := listp(a);
===================================================================================
==== after this runs should result in:
==== b = 0
===================================================================================
a := 0;
b := listp(a);
===================================================================================
==== after this runs should result in:
==== a = 5
===================================================================================
a:=[5,1,3];
if intp(a) then a:= a else a:= car(a) fi;
while listp(a) do
a:= car(a);
od
===================================================================================
==== after this runs should result in:
==== a = 5
===================================================================================
a:=[[5,4,2],1,3];
if intp(a) then a:= a else a:= car(a) fi;
while listp(a) do
a:= car(a);
od
===================================================================================
==== after this runs should result in:
==== a = 5
===================================================================================
a:= 5;
if intp(a) then a:= a else a:= car(a) fi;
while listp(a) do
a:= car(a);
od
===================================================================================
==== after this runs should result in:
==== a = 1
===================================================================================
a:= [[[1,2],[3,4,5],6],7,[8,9,10]];
if intp(a) then a:= a else a:= car(a) fi;
while listp(a) do
a:= car(a);
od
===================================================================================
==== after this runs should result in:
==== a = 7
===================================================================================
a:= [[[1,2],[3,4,5],6],7,[8,9,10]];
if intp(a) then a:= a else a:= cdr(a) fi;
while listp(a) do
a:= car(a);
od
===================================================================================
==== after this runs should result in:
==== a = 1
===================================================================================
a:= [[[1,2],[3,4,5],6],7,[8,9,10]];
if intp(a) then a:= a else while listp(a) do a:= car(a); od fi;
===================================================================================
==== after this runs should result in:
==== a = 99
===================================================================================
a:= [[[1,2],[3,4,5],6],7,[8,9,10]];
while list(a) do
if intp(a) then a := a else if nullp(a) then a := 99; else a := cdr(a); fi; fi;
od