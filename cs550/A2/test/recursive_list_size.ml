define size proc( L )
   if nullp(L) then return := 0 else return x:=1+size( cdr(L) ) fi;
end;
L1 := [1,2,3];
ANS := size(L1)