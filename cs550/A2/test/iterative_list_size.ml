define getListSize proc (L)
	repeat 
		c := cdr(L);
		L := c;
		x := x + 1;
	until nullp(L);
	return x;
end;

L1 := [1,2,[3,4,5]];
ANSWER := getListSize(L1)
