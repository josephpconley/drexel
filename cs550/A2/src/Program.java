/* this is the cup file for the mini language
 * at http://www.cs.drexel.edu/~jjohnson/2006-07/winter/cs360/lectures/lec6.html *
 * created by Xu, 2/5/07
 *
 * Modified by Mike Kopack for CS550, 2009 Spring Qtr.
 * Should be at the same level of completeness as the Lecture 2c
 * C++ version.
 * 
 * Last Modification by group 5 for CS550, 2010 Spring Qtr
 * Matt Caporali 
 * Joe Conley
 * John Evans
 * Aaron Miller
 */

import java.util.*;

/*
 * Base class for Lists and Numbers
 * 
 */
class Element extends Expr{
	public Element(){
	}

    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        return new Integer(0);
    }
}

/*
 * This class implements the cons operator. 
 */
class Cons extends List
{
	 private Expr expr1;
	 private Expr expr2;

	    public Cons(Expr op1, List op2) {
	        expr1 = op1;
	        expr2 = op2;
	    }
	    
	    public Cons(Ident op1, Ident op2) {
	        expr1 = op1;
	        expr2 = op2;
	    }
	    
	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
	    	ArrayList<Object> list = null;
	    	try
	    	{
				if (expr2 != null) {
					list = (ArrayList) expr2.eval(nametable, functiontable, var);
				} else {
					list = new ArrayList<Object>();
				}
				
				list.add(0, expr1.eval(nametable, functiontable, var));
	    	}
	    	catch (Exception e)
	    	{
	    		System.out.println("Wrong Type");
	    	}
			return  new List(list);
	    }
}

/*
 * This class implements the listp operator. 
 * the eval returns a 1 if the given expression is
 * a list and returns 0 if it an number
 */
class Listp extends Expr
{
	 private Expr expr1;
	 private String stringValue;

	    public Listp(Expr op1) {
	        expr1 = op1; 
	    }

	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
	    	
	    	if(expr1 instanceof List){
				Number n = new Number(1);
				return (Expr)n;
			}
			else if (expr1 instanceof Ident )
			{
				Number n = null;
				Object value = expr1.eval(nametable, functiontable, var);
				if(value instanceof Integer)
				{
					n = new Number(0);
				}
				
				 else {
					n = new Number(1);
				}
				return (Expr)n;
			}else{
				Number n = new Number(0);
				return (Expr)n;
			}

	    }
}

/*
 * This class implements the car operator. 
 * The eval method returns the first element in the list
 */
class Car extends Expr
{
	 private Expr expr1;

	    public Car(List op1) {
	        expr1 = op1;
	    }
	    
	    public Car(Ident op1) {
	        expr1 = op1;
	    }

	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
	    	try
	    	{
					ArrayList newList = (ArrayList) expr1.eval(nametable, functiontable, var);
					value = newList.get(0);
	    	}
	    	catch (Exception e)
	    	{
	    		System.out.println("Wrong Type being passed to Car");
	    	}
				return value;
	    }
}

/*
 * This class implements the cdr operator. 
 * the eval method returns a sublist of
 * an input list, returning the list minus the head element
 */
class Cdr extends Expr
{
	 private Expr expr1;

	    public Cdr(List op1) {
	        expr1 = op1;
	    }
	    
	    public Cdr(Ident op1) {
	        expr1 = op1;
	    }

	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
	    	try
	    	{
					ArrayList newList = (ArrayList) expr1.eval(nametable, functiontable, var);
					value = new List(new ArrayList(newList.subList(1, newList.size())));
	    	}
	    	catch (Exception e)
	    	{
	    		System.out.println("Wrong Type being passed to Cdr");
	    	}
			return value;
	    }
}

/*
 * This class implements the intp operator. 
 * the eval method returns a 0 if the given expression
 * is a list or 1 if it is an number
 */
class Intp extends Expr
{
	 private Expr expr1;

	    public Intp(Expr e) {
	    	expr1 = e;
	    }
	    
	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
	    	Object e = (Object)expr1.eval(nametable,functiontable,var);

	    	if(e instanceof java.lang.Integer){
	    		return new Integer(1);
	    	}else{
	    		return new Integer(0);
	    	}
	    }
}

/*
 * This class implements the nullp operator. 
 * the eval method returns a 1 if the given list is null
 * otherwise it returns 0
 */
class Nullp extends Expr
{
	 private Expr expr1;

	    public Nullp(Expr e) {
	    	expr1 = e;
	    }
	    
	    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {

	    	ArrayList l = (ArrayList)expr1.eval(nametable,functiontable,var);
	    	if(l == null || l.size() == 0){
	    		return new Integer(1);
	    	}else{
	    		return new Integer(0);
	    	}
	    }
}

/*
 * This class works at parse time
 * to help consolidate individual list
 * elements into a single list
 */
class ListHelper extends List
{
	public static List cons(Expr e, List l){
		ArrayList<Object> list = null;

			if (l != null) {
				list = (ArrayList) l.eval();
			} else {
				list = new ArrayList<Object>();
			}
			
			list.add(0, e);
			return new List(list);
		}
}

/*
 * This class handles a concatenation of two lists
 * 
 */
class CONCAT extends List
{
	public static Expr eval(List l1, List l2) {
		ArrayList<Object> list = (ArrayList) l1.eval();
		list.addAll((ArrayList) l2.eval());
		return new List(list);
	}
}

/*
 * This class handles lists within the parser
 * currently it doesn't support evaluating an expression
 * as an element. Thus a:=[1, 2, (3-4)] is not valid
 */
class List extends Element{
	public ArrayList<Object> list;

	public Object eval() {
		return list;
	}
	
	public List(Expr l){
		this.list = new ArrayList<Object>();
		if (l instanceof List) {
			this.list.addAll((ArrayList) ((List) l).eval());
		} else {
			this.list.add(l);
		}
	}

	public List(ArrayList<Object> list){
		this.list = list;
	}

	public List(){
		list = new ArrayList<Object>();
	}

	public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
		return list;
	}
	

	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i > 0) {
				builder.append(",");
			}
			builder.append(list.get(i).toString());
		}
		return builder.toString();
  }
}

/*
 * The expression base class
 */
class Expr {
    protected Object value;
    
    protected boolean isNumber = true;
    
    public Expr() {
    }

    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        return new Integer(-1);
    }
    
    public String toString() {
		
	   return String.valueOf(value);
    }

    
    boolean isNumber()
    {
    	return isNumber;
    }
    
    void setNotNumber()
    {
    	isNumber = false;
    }


		public Object eval() {
			return new Integer(-1);
		}

}

class Ident extends Expr {

    private String name;
    
    public int integer; //set to 1 if just a number;

    public Ident(String s) {
    	integer = 1;
        name = s;
    }
    
    public Ident(String s, int i)
    {
    	integer = i;
    	name = s;
    }

    public Object eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
				Object returnValue = null;

				if (name != null && nametable.containsKey(name)) {
					returnValue =  nametable.get(name);
				}
        return returnValue; 
    }
}

class Number extends Element {

    private Integer value;

    public Number(int n) {
        value = new Integer(n);
    }

    public Number(Integer n) {
        value = n;
    }

    public Integer eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        return value;
    }

    public String toString() {
		return String.valueOf(value);
	}
}

class Times extends Expr {

    private Expr expr1,  expr2;

    public Times(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Integer eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
    	value =  new Integer(((Integer) expr1.eval(nametable, functiontable, var)) * ((Integer) expr2.eval(nametable, functiontable, var)));
        return (Integer)value;
    }
}

class Plus extends Expr {

    private Expr expr1,  expr2;

    public Plus(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Integer eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        value = new Integer(((Integer) expr1.eval(nametable, functiontable, var)).intValue() + ((Integer) expr2.eval(nametable, functiontable, var)).intValue());       
        return (Integer)value;
    }
}

class Minus extends Expr {

    private Expr expr1,  expr2;

    public Minus(Expr op1, Expr op2) {
        expr1 = op1;
        expr2 = op2;
    }

    public Integer eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        value = new Integer(((Integer) expr1.eval(nametable, functiontable, var)).intValue() - ((Integer) expr2.eval(nametable, functiontable, var)).intValue());
        return (Integer)value;
    }
}

//added for 2c
class FunctionCall extends Expr {

    private String funcid;
    private ExpressionList explist;

    public FunctionCall(String id, ExpressionList el) {
        funcid = id;
        explist = el;
    }

    public Integer eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        return functiontable.get(funcid).apply(nametable, functiontable, var, explist);
    }
}

abstract class Statement {

    public Statement() {
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {
    }
}

// added for 2c
class DefineStatement extends Statement {

    private String name;
    private Proc proc;
    private ParamList paramlist;
    private StatementList statementlist;

    public DefineStatement(String id, Proc process) {
        name = id;
        proc = process;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functable, LinkedList var) {
        // get the named proc object from the function table.
        //System.out.println("Adding Process:"+name+" to Functiontable");
        functable.put(name, proc);
    }
}

class ReturnStatement extends Statement {

    private Expr expr;

    public ReturnStatement(Expr e) {
        expr = e;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {
        //Java can't throw exceptions of numbers, so we'll convert it to a string
        //and then on the other end we'll reconvert back to Integer..
        throw new Exception(Integer.toString((Integer) expr.eval(nametable, functiontable, var)));
    }
}

class AssignStatement extends Statement {

    private String name;
    private Expr elem;
    private boolean number;

    public AssignStatement(String id, Expr e) {
        name = id;
        elem = e;
        number = true;
    }
    
    public AssignStatement(String id, String otherId) {
        name = id;
        elem = new Ident(otherId, 0);
        number = true;
    }
    
    public AssignStatement(String id, List e) {
        name = id;
        elem = e;
        elem.setNotNumber();
        number = false;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        /* add name to the statementlist of variable names */
        if (!var.contains(name)) {
            var.add(name);
        //insert the variable with the specified name into the table with the
        // evaluated result (which must be an integer
        }

        if (elem instanceof List) {
					//nametable.put(name, ((List) elem).eval());
					nametable.put(name, elem.eval(nametable, functiontable, var));
				} else {
					
					nametable.put(name, elem.eval(nametable, functiontable, var));
				}
    }
}

class IfStatement extends Statement {

    private Expr expr;
    private StatementList stmtlist1,  stmtlist2;

    public IfStatement(Expr e, StatementList list1, StatementList list2) {
        expr = e;
        stmtlist1 = list1;
        stmtlist2 = list2;
    }

    public IfStatement(Expr e, StatementList list) {
        expr = e;
        stmtlist1 = list;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {
        if (((Integer) expr.eval(nametable, functiontable, var)) > 0) {
            stmtlist1.eval(nametable, functiontable, var);
        } else {
            stmtlist2.eval(nametable, functiontable, var);
        }
    }
}

class WhileStatement extends Statement {

    private Expr expr;
    private StatementList stmtlist;

    public WhileStatement(Expr e, StatementList list) {
        expr = e;
        stmtlist = list;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {
        while (((Integer) expr.eval(nametable, functiontable, var)) > 0) {
            stmtlist.eval(nametable, functiontable, var);
        }
    }
}

class RepeatStatement extends Statement {

    private Expr expr;
    private StatementList sl;

    public RepeatStatement(StatementList list, Expr e) {
        expr = e;
        sl = list;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {
        do {
            sl.eval(nametable, functiontable, var);
        } while (((Integer) expr.eval(nametable, functiontable, var)) > 0);

    }
}

//added for 2c
class ParamList {

    private LinkedList<String> parameterlist;

    public ParamList(String name) {
        parameterlist = new LinkedList<String>();
        parameterlist.add(name);
    }

    public ParamList(String name, ParamList parlist) {
        parameterlist = parlist.getParamList();
        parameterlist.add(name);
    }

    public LinkedList<String> getParamList() {
        return parameterlist;
    }
}

// Added for 2c
class ExpressionList {

    private LinkedList<Expr> list;

    public ExpressionList(Expr ex) {
        list = new LinkedList<Expr>();
        list.add(ex);
    }

    public ExpressionList(Expr ex, ExpressionList el) {
        list = new LinkedList<Expr>();
        //we need ot add the expression to the front of the list
        list.add(0, ex);

    }

    public LinkedList<Expr> getExpressions() {
        return list;
    }
}

class StatementList {

    private LinkedList<Statement> statementlist;

    public StatementList(Statement statement) {
        statementlist = new LinkedList<Statement>();
        statementlist.add(statement);
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) throws Exception {


        for (Statement stmt : statementlist) {
            stmt.eval(nametable, functiontable, var);

        }
    }

    public void insert(Statement s) {
        // we need to add it to the front of the list
        statementlist.add(s);
    }

    public LinkedList<Statement> getStatements() {
        return statementlist;
    }
}

class Proc {

    private ParamList parameterlist;
    private StatementList stmtlist;

    public Proc(ParamList pl, StatementList sl) {
        parameterlist = pl;
        stmtlist = sl;
    }

    public int apply(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var, ExpressionList expressionlist) {
        //System.out.println("Executing Proceedure");
        HashMap newnametable = new HashMap();

        // bind parameters in new name table
        // we need to get the underlying List structure that the ParamList uses...
        Iterator<String> p = parameterlist.getParamList().iterator();
        Iterator<Expr> e = expressionlist.getExpressions().iterator();

        if (parameterlist.getParamList().size() != expressionlist.getExpressions().size()) {
            System.out.println("Param count does not match");
            System.exit(1);
        }
        while (p.hasNext() && e.hasNext()) {

            // assign the evaluation of the expression to the parameter name.
            newnametable.put(p.next(), (Integer) e.next().eval(nametable, functiontable, var));
        //System.out.println("Loading Nametable for procedure with: "+p+" = "+nametable.get(p));

        }
        // evaluate function body using new name table and
        // old function table
        // eval statement list and catch return
        //System.out.println("Beginning Proceedure Execution..");
        try {
            stmtlist.eval(newnametable, functiontable, var);
        } catch (Exception result) {
            // Note, the result shold contain the proceedure's return value as a String
            //System.out.println("return value = "+result.getMessage());
            return Integer.parseInt(result.getMessage());
        }
        System.out.println("Error:  no return value");
        System.exit(1);
        // need this or the compiler will complain, but should never
        // reach this...
        return 0;
    }
}

class Program {

    private StatementList stmtlist;

    public Program(StatementList list) {
        stmtlist = list;
    }

    public void eval(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        try {
            stmtlist.eval(nametable, functiontable, var);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void dump(HashMap nametable, HashMap<String, Proc> functiontable, LinkedList var) {
        System.out.println(nametable.values());
        System.out.println("Dumping out all the variables...");
        if (nametable != null) {
            for (String name : (Set<String>)nametable.keySet()) {
                System.out.println(name + "=" + nametable.get(name));
            }
        }
        if (functiontable != null) {
            for (String name : functiontable.keySet()) {
                System.out.println("Function: " + name + " defined...");
            }
        }
    }
}
