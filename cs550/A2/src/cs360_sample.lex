/* this is the lex file for the sample mini language at
 * http://www.cs.drexel.edu/~jjohnson/2006-07/winter/cs360/lectures/lec6.html
 * 
 * created by Xu, 2/5/07
 */

import java_cup.runtime.Symbol;
%%
%cupsym sym

%cup
%%
"cons" {return new Symbol(sym.CONS); }
"car" {return new Symbol(sym.CAR); }
"cdr" {return new Symbol(sym.CDR); }
"nullp" {return new Symbol(sym.NULLP); }
"intp" {return new Symbol(sym.INTP); }
"listp" {return new Symbol(sym.LISTP);}
"||" {return new Symbol(sym.CONCAT);}
"[" {return new Symbol(sym.LBRACKET); }
"]" {return new Symbol(sym.RBRACKET); }
"," {return new Symbol(sym.COMMA); }
";" {return new Symbol(sym.SEMI); }
"+" {return new Symbol(sym.PLUS); }
"-" {return new Symbol(sym.MINUS); }
"*" {return new Symbol(sym.TIMES); }
":=" {return new Symbol(sym.ASSIGN); }
"define" {return new Symbol(sym.DEFINE); }
"(" {return new Symbol(sym.LPAREN); }
")" {return new Symbol(sym.RPAREN); }
"if" {return new Symbol(sym.IF); }
"then" {return new Symbol(sym.THEN); }
"else" {return new Symbol(sym.ELSE); }
"fi" {return new Symbol(sym.FI);}
"while" {return new Symbol(sym.WHILE); }
"do" {return new Symbol(sym.DO); }
"od" {return new Symbol(sym.OD); }
"proc" {return new Symbol(sym.PROC); }
"end" {return new Symbol(sym.END); }
"repeat" {return new Symbol(sym.REPEAT); }
"until" {return new Symbol(sym.UNTIL); }
"return" {return new Symbol(sym.RETURN); }
[0-9]+ {return new Symbol(sym.NUMBER, new Integer(yytext())); }
[a-z,A-Z]+ {return new Symbol(sym.ID, new String(yytext())); }
[ \t\r\n\f] {/* ignore white space */}
. {System.err.println("Illegal character: "+yytext());}
