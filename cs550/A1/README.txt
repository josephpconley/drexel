CS550 - Assignment 1
Joe Conley

This directory A1 contains parts 1 and 2 of assignment 1.  I did most of the development in Eclipse
(save for the command line tasks involved in part 2) and as such it might be easier to build/run
from there or a similar IDE.  If you were to import this project into an IDE and add everything from the lib
folder into the CLASSPATH, both parsers should compile and run without issue.  I also ran tests from the
command line and found it easier to add all the contents of the lib folder into the CLASSPATH variable
before running either parser from the command line.

Part 1 - Recursive Descent Parser

The recursive descent parser is laid out in cs550.as01.RDParser.  It relies on the java.regex.*
package to scan input for a list of tokens.  There is one method for each production, with
additional methods match() and getToken() which help to enforce these productions by
iterating over the tokens and checking for patterns.  If the input is accepted by these productions,
it will be stored in an Object[] and a call will be made to cs550.StringUtility.getList() to 
print this Object[] recursively.

Part 2 - Automatic Parser

The automatic parser generated using CUP/Jlex can be found at cs550.jlexcup.parser.  
This class was generated as follows:

Jlex
Creating jlexcup/grammar.lex, which defines how to tokenize the input via regular expressions
Running java Jlex.Main grammar.lex (with lib/Jlex in the CLASSPATH) to generate gramamr.lex.java
Renaming gramamr.lex.java to Yylex.java in order to integrate it with CUP

CUP
Creating jlexcup/grammar.cup, which defines the productions, terminals, and non-terminals 
	of the grammar, and also allows for the tracking of attributes in each variable
Running java -jar java-cup-11a.jar grammar.cup to generate two source files, sym.java and parser.java,
	and placing them in the cs550.jlexcup directory
Placing lib/java-cup-11a.jar on the CLASSPATH so we can run parser.java 

From here it remains to run cs550.jlexcup.parser, providing input via the command line.  This automatic parser
will also call cs550.StringUtility.getList() to recursively print the data structure. I soon learned 
that the automatic parser worked better with an additional terminal which indicated that the line of input 
was ended (SEMI).  There is also an additional production (list_group ::= list_group list_part | list_part;) which was added 
solely to allow processing of multiple inputs.  I credit the README file provided by C. Scott Ananian in the 
JLex documentation for figuring out how to process multiple inputs.

Testing/Functionality
The test cases are located at input/good.txt (test cases that should be accepted by both parsers properly)
and input/bad.txt (test cases which should be rejected by both parsers).  I have verified that 
the good and bad input behave appropriately in both parsers; however, both parsers terminate upon rejecting 
an input.  While it would be ideal to have the parsers written in such a way, given the time constraints I chose 
to focus on the core functionality of both parsers.  Another difference from the grammar in class is that I use
a semi-colon to indicate the full input is finished.  This proved easier to work with for the automatic parser 
and as a result I added to the manual parser to ensure that there are no illegal trailing characters.

 