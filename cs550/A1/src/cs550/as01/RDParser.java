package cs550.as01;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cs550.StringUtility;

/**
 * Class: RDParser
 *
 * Description: A recursive descent parser which uses the context-free grammar
 * 				discussed in class to parse input from the command line, 
 *  			place into an array of Objects, and print accordingly.  This follows the example
 *  			in class closely as each production is given it's own method.  Per the README file,
 *  			a semi-colon was added as a terminal to tell the parser when the proper input is over  
 *  
 * Created on Apr 8, 2010
 * @author Joe Conley
 */

public class RDParser {
	Matcher m;
	
	//regex pattern ensuring that the only possible tokens are in 
	//the set {"(",")",",","NUMBER",";"}
	final String REGEX = "\\(|\\)|\\,|-*[0-9]+|;";
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		String input = "";
		while(input != null){
			input = in.readLine();
			
			//Takes input, removes whitespace and sends it to the parser to be scanned
			RDParser parser = new RDParser(input);
			Object[] list = parser.list();
			
			//Checks to see if token following input list is a semi-colon; otherwise reject
			String semi = input.substring(parser.m.end());
			if(semi.equals(";")){
				System.out.println(StringUtility.getList(list));				
			}else{
				throw new Exception("String rejected!");
			}
		}
	}
	
	//Constructor for RDParser, scans input and parses out into tokens based on regex
	public RDParser(String input){
		m = Pattern.compile(REGEX).matcher(input);
	    m.find();
	}
	
	//The following productions are modeled very closely after the productions outlined in the lecture
	public Object[] list() throws Exception{
		Object[] list = new Object[0];
		
		match("\\(");
		String next = getToken();
		if(next.equals(")") == false){
			ArrayList<Object> seq = seq();
			Object[] a = new Object[seq.size()];
			list = seq.toArray(a);
		}
		match("\\)");
		return list;
	}

	public ArrayList<Object> seq() throws Exception{
		ArrayList<Object> seq = new ArrayList<Object>();
		seq.add(elt());
		if(getToken().equals(",")){
			m.find();
			seq.addAll(seq());
		}
		return seq;
	}
	
	public Object elt() throws Exception{
		Object el;
		if(m.group().equals("(")){
			el = list();
		}else{
			match("-*[0-9]+");	//match any NUMBER
			el = m.group();
		}
		return el;
	}
	
	//Matches the current token against the given pattern, throws an error if it doesn't match
	public void match(String pattern) throws Exception{
		if(Pattern.compile(pattern).matcher(m.group()).matches() == false){
			throw new Exception("No match!");
		}
	}
	
	//Finds the next token and returns it
	public String getToken(){
		m.find();
		return m.group();
	}
}
