package cs550;

/**
 * Class: StringUtility
 *
 * Description: Contains method for a recursive print statement used by my manual parser
 * 				and the automatically generated parser from lex/CUP
 *  
 * Created on Apr 10, 2010
 * @author Joe Conley
 */
public class StringUtility {
	public static String getList(Object[] list){
		StringBuffer sb = new StringBuffer("(");
		for(int i=0;i<list.length;i++){
			if(i > 0){
				sb.append(",");
			}
			
			if(list[i] instanceof String){
				sb.append(list[i].toString());
			}else{
				sb.append(getList((Object [])list[i]));
			}
		}
		sb.append(")");
		return sb.toString();
	}
}
